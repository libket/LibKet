########################################################################
# CMakeLists.txt
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

include(QuEST)

########################################################################
# Generate test suite
########################################################################

set(UNITTESTS_SRC "")

# Generate individual source/header files of the form
# QuEST-unittest_<test>.cxx
# QuEST-unittest_<test>.hpp
foreach(HEADER ${LIBKET_UNITTESTS_HEADERS})

  # Include unittest header file in HPP file
  set(INCLUDE_UNITTESTS_HEADERS "#include \"${HEADER}\"")

  # Extract unittest name
  string(REPLACE ".hpp"  "" HEADER ${HEADER})
  
  # Include unittest header file in CXX file
  set(INCLUDE_UNITTESTS_HPP "#include \"QuEST-unittest_${HEADER}.hpp\"")
    
  # Generate CXX file
  configure_file(QuEST-unittest.cxx.in QuEST-unittest_${HEADER}.cxx)
  list(APPEND UNITTESTS_SRC QuEST-unittest_${HEADER}.cxx)
      
  # Generate HPP file
  configure_file(QuEST-unittest.hpp.in QuEST-unittest_${HEADER}.hpp)
          
  configure_file("${CMAKE_CURRENT_SOURCE_DIR}/QuEST-unittest.suite.in"
    "${CMAKE_CURRENT_BINARY_DIR}/QuEST-unittest.tmp")
  file(READ "${CMAKE_CURRENT_BINARY_DIR}/QuEST-unittest.tmp"   tmp)
  file(APPEND "${CMAKE_CURRENT_BINARY_DIR}/QuEST-unittest_${HEADER}.hpp" "${tmp}")
  file(REMOVE "${CMAKE_CURRENT_BINARY_DIR}/QuEST-unittest.tmp")
  
endforeach()

# Create new header file
configure_file(QuEST-unittest.hpp.in QuEST-unittest.hpp)

configure_file("${CMAKE_CURRENT_SOURCE_DIR}/QuEST-unittest.suite.in"
  "${CMAKE_CURRENT_BINARY_DIR}/QuEST-unittest.tmp")
file(READ "${CMAKE_CURRENT_BINARY_DIR}/QuEST-unittest.tmp"   tmp)
file(APPEND "${CMAKE_CURRENT_BINARY_DIR}/QuEST-unittest.hpp" "${tmp}")
file(REMOVE "${CMAKE_CURRENT_BINARY_DIR}/QuEST-unittest.tmp")

# Include generated header file into search path
include_directories(${CMAKE_CURRENT_BINARY_DIR})

########################################################################
# Create executables from source files and add tests
########################################################################

add_executables("${UNITTESTS_SRC}")
add_tests("${UNITTESTS_SRC}")

file(GLOB UNITTESTS_SRC RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.cpp *.cxx)
add_executables("${UNITTESTS_SRC}")
add_tests("${UNITTESTS_SRC}")
