/** @file test_gate_prep_y.hpp
 *
 *  @brief LibKet::gates::prep_y() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_prep_y)
{
  TEST_API_CONSISTENCY( prep_y    );
  TEST_API_CONSISTENCY( PREP_Y    );
  TEST_API_CONSISTENCY( QPrep_Y() );
  
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QPrep_Y", prep_y    );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QPrep_Y", PREP_Y    );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QPrep_Y", QPrep_Y() );
}
