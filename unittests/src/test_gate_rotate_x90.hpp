/** @file test_gate_rotate_x90.hpp
 *
 *  @brief LibKet::gates::rotate_x90() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_x90)
{
  TEST_API_CONSISTENCY( rotate_x90 );
  TEST_API_CONSISTENCY( ROTATE_X90 );
  TEST_API_CONSISTENCY( rx90       );
  TEST_API_CONSISTENCY( RX90       );
  TEST_API_CONSISTENCY( Rx90       );
  TEST_API_CONSISTENCY( x90        );
  TEST_API_CONSISTENCY( X90         );

  TEST_API_CONSISTENCY( rotate_x90dag );
  TEST_API_CONSISTENCY( ROTATE_X90dag );
  TEST_API_CONSISTENCY( rx90dag       );
  TEST_API_CONSISTENCY( RX90dag       );
  TEST_API_CONSISTENCY( Rx90dag       );
  TEST_API_CONSISTENCY( x90dag        );
  TEST_API_CONSISTENCY( X90dag        );
  
  TEST_API_CONSISTENCY( QRotate_X90() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", rotate_x90, "QRotate_MX90", rotate_x90dag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", ROTATE_X90, "QRotate_MX90", ROTATE_X90dag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", rx90,       "QRotate_MX90", rx90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", RX90,       "QRotate_MX90", RX90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", Rx90,       "QRotate_MX90", Rx90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", x90,        "QRotate_MX90", x90dag        );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", X90,        "QRotate_MX90", X90dag        );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", rotate_x90dag, "QRotate_X90", rotate_x90 );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", ROTATE_X90dag, "QRotate_X90", ROTATE_X90 );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", rx90dag,       "QRotate_X90", rx90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", RX90dag,       "QRotate_X90", RX90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", Rx90dag,       "QRotate_X90", Rx90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", x90dag,        "QRotate_X90", x90        );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", X90dag,        "QRotate_X90", X90        );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", QRotate_X90(),
                                         "QRotate_MX90", QRotate_MX90() );  
}
