/** @file test_gate_pauli_x.hpp
 *
 *  @brief LibKet::gates::pauli_x() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_pauli_x)
{
  TEST_API_CONSISTENCY( pauli_x );
  TEST_API_CONSISTENCY( PAULI_X );
  TEST_API_CONSISTENCY( x       );
  TEST_API_CONSISTENCY( X       );

  TEST_API_CONSISTENCY( pauli_xdag );
  TEST_API_CONSISTENCY( PAULI_Xdag );
  TEST_API_CONSISTENCY( xdag       );
  TEST_API_CONSISTENCY( Xdag       );

  TEST_API_CONSISTENCY( QPauli_X() );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", pauli_x );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", PAULI_X );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", x       );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", X       );
  
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", pauli_xdag );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", PAULI_Xdag );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", xdag       );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", Xdag       );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QPauli_X", QPauli_X() );
}
