/** @file unittests/src/detail/cQASM.hpp

    @brief Unittests cQASM quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_CQASM_HPP
#define DETAIL_CQASM_HPP

#include <test_config.h>

#ifdef LIBKET_WITH_CQASM

template<std::size_t _qubits>
struct Fixture_cQASM
{
public:
  LibKet::QDevice<LibKet::QDeviceType::qi_26_simulator, _qubits> device;

  template<typename Expr>
  bool run(const Expr& expr)
  {
#ifdef LIBKET_BUILD_UNITTESTS_BACKEND
    try {
      LibKet::utils::json result = device(expr).eval(16);
      return device.template get<LibKet::QResultType::status>(result);
    } catch (...) {
      return false;
    }
#else
    return true;
#endif
  }
};

#endif // LIBKET_WITH_CQASM
#endif // DETAIL_CQASM_HPP
