/** @file unittests/src/test_filter.hpp
 *
 *  @brief Filter unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;

TEST_FIXTURE(Fixture, filter)
{
  TEST_API_CONSISTENCY( (all)          );
  TEST_API_CONSISTENCY( (sel<0,1,2,3>) );
  TEST_API_CONSISTENCY( (range<0,3>)   );
  TEST_API_CONSISTENCY( (shift<1>)     );
  TEST_API_CONSISTENCY( (qureg<0,4>)   );
  TEST_API_CONSISTENCY( (qubit<0>)     );

  TEST_API_CONSISTENCY( (QFilterSelectAll())        );
  TEST_API_CONSISTENCY( (QFilterSelect<0,1,2,3>())  );
  TEST_API_CONSISTENCY( (QFilterSelectRange<0,3>()) );
  TEST_API_CONSISTENCY( (QFilterShift<1>())         );
  TEST_API_CONSISTENCY( (QRegister<0,4>())          );
  TEST_API_CONSISTENCY( (QBit<0>())                 );
  
  try {
    // all(all()) == all()
    auto expr = all(all());
    auto expr1 = all();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // all(sel<0,1,2,3>()) == all()
    auto expr = all(sel<0, 1, 2, 3>());
    auto expr1 = all();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // all(range<0,3>()) == all()
    auto expr = all(range<0, 3>());
    auto expr1 = all();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // all(qureg<0,4>()) == all()
    auto expr = all(qureg<0, 4>());
    auto expr1 = all();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // all(qubit<0>()) == all()
    auto expr = all(qubit<0>());
    auto expr1 = all();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<0,1,2,3>(all()) == sel<0,1,2,3>()
    auto expr = sel<0, 1, 2, 3>(all());
    auto expr1 = sel<0, 1, 2, 3>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<1,3>(sel<0,1,2,3>()) == sel<1,3>()
    auto expr = sel<1, 3>(sel<0, 1, 2, 3>());
    auto expr1 = sel<1, 3>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<1,3>(range<0,3>()) == sel<1,3>()
    auto expr = sel<1, 3>(range<0, 3>());
    auto expr1 = sel<1, 3>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<1,3>(qureg<0,4>()) == sel<1,3>()
    auto expr = sel<1, 3>(qureg<0, 4>());
    auto expr1 = sel<1, 3>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // sel<0>(qubit<3>()) == sel<3>()
    auto expr = sel<0>(qubit<3>());
    auto expr1 = sel<3>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,3>(all()) == range<0,3>()
    auto expr = range<0, 3>(all());
    auto expr1 = range<0, 3>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,3>(sel<5,6,7,8,9,10>()) == sel<5,6,7,8>()
    auto expr = range<0, 3>(sel<5, 6, 7, 8, 9, 10>());
    auto expr1 = sel<5, 6, 7, 8>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,3>(range<5,10>()) == sel<5,6,7,8>()
    auto expr = range<0, 3>(range<5, 10>());
    auto expr1 = sel<5, 6, 7, 8>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,3>(qureg<5,6>()) == sel<5,6,7,8>()
    auto expr = range<0, 3>(qureg<5, 6>());
    auto expr1 = sel<5, 6, 7, 8>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // range<0,0>(qubit<5>()) == qubit<5>()
    auto expr = range<0, 0>(qubit<5>());
    auto expr1 = sel<5>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<3,6>(all()) == range<3,8>()
    auto expr = qureg<3, 6>(all());
    auto expr1 = range<3, 8>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<3,6>(sel<1,2,3,4,5,6,7,8,9,10>()) == range<4,9>()
    auto expr = qureg<3, 6>(sel<1, 2, 3, 4, 5, 6, 7, 8, 9, 10>());
    auto expr1 = range<4, 9>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<3,6>(range<1,10>()) == range<4,9>()
    auto expr = qureg<3, 6>(range<1, 10>());
    auto expr1 = range<4, 9>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<3,6>(qureg<1,10>()) == range<4,9>()
    auto expr = qureg<3, 6>(qureg<1, 10>());
    auto expr1 = range<4, 9>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qureg<0,1>(qubit<1>()) == sel<1>()
    auto expr = qureg<0, 1>(qubit<1>());
    auto expr1 = sel<1>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<1>(all()) == sel<1>()
    auto expr = qubit<1>(all());
    auto expr1 = sel<1>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<1>(sel<1,2,3,4,5>()) == sel<2>()
    auto expr = qubit<1>(sel<1, 2, 3, 4, 5>());
    auto expr1 = sel<2>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<1>(range<1,5>()) == sel<2>()
    auto expr = qubit<1>(range<1, 5>());
    auto expr1 = sel<2>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<1>(qureg<1,5>()) == sel<2>()
    auto expr = qubit<1>(qureg<1, 5>());
    auto expr1 = sel<2>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qubit<0>(qubit<1>()) == sel<1>()
    auto expr = qubit<0>(qubit<1>());
    auto expr1 = sel<1>();
    CHECK((std::is_same<decltype(expr), decltype(expr1)>::value));

  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
