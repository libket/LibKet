/** @file test_gate_hadamard.hpp
 *
 *  @brief LibKet::gates::hadamard() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_hadamard)
{
  TEST_API_CONSISTENCY( hadamard );
  TEST_API_CONSISTENCY( HADAMARD );
  TEST_API_CONSISTENCY( h        );
  TEST_API_CONSISTENCY( H        );

  TEST_API_CONSISTENCY( hadamarddag );
  TEST_API_CONSISTENCY( HADAMARDdag );
  TEST_API_CONSISTENCY( hdag        );
  TEST_API_CONSISTENCY( Hdag        );

  TEST_API_CONSISTENCY( QHadamard() );
  
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", hadamard );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", HADAMARD );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", h        );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", H        );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", hadamarddag );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", HADAMARDdag );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", hdag        );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", Hdag        );
    
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QHadamard", QHadamard() );
}
