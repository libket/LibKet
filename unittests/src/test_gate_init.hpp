/** @file test_gate_init.hpp
 *
 *  @brief LibKet::gates::init() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_init)
{
  TEST_API_CONSISTENCY( init );
  TEST_API_CONSISTENCY( INIT );
  
  TEST_API_CONSISTENCY( QInit() );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QInit", init );
  TEST_UNARY_GATE( "QInit", INIT );
  
  TEST_UNARY_GATE( "QInit", QInit() );
  
  try {
    // init(init(init()))
    auto expr = LibKet::gates::init(LibKet::gates::init(init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n"
      "|   gate = QInit\n"
      "| filter = QFilterSelectAll\n"
      "|   expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n"
      "|   gate = QInit\n"
      "| filter = QFilterSelectAll\n"
      "|   expr = UnaryQGate\n"
      "|          |   gate = QInit\n"
      "|          | filter = QFilterSelectAll\n"
      "|          |   expr = UnaryQGate\n"
      "|          |          |   gate = QInit\n"
      "|          |          | filter = QFilterSelectAll\n"
      "|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
