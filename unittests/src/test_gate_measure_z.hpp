/** @file test_gate_measure_z.hpp
 *
 *  @brief LibKet::gates::measure_z() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_measure_z)
{
  TEST_API_CONSISTENCY( measure_z );
  TEST_API_CONSISTENCY( MEASURE_Z );
  
  TEST_API_CONSISTENCY( QMeasure_Z() );

  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QMeasure_Z", measure_z );
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QMeasure_Z", MEASURE_Z );
  
  TEST_UNARY_GATE_OPTIMIZE_SINGLE( "QMeasure_Z", QMeasure_Z() );
}
