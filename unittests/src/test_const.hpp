/** @file unittests/src/test_const.hpp
 *
 *  @brief QConst unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet;

TEST_FIXTURE(Fixture, constvalue)
{
  try {
    auto c = QConst1(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst1(3)");
    CHECK_EQUAL(c.to_string(), "3");
    CHECK_EQUAL(c.to_type(),   "QConst1_t(3)");
    CHECK_EQUAL(c.to_obj(),    "QConst1(3)");

    CHECK_EQUAL(c.to_float(),       3.f);
    CHECK_EQUAL(c.to_double(),      3.);
    CHECK_EQUAL(c.to_long_double(), 3.L);
    CHECK_EQUAL(c.value(),  (real_t)3.);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst2(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst2(3.)");
    CHECK_EQUAL(c.to_string(), "3.");
    CHECK_EQUAL(c.to_type(),   "QConst2_t(3.)");
    CHECK_EQUAL(c.to_obj(),    "QConst2(3.)");

    CHECK_EQUAL(c.to_float(),       3.f);
    CHECK_EQUAL(c.to_double(),      3.);
    CHECK_EQUAL(c.to_long_double(), 3.L);
    CHECK_EQUAL(c.value(),  (real_t)3.);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst4(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst4(3.14)");
    CHECK_EQUAL(c.to_string(), "3.14");
    CHECK_EQUAL(c.to_type(),   "QConst4_t(3.14)");
    CHECK_EQUAL(c.to_obj(),    "QConst4(3.14)");

    CHECK_EQUAL(c.to_float(),       3.14f);
    CHECK_EQUAL(c.to_double(),      3.14);
    CHECK_EQUAL(c.to_long_double(), 3.14L);
    CHECK_EQUAL(c.value(),  (real_t)3.14);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst8(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst8(3.141592)");
    CHECK_EQUAL(c.to_string(), "3.141592");
    CHECK_EQUAL(c.to_type(),   "QConst8_t(3.141592)");
    CHECK_EQUAL(c.to_obj(),    "QConst8(3.141592)");

    CHECK_EQUAL(c.to_float(),       3.141592f);
    CHECK_EQUAL(c.to_double(),      3.141592);
    CHECK_EQUAL(c.to_long_double(), 3.141592L);
    CHECK_EQUAL(c.value(),  (real_t)3.141592);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst16(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst16(3.14159265358979)");
    CHECK_EQUAL(c.to_string(), "3.14159265358979");
    CHECK_EQUAL(c.to_type(),   "QConst16_t(3.14159265358979)");
    CHECK_EQUAL(c.to_obj(),    "QConst16(3.14159265358979)");

    CHECK_EQUAL(c.to_float(),       3.14159265358979f);
    CHECK_EQUAL(c.to_double(),      3.14159265358979);
    CHECK_EQUAL(c.to_long_double(), 3.14159265358979L);
    CHECK_EQUAL(c.value(),  (real_t)3.14159265358979);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst32(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst32(3.141592653589793238462643383279)");
    CHECK_EQUAL(c.to_string(), "3.141592653589793238462643383279");
    CHECK_EQUAL(c.to_type(),   "QConst32_t(3.141592653589793238462643383279)");
    CHECK_EQUAL(c.to_obj(),    "QConst32(3.141592653589793238462643383279)");

    CHECK_EQUAL(c.to_float(),       3.14159265358979323846f);
    CHECK_EQUAL(c.to_double(),      3.14159265358979323846);
    CHECK_EQUAL(c.to_long_double(), 3.14159265358979323846L);
    CHECK_EQUAL(c.value(),  (real_t)3.14159265358979323846);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst64(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst64(3.141592653589793238462643383279502884)");
    CHECK_EQUAL(c.to_string(), "3.141592653589793238462643383279502884");
    CHECK_EQUAL(c.to_type(),   "QConst64_t(3.141592653589793238462643383279502884)");
    CHECK_EQUAL(c.to_obj(),    "QConst64(3.141592653589793238462643383279502884)");

    CHECK_EQUAL(c.to_float(),       3.141592653589793238462643383279502884f);
    CHECK_EQUAL(c.to_double(),      3.141592653589793238462643383279502884);
    CHECK_EQUAL(c.to_long_double(), 3.141592653589793238462643383279502884L);
    CHECK_EQUAL(c.value(),  (real_t)3.141592653589793238462643383279502884);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst128(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst128(3.141592653589793238462643383279502884)");
    CHECK_EQUAL(c.to_string(), "3.141592653589793238462643383279502884");
    CHECK_EQUAL(c.to_type(),   "QConst128_t(3.141592653589793238462643383279502884)");
    CHECK_EQUAL(c.to_obj(),    "QConst128(3.141592653589793238462643383279502884)");

    CHECK_EQUAL(c.to_float(),       3.141592653589793238462643383279502884f);
    CHECK_EQUAL(c.to_double(),      3.141592653589793238462643383279502884);
    CHECK_EQUAL(c.to_long_double(), 3.141592653589793238462643383279502884L);
    CHECK_EQUAL(c.value(),  (real_t)3.141592653589793238462643383279502884);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst256(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    CHECK_EQUAL(ss.str(),      "QConst256(3.141592653589793238462643383279502884)");
    CHECK_EQUAL(c.to_string(), "3.141592653589793238462643383279502884");
    CHECK_EQUAL(c.to_type(),   "QConst256_t(3.141592653589793238462643383279502884)");
    CHECK_EQUAL(c.to_obj(),    "QConst256(3.141592653589793238462643383279502884)");

    CHECK_EQUAL(c.to_float(),       3.141592653589793238462643383279502884f);
    CHECK_EQUAL(c.to_double(),      3.141592653589793238462643383279502884);
    CHECK_EQUAL(c.to_long_double(), 3.141592653589793238462643383279502884L);
    CHECK_EQUAL(c.value(),  (real_t)3.141592653589793238462643383279502884);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst(3.141592653589793238462643383279502884);
    std::stringstream ss; ss << c;
    if (std::is_same<LibKet::real_t, float>::value) {
      CHECK_EQUAL(ss.str(),      "QConst8(3.141592)");
      CHECK_EQUAL(c.to_string(), "3.141592");
      CHECK_EQUAL(c.to_type(),   "QConst8_t(3.141592)");
      CHECK_EQUAL(c.to_obj(),    "QConst8(3.141592)");
    }
    else if (std::is_same<LibKet::real_t, double>::value) {
      CHECK_EQUAL(ss.str(),      "QConst16(3.14159265358979)");
      CHECK_EQUAL(c.to_string(), "3.14159265358979");
      CHECK_EQUAL(c.to_type(),   "QConst16_t(3.14159265358979)");
      CHECK_EQUAL(c.to_obj(),    "QConst16(3.14159265358979)");
    }

    CHECK_CLOSE(c.to_float(),       3.141592653589793238462643383279502884f, std::numeric_limits<LibKet::real_t>::epsilon() * c.to_float()  * 10);
    CHECK_CLOSE(c.to_double(),      3.141592653589793238462643383279502884,  std::numeric_limits<LibKet::real_t>::epsilon() * c.to_double() * 10);
    CHECK_CLOSE(c.to_long_double(), 3.141592653589793238462643383279502884L, std::numeric_limits<LibKet::real_t>::epsilon() * c.to_long_double() * 10);
    CHECK_CLOSE(c.value(), (real_t) 3.141592653589793238462643383279502884,  std::numeric_limits<LibKet::real_t>::epsilon() * c.value() * 10);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = +QConst64(3.141);
    CHECK_EQUAL(c.to_obj(), "QConst64(3.141)");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = -QConst64(3.141);
    CHECK_EQUAL(c.to_obj(), "QConst64(-3.141)");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = -QConst64(-3.141);
    CHECK_EQUAL(c.to_obj(), "QConst64(3.141)");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst64(3.141);
    auto d = QConst64(3.142);
    CHECK_EQUAL(c==c, true);
    CHECK_EQUAL(c==d, false);
    CHECK_EQUAL(c!=c, false);
    CHECK_EQUAL(c!=d, true);
    CHECK_EQUAL(c<=c, true);
    CHECK_EQUAL(c<=d, true);
    CHECK_EQUAL(c<c,  false);
    CHECK_EQUAL(c<d,  true);
    CHECK_EQUAL(c>=c, true);
    CHECK_EQUAL(c>=d, false);
    CHECK_EQUAL(c>c,  false);
    CHECK_EQUAL(c>d,  false);    
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    auto c = QConst8(3.141592653589793238462643383279502884);
    auto d = QConst8(2);

    CHECK_EQUAL((c+d).to_string(), "5.141592");
    CHECK_EQUAL((c-d).to_string(), "1.141592");
    CHECK_EQUAL((c*d).to_string(), "6.283184");
    CHECK_EQUAL((c/d).to_string(), "1.570796");

    CHECK_EQUAL((-c+d).to_string(), "-1.14159");
    CHECK_EQUAL((-c-d).to_string(), "-5.14159");
    CHECK_EQUAL((-c*d).to_string(), "-6.28318");
    CHECK_EQUAL((-c/d).to_string(), "-1.570795");    
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
  
}
