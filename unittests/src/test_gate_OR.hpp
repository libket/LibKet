/** @file test_gate_OR.hpp
 *
 *  @brief LibKet::gates::qor() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller & Huub Donkers
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_qor)
{
  TEST_API_CONSISTENCY( qor );
  TEST_API_CONSISTENCY( QOR );
  
  try {
    // qor()
    auto expr = qor();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QOR\n| filter = QFilter\n|  expr0 "
                "= QFilter\n|  expr1 = QFilter\n|  expr2 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qor(all(),all())
    auto expr = qor(all(), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QOR\n| filter = "
                "QFilterSelectAll\n|  expr0 = QFilterSelectAll\n|  expr1 = "
                "QFilterSelectAll\n|  expr2 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qor(sel<0,1,3,5>(), sel<2,4,6,7>(), sel<8,9,10,11>())
    auto expr =
      qor(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(), sel<8, 9, 10, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 3 5 2 4 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 = "
      "QFilterSelect [ 2 4 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qor(range<0,3>(), range<4,7>(), range<8,11>())
    auto expr = qor(range<0, 3>(), range<4, 7>(), range<8, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qor(qureg<0,4>(), qureg<4,4>(), qureg<8,4>())
    auto expr = qor(qureg<0, 4>(), qureg<4, 4>(), qureg<8, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // qor(qubit<1>(), qubit<2>(), qubit<3>())
    auto expr = qor(qubit<1>(), qubit<2>(), qubit<3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 1 "
                "2 3 ]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = "
                "QFilterSelect [ 2 ]\n|  expr2 = QFilterSelect [ 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // OR()
    auto expr = OR();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QOR\n| filter = QFilter\n|  expr0 "
                "= QFilter\n|  expr1 = QFilter\n|  expr2 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // OR(all(),all())
    auto expr = OR(all(), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QOR\n| filter = "
                "QFilterSelectAll\n|  expr0 = QFilterSelectAll\n|  expr1 = "
                "QFilterSelectAll\n|  expr2 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // OR(sel<0,1,3,5>(), sel<2,4,6,7>(), sel<8,9,10,11>())
    auto expr =
      OR(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(), sel<8, 9, 10, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 3 5 2 4 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 = "
      "QFilterSelect [ 2 4 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // OR(range<0,3>(), range<4,7>(), range<8,11>())
    auto expr = OR(range<0, 3>(), range<4, 7>(), range<8, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // OR(qureg<0,4>(), qureg<4,4>(), qureg<8,4>())
    auto expr = OR(qureg<0, 4>(), qureg<4, 4>(), qureg<8, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // OR(qubit<1>(), qubit<2>(), qubit<3>())
    auto expr = OR(qubit<1>(), qubit<2>(), qubit<3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 1 "
                "2 3 ]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = "
                "QFilterSelect [ 2 ]\n|  expr2 = QFilterSelect [ 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QOR()
    auto expr = QOR();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QOR\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QOR()(all(), all(), all())
    auto expr = QOR()(all(), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QOR\n| filter = "
                "QFilterSelectAll\n|  expr0 = QFilterSelectAll\n|  expr1 = "
                "QFilterSelectAll\n|  expr2 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QOR()(sel<0,1,3,5>(), sel<2,4,6,7>(), sel<8,9,10,11>())
    auto expr =
      QOR()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(), sel<8, 9, 10, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 3 5 2 4 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 3 5 ]\n|  expr1 = "
      "QFilterSelect [ 2 4 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QOR()(range<0,3>(), range<4,7>(), range<8,11>())
    auto expr = QOR()(range<0, 3>(), range<4, 7>(), range<8, 11>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QOR()(qureg<0,4>(), qureg<4,4>(), qureg<8,4>())
    auto expr = QOR()(qureg<0, 4>(), qureg<4, 4>(), qureg<8, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 0 1 2 3 4 5 "
      "6 7 8 9 10 11 ]\n|  expr0 = QFilterSelect [ 0 1 2 3 ]\n|  expr1 = "
      "QFilterSelect [ 4 5 6 7 ]\n|  expr2 = QFilterSelect [ 8 9 10 11 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QOR()(qubit<1>(), qubit<2>(), qubit<3>())
    auto expr = QOR()(qubit<1>(), qubit<2>(), qubit<3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "TernaryQGate\n|   gate = QOR\n| filter = QFilterSelect [ 1 "
                "2 3 ]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = "
                "QFilterSelect [ 2 ]\n|  expr2 = QFilterSelect [ 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(qor(sel<0, 1, 2>(), sel<3, 4, 5>(), sel<6, 7, 8>(init())))
    auto expr = qor(sel<0, 1, 2>(), sel<3, 4, 5>(), sel<6, 7, 8>(init()));
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
