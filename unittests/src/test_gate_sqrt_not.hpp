/** @file test_gate_sqrt_not.hpp
 *
 *  @brief LibKet::gates::sqrt_not() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_sqrt_not)
{
  TEST_API_CONSISTENCY( sqrt_not<> );
  TEST_API_CONSISTENCY( SQRT_NOT<> );
  TEST_API_CONSISTENCY( snot<>     );
  TEST_API_CONSISTENCY( SNOT<>     );

  TEST_API_CONSISTENCY( sqrt_notdag<> );
  TEST_API_CONSISTENCY( SQRT_NOTdag<> );
  TEST_API_CONSISTENCY( snotdag<>     );
  TEST_API_CONSISTENCY( SNOTdag<>     );  

  TEST_API_CONSISTENCY( QSqrt_Not<>() );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", sqrt_not<> );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", SQRT_NOT<> );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", snot<>     );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", SNOT<>     );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", sqrt_notdag<> );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", SQRT_NOTdag<> );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", snotdag<>     );
  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", SNOTdag<>     );

  TEST_UNARY_GATE_OPTIMIZE_IDENTITY( "QSqrt_Not<QConst1_t(0)>", QSqrt_Not<>() );
}
