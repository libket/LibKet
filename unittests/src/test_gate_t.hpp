/** @file test_gate_t.hpp
 *
 *  @brief LibKet::gates::t() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_t)
{
  TEST_API_CONSISTENCY( t );
  TEST_API_CONSISTENCY( T );

  TEST_API_CONSISTENCY( QT() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QT", t, "QTdag", tdag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QT", T, "QTdag", Tdag );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QT", QT(), "QTdag", QTdag() );
}
