/** @file test_gate_tdag.hpp
 *
 *  @brief LibKet::gates::tdag() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_tdag)
{
  TEST_API_CONSISTENCY( tdag );
  TEST_API_CONSISTENCY( Tdag );

  TEST_API_CONSISTENCY( QTdag() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QTdag", tdag, "QT", t );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QTdag", Tdag, "QT", T );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QTdag", QTdag(), "QT", QT() );
}
