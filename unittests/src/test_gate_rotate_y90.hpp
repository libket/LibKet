/** @file test_gate_rotate_y90.hpp
 *
 *  @brief LibKet::gates::rotate_y90() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_y90)
{
  TEST_API_CONSISTENCY( rotate_y90 );
  TEST_API_CONSISTENCY( ROTATE_Y90 );
  TEST_API_CONSISTENCY( ry90       );
  TEST_API_CONSISTENCY( RY90       );
  TEST_API_CONSISTENCY( Ry90       );
  TEST_API_CONSISTENCY( y90        );
  TEST_API_CONSISTENCY( Y90         );

  TEST_API_CONSISTENCY( rotate_y90dag );
  TEST_API_CONSISTENCY( ROTATE_Y90dag );
  TEST_API_CONSISTENCY( ry90dag       );
  TEST_API_CONSISTENCY( RY90dag       );
  TEST_API_CONSISTENCY( Ry90dag       );
  TEST_API_CONSISTENCY( y90dag        );
  TEST_API_CONSISTENCY( Y90dag        );
  
  TEST_API_CONSISTENCY( QRotate_Y90() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", rotate_y90, "QRotate_MY90", rotate_y90dag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", ROTATE_Y90, "QRotate_MY90", ROTATE_Y90dag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", ry90,       "QRotate_MY90", ry90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", RY90,       "QRotate_MY90", RY90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", Ry90,       "QRotate_MY90", Ry90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", y90,        "QRotate_MY90", y90dag        );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", Y90,        "QRotate_MY90", Y90dag        );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", rotate_y90dag, "QRotate_Y90", rotate_y90 );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", ROTATE_Y90dag, "QRotate_Y90", ROTATE_Y90 );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", ry90dag,       "QRotate_Y90", ry90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", RY90dag,       "QRotate_Y90", RY90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", Ry90dag,       "QRotate_Y90", Ry90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", y90dag,        "QRotate_Y90", y90        );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MY90", Y90dag,        "QRotate_Y90", Y90        );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_Y90", QRotate_Y90(),
                                         "QRotate_MY90", QRotate_MY90() );  
}

