/** @file test_gate_rotate_mx90.hpp
 *
 *  @brief LibKet::gates::rotate_mx90() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_mx90)
{
  TEST_API_CONSISTENCY( rotate_mx90  );
  TEST_API_CONSISTENCY( ROTATE_MX90  );
  TEST_API_CONSISTENCY( rmx90        );
  TEST_API_CONSISTENCY( RMX90        );
  TEST_API_CONSISTENCY( Rmx90        );
  TEST_API_CONSISTENCY( mx90         );
  TEST_API_CONSISTENCY( MX90         );

  TEST_API_CONSISTENCY( rotate_mx90dag );
  TEST_API_CONSISTENCY( ROTATE_MX90dag );
  TEST_API_CONSISTENCY( rmx90dag       );
  TEST_API_CONSISTENCY( RMX90dag       );
  TEST_API_CONSISTENCY( Rmx90dag       );
  TEST_API_CONSISTENCY( mx90dag        );
  TEST_API_CONSISTENCY( MX90dag        );
  
  TEST_API_CONSISTENCY( QRotate_MX90() );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", rotate_mx90, "QRotate_X90", rotate_mx90dag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", ROTATE_MX90, "QRotate_X90", ROTATE_MX90dag );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", rmx90,       "QRotate_X90", rmx90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", RMX90,       "QRotate_X90", RMX90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", Rmx90,       "QRotate_X90", Rmx90dag       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", mx90,        "QRotate_X90", mx90dag        );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", MX90,        "QRotate_X90", MX90dag        );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", rotate_mx90dag, "QRotate_MX90", rotate_mx90 );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", ROTATE_MX90dag, "QRotate_MX90", ROTATE_MX90 );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", rmx90dag,       "QRotate_MX90", rmx90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", RMX90dag,       "QRotate_MX90", RMX90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", Rmx90dag,       "QRotate_MX90", Rmx90       );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", mx90dag,        "QRotate_MX90", mx90        );
  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_X90", MX90dag,        "QRotate_MX90", MX90        );

  TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY( "QRotate_MX90", QRotate_MX90(),
                                         "QRotate_X90", QRotate_X90() );  
}
