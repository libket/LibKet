/** @file UNARY.hpp

    @brief Unittests configuration for unary gates

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef UNARY_H
#define UNARY_H

/**
 *  @brief Macro creates a unit test for the given expression in
 *  combination with the given filter expression, i.e.
 *
 *  \code
 *  auto expr = _expr([_args...,] _filter() );
 *  \endcode   
 *
 *  @note Internal use only
 *
 *  @{
 */
#define  _TEST_UNARY_GATE_FILTER(x) EVAL0(__TEST_UNARY_GATE_FILTER x)
#define __TEST_UNARY_GATE_FILTER(_str, _expr, _fstr, _filter, _args...) \
  try {                                                                 \
    auto expr = _expr(_args IFN(_args)(IFN(_filter())(,)) _filter());   \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (!std::is_same<decltype(expr), decltype(init())>::value          \
        )                                                               \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = " _fstr "\n"                              \
                  "|   expr = " _fstr "\n");                            \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = QFilter\n");                              \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }

/** @}
 *  @brief Macro creates a unit test for the given expression in
 *  combination with the given filter and gate expressions, i.e.
 * 
 *  \code
 *   auto expr = _expr([_args...,] _filter( _gate() ) );
 *  \endcode   
 *
 *  @note Internal use only
 *
 *  @{
 */
#define  _TEST_UNARY_GATE_FILTER_GATE(x) EVAL0(__TEST_UNARY_GATE_FILTER_GATE x)
#define __TEST_UNARY_GATE_FILTER_GATE(_str, _expr, _fstr, _filter, _gstr, _gate, _args...) \
  try {                                                                 \
    auto expr = _expr(IFN(_args)(_args,) _filter(_gate()));             \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    std::cout << _expr(_args) << "<<>>" << _gate() << std::endl;        \
    std::cout << std::is_same<decltype(_expr(_args)), decltype(_gate())>::value << std::endl; \
    show<99>(expr);                                                     \
    if (LibKet::optimize_gates::value                                   \
        &&                                                              \
        std::is_same<decltype(_expr(_args)), decltype(_gate())>::value) \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = " _fstr "\n"                              \
                  "|   expr = QFilter\n");                              \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = " _fstr "\n"                              \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _gstr "\n"                   \
                  "|          | filter = " _fstr "\n"                   \
                  "|          |   expr = QFilter\n");                   \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }
///@}

/**
   @brief Macro creates unit tests for unary gates (no optimization rules)

   These tests check that the expression in combination with filters
   and other gates produces the correct abstract syntax tree
   representation.

   These tests do not check any cases where optimization rules apply.
 */
#define TEST_UNARY_GATE(_str, _expr, _args...)                          \
  MAP(_TEST_UNARY_GATE_FILTER,                                          \
      (_str, _expr, "QFilter",                   EVAL0,          _args), \
      (_str, _expr, "QFilterSelectAll",          (all),          _args), \
      (_str, _expr, "QFilterSelect [ 0 1 3 5 ]", (sel<0,1,3,5>), _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (range<0,3>),   _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (qureg<0,4>),   _args), \
      (_str, _expr, "QFilterSelect [ 1 ]",       (qubit<1>),     _args) \
      );                                                                \
 MAP(_TEST_UNARY_GATE_FILTER_GATE,                                      \
      (_str, _expr, "QFilterSelectAll",          EVAL0,          "QInit", init, _args), \
      (_str, _expr, "QFilterSelectAll",          (all),          "QInit", init, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 3 5 ]", (sel<0,1,3,5>), "QInit", init, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (range<0,3>),   "QInit", init, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (qureg<0,4>),   "QInit", init, _args), \
      (_str, _expr, "QFilterSelect [ 1 ]",       (qubit<1>),     "QInit", init, _args) \
     );                                                                 \
 MAP(_TEST_UNARY_GATE_FILTER_GATE,                                      \
      (_str, _expr, "QFilter",                   EVAL0,          "QReset", reset, _args), \
      (_str, _expr, "QFilterSelectAll",          (all),          "QReset", reset, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 3 5 ]", (sel<0,1,3,5>), "QReset", reset, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (range<0,3>),   "QReset", reset, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (qureg<0,4>),   "QReset", reset, _args), \
      (_str, _expr, "QFilterSelect [ 1 ]",       (qubit<1>),     "QReset", reset, _args) \
      );                                                                \
                                                                        \
  try {                                                                 \
    auto expr = _expr(IFN(_args)(_args,) init());                       \
    CHECK(run(expr));                                                   \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }

/**
   @brief Macro creates unit tests for unary gates (double application
   yields identity) for the given expression in combination with the
   given filter expression, i.e.

   \code
   auto expr = _expr([_args...,] _expr([_args...,] _filter() )); // -> id
   \endcode
*/
#define  _TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER(x) EVAL0(__TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER x)
#define __TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER(_str, _expr, _fstr, _filter, _args...) \
  try {                                                                 \
    auto expr = _expr(IFN(_args)(_args,)                                \
                      _expr(IFN(_args)(IFN(_filter())(,)) _filter() )); \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (LibKet::optimize_gates::value)                                  \
      CHECK_EQUAL(ss.str(), _fstr "\n");                                \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = " _fstr "\n"                              \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str "\n"                    \
                  "|          | filter = " _fstr "\n"                   \
                  "|          |   expr = " _fstr "\n");                 \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }

/**
   @brief Macro creates unit tests for unary gates (double application
   yields identity) for the given expression in combination with the
   given filter and gate expressions, i.e.

   \code
   auto expr = _expr([_args...,] _expr([_args...,] _filter( _gate() ) )); // -> _gate()
   \endcode
*/
#define  _TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER_GATE(x) EVAL0(__TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER_GATE x)
#define __TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER_GATE(_str, _expr, _fstr, _filter, _gstr, _gate, _args...) \
  try {                                                                 \
    auto expr = _expr(IFN(_args)(_args,)                                \
                      _expr(IFN(_args)(_args,) _filter(_gate()) ));     \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (LibKet::optimize_gates::value)                                  \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _gstr "\n"                              \
                  "| filter = " _fstr "\n"                              \
                  "|   expr = QFilter\n");                              \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = " _fstr "\n"                              \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str "\n"                    \
                  "|          | filter = " _fstr "\n"                   \
                  "|          |   expr = UnaryQGate\n"                  \
                  "|          |          |   gate = " _gstr "\n"        \
                  "|          |          | filter = " _fstr "\n"        \
                  "|          |          |   expr = QFilter\n");        \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }

/**
   @brief Macro creates unit tests for unary gates (double application yields identity)

   These tests check that the expression in combination with filters
   and other gates produces the correct abstract syntax tree
   representation.

   These tests perform all tests from the TEST_UNARY_GATE macro and,
   additionally, check that the double application of the same unary
   gate yields the identity, e.g. hadamard(hadamard()) = id.
 */
#define TEST_UNARY_GATE_OPTIMIZE_IDENTITY(_str, _expr, _args...)        \
  TEST_UNARY_GATE(_str, _expr, _args);                                  \
  MAP(_TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER,                        \
      (_str, _expr, "QFilter",                   EVAL0,          _args), \
      (_str, _expr, "QFilterSelectAll",          (all),          _args), \
      (_str, _expr, "QFilterSelect [ 0 1 3 5 ]", (sel<0,1,3,5>), _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (range<0,3>),   _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (qureg<0,4>),   _args), \
      (_str, _expr, "QFilterSelect [ 1 ]",       (qubit<1>),     _args)); \
  MAP(_TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER_GATE,                   \
      (_str, _expr, "QFilterSelectAll",          EVAL0,          "QInit", init, _args), \
      (_str, _expr, "QFilterSelectAll",          (all),          "QInit", init, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 3 5 ]", (sel<0,1,3,5>), "QInit", init, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (range<0,3>),   "QInit", init, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (qureg<0,4>),   "QInit", init, _args), \
      (_str, _expr, "QFilterSelect [ 1 ]",       (qubit<1>),     "QInit", init, _args)); \
  MAP(_TEST_UNARY_GATE_OPTIMIZE_IDENTITY_FILTER_GATE,                   \
      (_str, _expr, "QFilter",                   EVAL0,          "QReset", reset, _args), \
      (_str, _expr, "QFilterSelectAll",          (all),          "QReset", reset, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 3 5 ]", (sel<0,1,3,5>), "QReset", reset, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (range<0,3>),   "QReset", reset, _args), \
      (_str, _expr, "QFilterSelect [ 0 1 2 3 ]", (qureg<0,4>),   "QReset", reset, _args), \
      (_str, _expr, "QFilterSelect [ 1 ]",       (qubit<1>),     "QReset", reset, _args));


// Create tests for unary gates (double application yields single gate)
#define TEST_UNARY_GATE_OPTIMIZE_SINGLE(_str, _expr, _args...)          \
  TEST_UNARY_GATE(_str, _expr, _args);                                  \
  try {                                                                 \
  auto expr = _expr(IFN(_args)(_args,)                                  \
                    _expr(IFN(_args)(_args)));                          \
  std::stringstream ss;                                                 \
  show<99>(expr, ss);                                                   \
  if (LibKet::optimize_gates::value)                                    \
    if (!std::is_same<decltype(_expr(_args)), decltype(init())>::value) \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = QFilter\n"                                \
                  "|   expr = QFilter\n");                              \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = QFilter\n");                              \
  else                                                                  \
    if (!std::is_same<decltype(_expr(_args)), decltype(init())>::value) \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = QFilter\n"                                \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str "\n"                    \
                  "|          | filter = QFilter\n"                     \
                  "|          |   expr = QFilter\n");                   \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str "\n"                    \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = QFilter\n");                   \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr(IFN(_args)(_args,)                                \
                      _expr(IFN(_args)(_args,) init()));                \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (LibKet::optimize_gates::value)                                  \
      if (!std::is_same<decltype(_expr(_args)), decltype(init())>::value) \
        CHECK_EQUAL(ss.str(),                                           \
                    "UnaryQGate\n"                                      \
                    "|   gate = " _str "\n"                             \
                    "| filter = QFilterSelectAll\n"                     \
                    "|   expr = UnaryQGate\n"                           \
                    "|          |   gate = QInit\n"                     \
                    "|          | filter = QFilterSelectAll\n"          \
                    "|          |   expr = QFilter\n");                 \
      else                                                              \
        CHECK_EQUAL(ss.str(),                                           \
                    "UnaryQGate\n"                                      \
                    "|   gate = " _str "\n"                             \
                    "| filter = QFilterSelectAll\n"                     \
                    "|   expr = QFilter\n");                            \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str "\n"                               \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str "\n"                    \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = UnaryQGate\n"                  \
                  "|          |          |   gate = QInit\n"            \
                  "|          |          | filter = QFilterSelectAll\n" \
                  "|          |          |   expr = QFilter\n");        \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }

// Create tests for unary gate (two consecutive gates without parameters yield identity)
#define TEST_UNARY_GATE_OPTIMIZE_DUO_NOARG_IDENTITY(_str0, _expr0, _str1, _expr1) \
  TEST_UNARY_GATE(_str0, _expr0);                                       \
  TEST_UNARY_GATE(_str1, _expr1);                                       \
  try {                                                                 \
    auto expr = _expr0(_expr1());                                       \
  std::stringstream ss;                                                 \
  show<99>(expr, ss);                                                   \
  if (LibKet::optimize_gates::value)                                    \
    CHECK_EQUAL(ss.str(), "QFilter\n");                                 \
  else                                                                  \
    CHECK_EQUAL(ss.str(),                                               \
                "UnaryQGate\n"                                          \
                "|   gate = " _str0 "\n"                                \
                "| filter = QFilter\n"                                  \
                "|   expr = UnaryQGate\n"                               \
                "|          |   gate = " _str1 "\n"                     \
                "|          | filter = QFilter\n"                       \
                "|          |   expr = QFilter\n");                     \
  } catch (const std::exception& e) {                                   \
      std::cerr << e.what() << std::endl;                               \
      return;                                                           \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr1(_expr0());                                       \
  std::stringstream ss;                                                 \
  show<99>(expr, ss);                                                   \
  if (LibKet::optimize_gates::value)                                    \
    CHECK_EQUAL(ss.str(), "QFilter\n");                                 \
  else                                                                  \
    CHECK_EQUAL(ss.str(),                                               \
                "UnaryQGate\n"                                          \
                "|   gate = " _str1 "\n"                                \
                "| filter = QFilter\n"                                  \
                "|   expr = UnaryQGate\n"                               \
                "|          |   gate = " _str0 "\n"                     \
                "|          | filter = QFilter\n"                       \
                "|          |   expr = QFilter\n");                     \
  } catch (const std::exception& e) {                                   \
      std::cerr << e.what() << std::endl;                               \
      return;                                                           \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr0(_expr1(init()));                                 \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (LibKet::optimize_gates::value)                                  \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = QInit\n"                                  \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = QFilter\n");                              \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str0 "\n"                              \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str1 "\n"                   \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = UnaryQGate\n"                  \
                  "|          |          |   gate = QInit\n"            \
                  "|          |          | filter = QFilterSelectAll\n" \
                  "|          |          |   expr = QFilter\n");        \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr1(_expr0(init()));                                 \
    std::stringstream ss;                                               \
      show<99>(expr, ss);                                               \
      if (LibKet::optimize_gates::value)                                \
        CHECK_EQUAL(ss.str(),                                           \
                    "UnaryQGate\n"                                      \
                    "|   gate = QInit\n"                                \
                    "| filter = QFilterSelectAll\n"                     \
                    "|   expr = QFilter\n");                            \
      else                                                              \
        CHECK_EQUAL(ss.str(),                                           \
                    "UnaryQGate\n"                                      \
                    "|   gate = " _str1 "\n"                            \
                    "| filter = QFilterSelectAll\n"                     \
                    "|   expr = UnaryQGate\n"                           \
                    "|          |   gate = " _str0 "\n"                 \
                    "|          | filter = QFilterSelectAll\n"          \
                    "|          |   expr = UnaryQGate\n"                \
                    "|          |          |   gate = QInit\n"          \
                    "|          |          | filter = QFilterSelectAll\n" \
                    "|          |          |   expr = QFilter\n");      \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }

// Create tests for unary gate (two consecutive gates with parameters yield identity)
#define TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_IDENTITY(_str0, _expr0, _arg0, \
                                                  _str1, _expr1, _arg1) \
  TEST_UNARY_GATE(_str0, _expr0, _arg0);                                \
  TEST_UNARY_GATE(_str1, _expr1, _arg1);                                \
                                                                        \
  try {                                                                 \
    auto expr = _expr0(_arg0, _expr1(_arg1));                           \
  std::stringstream ss;                                                 \
  show<99>(expr, ss);                                                   \
  if (LibKet::optimize_gates::value)                                    \
    CHECK_EQUAL(ss.str(), "QFilter\n");                                 \
  else                                                                  \
    CHECK_EQUAL(ss.str(),                                               \
                "UnaryQGate\n"                                          \
                "|   gate = " _str0 "\n"                                \
                "| filter = QFilter\n"                                  \
                "|   expr = UnaryQGate\n"                               \
                "|          |   gate = " _str1 "\n"                     \
                "|          | filter = QFilter\n"                       \
                "|          |   expr = QFilter\n");                     \
  } catch (const std::exception& e) {                                   \
      std::cerr << e.what() << std::endl;                               \
      return;                                                           \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr1(_arg1, _expr0(_arg0));                           \
  std::stringstream ss;                                                 \
  show<99>(expr, ss);                                                   \
  if (LibKet::optimize_gates::value)                                    \
    CHECK_EQUAL(ss.str(), "QFilter\n");                                 \
  else                                                                  \
    CHECK_EQUAL(ss.str(),                                               \
                "UnaryQGate\n"                                          \
                "|   gate = " _str1 "\n"                                \
                "| filter = QFilter\n"                                  \
                "|   expr = UnaryQGate\n"                               \
                "|          |   gate = " _str0 "\n"                     \
                "|          | filter = QFilter\n"                       \
                "|          |   expr = QFilter\n");                     \
  } catch (const std::exception& e) {                                   \
      std::cerr << e.what() << std::endl;                               \
      return;                                                           \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr0(_arg0, _expr1(_arg1, init()));                   \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (LibKet::optimize_gates::value)                                  \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = QInit\n"                                  \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = QFilter\n");                              \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str0 "\n"                              \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str1 "\n"                   \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = UnaryQGate\n"                  \
                  "|          |          |   gate = QInit\n"            \
                  "|          |          | filter = QFilterSelectAll\n" \
                  "|          |          |   expr = QFilter\n");        \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr1(_arg1, _expr0(_arg0, init()));                   \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (LibKet::optimize_gates::value)                                  \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = QInit\n"                                  \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = QFilter\n");                              \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str1 "\n"                              \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str0 "\n"                   \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = UnaryQGate\n"                  \
                  "|          |          |   gate = QInit\n"            \
                  "|          |          | filter = QFilterSelectAll\n" \
                  "|          |          |   expr = QFilter\n");        \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }

// Create tests for unary gate (two consecutive gates with parameters yield single gate)
#define TEST_UNARY_GATE_OPTIMIZE_DUO_ARG_SINGLE(_str0, _expr0, _arg0,   \
                                                _str1, _expr1, _arg1,   \
                                                _str_out0, _str_out1)   \
  TEST_UNARY_GATE(_str0, _expr0, _arg0);                                \
  TEST_UNARY_GATE(_str1, _expr1, _arg1);                                \
                                                                        \
  try {                                                                 \
    auto expr = _expr0(_arg0, _expr1(_arg1));                           \
  std::stringstream ss;                                                 \
  show<99>(expr, ss);                                                   \
  if (LibKet::optimize_gates::value)                                    \
    CHECK_EQUAL(ss.str(),                                               \
                "UnaryQGate\n"                                          \
                "|   gate = " _str_out0 "\n"                            \
                "| filter = QFilter\n"                                  \
                "|   expr = QFilter\n");                                \
  else                                                                  \
    CHECK_EQUAL(ss.str(),                                               \
                "UnaryQGate\n"                                          \
                "|   gate = " _str0 "\n"                                \
                "| filter = QFilter\n"                                  \
                "|   expr = UnaryQGate\n"                               \
                "|          |   gate = " _str1 "\n"                     \
                "|          | filter = QFilter\n"                       \
                "|          |   expr = QFilter\n");                     \
  } catch (const std::exception& e) {                                   \
      std::cerr << e.what() << std::endl;                               \
      return;                                                           \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr1(_arg1, _expr0(_arg0));                           \
  std::stringstream ss;                                                 \
  show<99>(expr, ss);                                                   \
  if (LibKet::optimize_gates::value)                                    \
    CHECK_EQUAL(ss.str(),                                               \
                "UnaryQGate\n"                                          \
                "|   gate = " _str_out1 "\n"                            \
                "| filter = QFilter\n"                                  \
                "|   expr = QFilter\n");                                \
  else                                                                  \
    CHECK_EQUAL(ss.str(),                                               \
                "UnaryQGate\n"                                          \
                "|   gate = " _str1 "\n"                                \
                "| filter = QFilter\n"                                  \
                "|   expr = UnaryQGate\n"                               \
                "|          |   gate = " _str0 "\n"                     \
                "|          | filter = QFilter\n"                       \
                "|          |   expr = QFilter\n");                     \
  } catch (const std::exception& e) {                                   \
      std::cerr << e.what() << std::endl;                               \
      return;                                                           \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr0(_arg0, _expr1(_arg1, init()));                   \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (LibKet::optimize_gates::value)                                  \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str_out0 "\n"                          \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = QInit\n"                       \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = QFilter\n");                   \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str0 "\n"                              \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str1 "\n"                   \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = UnaryQGate\n"                  \
                  "|          |          |   gate = QInit\n"            \
                  "|          |          | filter = QFilterSelectAll\n" \
                  "|          |          |   expr = QFilter\n");        \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }                                                                     \
                                                                        \
  try {                                                                 \
    auto expr = _expr1(_arg1, _expr0(_arg0, init()));                   \
    std::stringstream ss;                                               \
    show<99>(expr, ss);                                                 \
    if (LibKet::optimize_gates::value)                                  \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str_out1 "\n"                          \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = QInit\n"                       \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = QFilter\n");                   \
    else                                                                \
      CHECK_EQUAL(ss.str(),                                             \
                  "UnaryQGate\n"                                        \
                  "|   gate = " _str1 "\n"                              \
                  "| filter = QFilterSelectAll\n"                       \
                  "|   expr = UnaryQGate\n"                             \
                  "|          |   gate = " _str0 "\n"                   \
                  "|          | filter = QFilterSelectAll\n"            \
                  "|          |   expr = UnaryQGate\n"                  \
                  "|          |          |   gate = QInit\n"            \
                  "|          |          | filter = QFilterSelectAll\n" \
                  "|          |          |   expr = QFilter\n");        \
  } catch (const std::exception& e) {                                   \
    std::cerr << e.what() << std::endl;                                 \
    return;                                                             \
  }

#endif // UNARY_HPP
