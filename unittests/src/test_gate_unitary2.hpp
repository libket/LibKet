/** @file test_gate_unitary2.hpp
 *
 *  @brief LibKet::gates::unitary2() unittests
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_unitary2)
{
  TEST_API_CONSISTENCY( unitary2<LibKet::u2_ftor> );
  TEST_API_CONSISTENCY( UNITARY2<LibKet::u2_ftor> );
  TEST_API_CONSISTENCY( u2<LibKet::u2_ftor>       );
  TEST_API_CONSISTENCY( U2<LibKet::u2_ftor>       );
  
  TEST_API_CONSISTENCY( QUnitary2<LibKet::u2_ftor>() );

  TEST_UNARY_GATE( "QUnitary2<4729080009990889727,QConst1_t(0)>", unitary2<LibKet::u2_ftor> );
  TEST_UNARY_GATE( "QUnitary2<4729080009990889727,QConst1_t(0)>", UNITARY2<LibKet::u2_ftor> );
  TEST_UNARY_GATE( "QUnitary2<4729080009990889727,QConst1_t(0)>", u2<LibKet::u2_ftor> );
  TEST_UNARY_GATE( "QUnitary2<4729080009990889727,QConst1_t(0)>", U2<LibKet::u2_ftor> );

  TEST_UNARY_GATE( "QUnitary2<4729080009990889727,QConst1_t(0)>", QUnitary2<LibKet::u2_ftor>() );  
}
