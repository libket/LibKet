/** @file c_api/QJob.cpp

@brief C API quantum job execution class implementation

@copyright This file is part of the LibKet library (C API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller

@ingroup c_api
*/

#include <stdlib.h>

#include <QJob.h>
#include <QBase.hpp>
#include <QJob.hpp>

/**
   @brief Quantum job class specialization for Python executor
   
   The LibKet quantum job class implements a quantum job that can be
   executed within the LibKet quantum stream class for the Python
   executor

   @ingroup c_api
*/
struct qPythonJob {
  void *obj;
};

/**
   @brief Creates a Python executor job

   @ingroup c_api
*/
qError_t qPythonJobCreate(qPythonJob_t* job,
                          const char* script,
                          const char* method_run,
                          const char* method_wait,
                          const char* method_query,
                          PyObject* module,
                          PyObject* global,
                          PyObject* local,
                          const qPythonJob_t* depend = NULL)
{
  LibKet::QJob<LibKet::QJobType::Python> * obj;
  
  job = (__typeof__(job))malloc(sizeof(*job));
  obj = new LibKet::QJob<LibKet::QJobType::Python>(std::string(script),
                                                    std::string(method_run),
                                                    std::string(method_wait),
                                                    std::string(method_query),
                                                    module,
                                                    global,
                                                    local,
                                                    static_cast<LibKet::QJob<LibKet::QJobType::Python> *>(depend->obj));
  job->obj = obj;
  
  return qSuccess;
}

/**
   @brief Destroys a Python executor job

   @ingroup c_api
*/
qError_t qPythonJobDestroy(qPythonJob_t* job)
{
  if (job==NULL)
    return qErrorInvalidValue;
  
  delete static_cast<LibKet::QJob<LibKet::QJobType::Python> *>(job->obj);
  free(job);

  return qSuccess;
}

/**
   @brief (Re-)uns a Python executor job

   @ingroup c_api
*/
qError_t qPythonJobRun(qPythonJob_t* job, qPythonJob_t* depend = NULL)
{
  if (job==NULL)
    return qErrorInvalidValue;

  static_cast<LibKet::QJob<LibKet::QJobType::Python> *>(job->obj)
    ->run(static_cast<LibKet::QJob<LibKet::QJobType::Python> *>(depend->obj));

  return qSuccess;
}

/**
   @brief Waits for Python executor job to complete and returns the result as JSON object

   @ingroup c_api
*/
qError_t qPythonJobGet(const qPythonJob_t* job /* C JSON*/)
{
  if (job==NULL)
    return qErrorInvalidValue;

  /* TODO */

  return qErrorNotYetImplemented;
}

/**
   @brief Waits for Python executor job to complete

   @ingroup c_api
*/
qError_t qPythonJobWait(const qPythonJob_t* job)
{
  if (job==NULL)
    return qErrorInvalidValue;

  static_cast<LibKet::QJob<LibKet::QJobType::Python> *>(job->obj)
    ->wait();

  return qSuccess;
}

/**
   @brief Returns qSuccess if Python executor job has completed, or qErrorNotReady if not

   @ingroup c_api
*/
qError_t qPythonJobQuery(const qPythonJob_t* job)
{
  if (job==NULL)
    return qErrorInvalidValue;

  //return (static_cast<LibKet::QJob<LibKet::QJobType::Python> *>(job->obj)
  //->query() ? qSuccess : qErrorNotReady);
  return qErrorNotYetImplemented;
}

/**
   @brief Returns the duration of the Python executor job

   @ingroup c_api
*/
qError_t qPythonJobDuration(const qPythonJob_t* job, double& count)
{
  if (job==NULL)
    return qErrorInvalidValue;

  count = static_cast<LibKet::QJob<LibKet::QJobType::Python> *>(job->obj)
    ->duration().count();
  
  return qSuccess;
}

/**
   @brief Quantum job class specialization for C++ executor
   
   The LibKet quantum job class implements a quantum job that can be
   executed within the LibKet quantum stream class for the C++
   executor

   @ingroup c_api
*/
struct qCppJob {
  void *obj;
};

/**
   @brief Creates a C++ executor job

   @ingroup c_api
*/
qError_t qCppJobCreate(qCppJob_t* job)
{
  LibKet::QJob<LibKet::QJobType::CXX> * obj;
  
  job      = (__typeof__(job))malloc(sizeof(*job));
  //obj    = new LibKet::QJob<LibKet::QJobType::CXX>("","","","");
  job->obj = obj;
  
  return qSuccess;
}

/**
   @brief Destroys a C++ executor job

   @ingroup c_api
*/
qError_t qCppJobDestroy(qCppJob_t* job)
{
  if (job==NULL)
    return qErrorInvalidValue;
  
  delete static_cast<LibKet::QJob<LibKet::QJobType::CXX> *>(job->obj);
  free(job);

  return qSuccess;
}

/**
   @brief (Re-)uns a C++ executor job

   @ingroup c_api
*/
qError_t qCppJobRun(const qCppJob_t* job, qCppJob_t* depend = NULL)
{
  if (job==NULL)
    return qErrorInvalidValue;

  static_cast<LibKet::QJob<LibKet::QJobType::CXX> *>(job->obj)
    ->run(static_cast<LibKet::QJob<LibKet::QJobType::CXX> *>(depend->obj));

  return qSuccess;
}

/**
   @brief Waits for C++ executor job to complete

   @ingroup c_api
*/
qError_t qCppJobWait(const qCppJob_t* job)
{
  if (job==NULL)
    return qErrorInvalidValue;

  static_cast<LibKet::QJob<LibKet::QJobType::CXX> *>(job->obj)
    ->wait();

  return qSuccess;
}

/**
   @brief Returns qSuccess if C++ executor job has completed, or qErrorNotReady if not

   @ingroup c_api
*/
qError_t qCppJobQuery(const qCppJob_t* job)
{
  if (job==NULL)
    return qErrorInvalidValue;

  //return (static_cast<LibKet::QJob<LibKet::QJobType::CXX> *>(job->obj)
  //->query() ? qSuccess : qErrorNotReady);
  return qErrorNotYetImplemented;
}

/**
   @brief Returns the duration of the C++ executor job

   @ingroup c_api
*/
qError_t qCppJobDuration(const qCppJob_t* job, double& count)
{
  if (job==NULL)
    return qErrorInvalidValue;

  count = static_cast<LibKet::QJob<LibKet::QJobType::CXX> *>(job->obj)
    ->duration().count();
  
  return qSuccess;
}
