/** @file c_api/QFilters.h

@brief C API quantum filter classes declaration

@copyright This file is part of the LibKet library (C API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller

@ingroup c_api
*/

#pragma once
#ifndef C_API_QFILTERS_H
#define C_API_QFILTERS_H

#include <QBase.h>
#include <QConfig.h>

#ifdef __cplusplus
extern "C" {
#endif

  // ...

#ifdef __cplusplus
}
#endif

#endif // C_API_QFILTERS_H
