/** @file examples/tutorial07_arb_ctrl.cpp

    @brief C++ tutorial-07: Quantum expression with arbitrary control gate

    This tutorial illustrates the usage of the arbitrary control gate

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{
  // Set number of qubits
  const size_t nqubits = 8;

  // Create quantum expression
  auto expr = measure(
                      all(
                          arb_ctrl<>(
                                     cx(),
                                     sel<0,1,2,3>(),
                                     sel<4>(
                                            
                                            ),
                                     sel<5,6,7>(
                                                init()
                                                )
                                     )
                          )
                      );

  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, expr, 1024, result);
  
  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}
