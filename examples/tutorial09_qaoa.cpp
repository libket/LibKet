/** @file examples/tutorial09_qaoa.cpp

    @brief C++ tutorial-09: Quantum Approximate Optimization Algorithm.
           This tutorial shows how to create a QAOA circuit for the
           maxcut problem on an arbitrary graph.

    @copyright This file is part of the LibKet library.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller, Huub Donkers
*/

#include <iostream>
#include <LibKet.hpp>
#include <nlopt.hpp>

#define PI  3.14159265358979323846  /* pi */

// Import LibKet namespaces
using namespace LibKet; 
using namespace LibKet::circuits;

struct Graph{
      // Create compile-time graph
    static utils::graph<>
      ::edge<0,1>
      ::edge<1,2>
      ::edge<2,3>
      ::edge<3,4>
      ::edge<4,0> g;
};

struct QAOA{

  //Maxcut ZZ-loop
  template<index_t start, index_t end, index_t step, index_t index>
  struct maxcut_cost_function
  {  
    template<typename  Expr, typename Graph, typename Gamma>
    inline constexpr auto operator()(Expr&& expr, Graph, Gamma) noexcept
    {
      auto cnot1 = cnot(sel<Graph::template from<index>()>(gototag<0>()),
                        sel<Graph::template to<index>()>(gototag<0>(expr))
                        );

      auto rot_z_gamma = rz(Gamma{}, sel<1>(cnot1));

      auto cnot2 = cnot(sel<Graph::template from<index>()>(gototag<0>()),
                        sel<Graph::template to<index>()>(gototag<0>(rot_z_gamma))
                        );

      return gototag<0>(cnot2);         
    }
  };

  //Function to generate maxcut circuit for one P iteration
   template<typename Graph, typename Params>
   auto genMaxCutCircuit(Graph, Params&& params){
    
    //Set circuits parameters
    auto beta = QVar(params[0]*2*PI);
    auto gamma = QVar(params[1]*2*PI);
      
    //Set initial state to superposition
    auto init_state = all(h(init()));

    //Cost unitaries
    auto cost = utils::constexpr_for<0, Graph::size()-1, 1, maxcut_cost_function>(tag<0>(init_state), Graph{}, gamma);

    //Mixer unitaries
    auto mixer = rx(beta, cost);  

    //Measure result
    auto qaoa_circuit = measure(mixer);

    return qaoa_circuit;

   }

    //Loop to determine number of shared edges
    template<index_t start, index_t end, index_t step, index_t index>
    struct shared_edge_loop
    {  
      template<typename Shared, typename Graph, typename BitString>
      inline constexpr auto operator()(Shared&& shared, Graph, BitString bitString) noexcept
      {
        if(bitString[Graph::template from<index>()] != bitString[Graph::template to<index>()]){
          shared -= 1;
        }

        return shared;         
      }
    };

   //Convert Histogram output to expectation value
   template<typename graphType, typename histType>
   float histToExp(graphType graph, histType hist) {

    static constexpr const std::size_t numEdges = graph.size();
    static constexpr const std::size_t numStates = hist.size();
    static constexpr const std::size_t numBits = 2;

    int average = 0;
    int total_count = 0;
    //Loop over all output states
    for(int i = 0; i < numStates; i++){
      
      //Convert state number to bitstring
      std::string bitString = std::bitset<numBits>(i).to_string();

      //Loop over all egdes
      int shared = 0;
      int shared_edges = utils::constexpr_for<0, numEdges-1, 1, shared_edge_loop>(shared, graph, bitString);

      //Sum shared edges with histogram weight
      average += shared_edges * hist[i];
      total_count += hist[i];
    }

    //Return average result
    return (float)average / (float)total_count;
  }

   template<typename Graph, typename Params>
   float runQAOA(Graph g, Params&& params){
     
     //Generate maxcut circuit
     auto circuit = genMaxCutCircuit(g, params);

     //Execute circuit on quantum backend
     QDevice<QDeviceType::qiskit_aer_simulator, 5> qpu;

     //Load ansatz onto QPU and execute
     qpu(circuit);
     nlohmann::json result = qpu.eval(4096);
     auto hist = qpu.get<QResultType::histogram>(result);

     //Get expectation value from histogram
     auto expectation = histToExp(g, hist);

     return expectation;
   }

};

//NLOpt optimization function
static double vqa_opt(const std::vector<double> &params, std::vector<double> &grad, void *func_data)
{

   //Get VQE struct
   QAOA qaoa;

   //Get graph
   Graph graph;

   //Run VQE and get expectation function
   double expectation = qaoa.runQAOA(graph.g, params);

   //Iteration message:
   std::cout << "Optimizing expectation: " << expectation << std::endl;

   return expectation;
}

int main(int argc, char *argv[])
{  
   //Set U3 parameters   
   std::size_t numParams = 2;

   //Set NLOPT variables     
   double _minf;
   std::vector<double> _opt_params;

   //NLopt optimizer initialisation    
   std::vector<double> lb;
   std::vector<double> ub;
   std::vector<double> params;
   float tol = 1e-3;
   
   //Fill in parameters based on algorithm   
   for(int i=0; i < numParams; i++){
            lb.push_back(0.0);      //Set lower bounds to 0.0
            ub.push_back(1.0);      //Set upper bounds to 1.0
            params.push_back(0.5);  //Set initial pararms to 0.5
          } 

   struct OptData{} optData;

   //Set parameters
   nlopt::opt opt(nlopt::LN_COBYLA, numParams);
   opt.set_lower_bounds(lb);
   opt.set_upper_bounds(ub);
   opt.set_min_objective(vqa_opt, &optData); 
   opt.set_xtol_rel(tol);   

   try{
       nlopt::result result = opt.optimize(params, _minf);
       _opt_params = params;
   }
   catch(std::exception &e) {
       std::cout << "nlopt failed: " << e.what() << std::endl;
   }

   std::cout << "QAOA Expecation: " << _minf << std::endl;


   //Rerun QAOA with optimal parameters, print circuit and histogram
   QDevice<QDeviceType::qiskit_aer_simulator, 5> qpu;
   QAOA qaoa;
   Graph graph;

   qpu(qaoa.genMaxCutCircuit(graph.g, _opt_params));

   nlohmann::json result = qpu.eval(4096);

   std::cout << qpu.print_circuit() << std::endl;
   std::cout << "Histogram: " << qpu.get<QResultType::histogram>(result) << std::endl;
  
  return 0;
}