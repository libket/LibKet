/** @file examples/tutorial02_teleportation.cpp

    @brief C++ tutorial-02: Quantum Fourier transform

    This tutorial illustrates the basic usage of pre-implemeneted
    quantum circuits like the Quantum Fourier transform

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>
#include <LibKet.hpp>
#include "common.h"

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{
  // Set number of qubits
  const std::size_t nqubits = 3;
  
  // Create simple quantum expression
  auto bell_12 = all(cnot(sel<1>(),sel<2>(all(h(sel<1>(init()))))));            //Bell pair of bottom two qubits
  auto inv_bell_01 = all(h(sel<0>(all(cnot(sel<0>(),sel<1>(bell_12))))));       //Inverse bell transform of top two qubits
  auto measure_01 = all(measure(sel<0,1>(inv_bell_01)));                        //Measure top two qubits
  auto correct = cz(sel<0>(), sel<2>(all(cx(sel<1>(), sel<2>(measure_01)))));   //Use measurement results to correct bottom qubit

  // Create empty JSON object to receive results
  utils::json result;

  // Evaluate quantum expression on multiple devices
  EVAL_EXPR_SIMPLE(nqubits, correct, 1024, result);

  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";

  return 0;
}
