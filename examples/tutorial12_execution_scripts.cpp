/** @file examples/tutorial12_execution_scripts.cpp

    @brief C++ tutorial-05: Quantum executions script methods

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Huub Donkers
*/

#include <iostream>
#include <LibKet.hpp>

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

int main(int argc, char *argv[])
{
  // Set number of qubits and shots
  const std::size_t nqubits = 4;
  const std::size_t shots = 1024;

  //Contstruct simple quantum expression
  auto expr = measure(all(h(sel<0,2>(init()))));

  //Create new quantum device and load quantum expression into it
  QDevice<QDeviceType::qiskit_aer_simulator, nqubits> qpu;
  qpu(expr);

  //Create json object to store results
  utils::json result;

  //Synchronous execution with execution scripts
  //Historgram is saved in the build folder
  auto job = qpu.execute(shots,
                         /* init_script   */
                         "import base64\n"
                         "from qiskit.visualization import plot_histogram\n",
                         /* before_script */
                         "",
                         /* after_script */
                         "counts = result.get_counts()\n"
                         "plot_histogram(counts, color='midnightblue', title=\"Histogram\").savefig('histogram.png')\n"
                         ); 

  return 0;
}
