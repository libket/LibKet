/** @file python_api/PyQExpression.cpp

    @brief Python API quantum data class

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include "PyQExpression.hpp"

namespace py = pybind11;

/**
   @brief Quantum backend lookup table

   @ingroup py_api
 */
std::map<PyQBackendType, std::string> PyQBackendTypeMap = {
       {PyQBackendType::AQASM,      "PyQBackendType::AQASM"},
       {PyQBackendType::cQASMv1,    "PyQBackendType::cQASMv1"},
       {PyQBackendType::OpenQASMv2, "PyQBackendType::OpenQASMv2"},
       {PyQBackendType::OpenQL,     "PyQBackendType::OpenQL"},
       {PyQBackendType::QASM,       "PyQBackendType::QASM"},
       {PyQBackendType::Quil,       "PyQBackendType::Quil"},
       {PyQBackendType::QX,         "PyQBackendType::QX"}
};

std::string PyQExpression::print() const
{ return "<pylibket.data.PyQExpression qubits=" + std::to_string(qubits) + ", backend=" + PyQBackendTypeMap[backend] + ">"; }

/**
   @brief Creates the LibKet quantum data Python module

   @ingroup py_api
*/
void pybind11_init_data(pybind11::module &m)
{
  m.attr("__name__") = "pylibket.data";
  m.attr("__version__") = LIBKET_VERSION;
  m.doc() = "LibKet: the Quantum Expression Template Library";

  py::enum_<PyQBackendType>(m, "PyQBackendType")
    .value("AQASM",      PyQBackendType::AQASM)
    .value("cQASMv1",    PyQBackendType::cQASMv1)
    .value("OpenQASMv2", PyQBackendType::OpenQASMv2)
    .value("OpenQL",     PyQBackendType::OpenQL)
    .value("QASM",       PyQBackendType::QASM)
    .value("Quil",       PyQBackendType::Quil)
    .value("QX",         PyQBackendType::QX)
    .export_values();
  
  py::class_<PyQExpression, PyQBase, std::shared_ptr<PyQExpression>>(m, "PyQExpression")
    .def(py::init<>())
    .def(py::init<std::size_t>())
    .def(py::init<std::size_t, PyQBackendType>())
    .def("print", &PyQExpression::print)
    .def("__repr__",
         [](const PyQExpression &obj) {
           return obj.print();
         }
         )
    .def_readwrite("qubits", &PyQExpression::qubits)
    .def_readwrite("backend", &PyQExpression::backend);
}
