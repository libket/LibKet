/** @file python_api/PyQCircuits.cpp

@brief Python API quantum circuit classes

@copyright This file is part of the LibKet library (Python API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#include "PyQCircuits.hpp"

namespace py = pybind11;

/**
   @brief Creates the LibKet quantum circuit Python module

   @ingroup py_circuit
*/
void pybind11_init_circuit(py::module &m) {

  m.attr("__name__") = "pylibket.circuit";
  m.attr("__version__") = LIBKET_VERSION;
  m.doc() = "LibKet: the Quantum Expression Template Library";
  
  py::class_<PyQCircuit, PyQBase, std::shared_ptr<PyQCircuit>>(m, "PyQCircuit");
}
