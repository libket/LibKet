/** @file python_api/PyQGates.hpp

    @brief Python API quantum gate classes

    @defgroup py_gates Quantum gates

    @ingroup py_api
    
    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

*/

#pragma once
#ifndef PYTHON_API_QGATES_HPP
#define PYTHON_API_QGATES_HPP

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "PyQBase.hpp"

/**
   @brief BARRIER gate class

   @ingroup py_gates
*/
class PyQGateBarrier : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateBarrier() = default;

  /// @brief Constructor
  PyQGateBarrier(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateBarrier>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::barrier(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateBarrier> tmp =
      std::make_shared<PyQGateBarrier>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief CCNOT gate class

   @ingroup py_gates
*/
class PyQGateCCNOT : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateCCNOT() = default;

  /// @brief Constructor
  PyQGateCCNOT(std::shared_ptr<PyQObject> ctrl0,
               std::shared_ptr<PyQObject> ctrl1,
               std::shared_ptr<PyQObject> obj)
    : _ctrl0(ctrl0)
    , _ctrl1(ctrl1)
    , PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateCCNOT>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::ccnot(" + _ctrl0->print() + "," + _ctrl1->print() +
           "," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateCCNOT apply(std::shared_ptr<PyQObject> ctrl0,
                     std::shared_ptr<PyQObject> ctrl1,
                     std::shared_ptr<PyQObject> obj)
  {
    _ctrl0 = ctrl0;
    _ctrl1 = ctrl1;
    _obj = obj;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateCCNOT> tmp = std::make_shared<PyQGateCCNOT>(*this);
    tmp->_obj = _obj->duplicate();
    tmp->_ctrl0 = _ctrl0->duplicate();
    tmp->_ctrl1 = _ctrl1->duplicate();
    return tmp;
  }

private:
  std::shared_ptr<PyQObject> _ctrl0, _ctrl1;
};

/**
   @brief CNOT gate class

   @ingroup py_gates
*/
class PyQGateCNOT : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateCNOT() = default;

  /// @brief Constructor
  PyQGateCNOT(std::shared_ptr<PyQObject> ctrl, std::shared_ptr<PyQObject> obj)
    : _ctrl(ctrl)
    , PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateCNOT>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::cnot(" + _ctrl->print() + "," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateCNOT apply(std::shared_ptr<PyQObject> ctrl,
                    std::shared_ptr<PyQObject> obj)
  {
    _ctrl = ctrl;
    _obj = obj;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateCNOT> tmp = std::make_shared<PyQGateCNOT>(*this);
    tmp->_ctrl = _ctrl->duplicate();
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

private:
  std::shared_ptr<PyQObject> _ctrl;
};

/**
   @brief CPHASE gate class

   @ingroup py_gates
*/
class PyQGateCPhase : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateCPhase(const std::string& angle = "0.0",
                const std::string& tol = "0.0")
    : _ctrl()
    , PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateCPhase(std::shared_ptr<PyQObject> ctrl,
                std::shared_ptr<PyQObject> obj,
                const std::string& angle = "0.0",
                const std::string& tol = "0.0")
    : _ctrl(ctrl)
    , PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateCPhase>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::cphase<QConst_t(" + tol + ")>(QConst(" + angle +
           ")," + _ctrl->print() + "," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateCPhase apply(std::shared_ptr<PyQObject> ctrl,
                      std::shared_ptr<PyQObject> obj)
  {
    _ctrl = ctrl;
    _obj = obj;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateCPhase> tmp = std::make_shared<PyQGateCPhase>(*this);
    tmp->_ctrl = _ctrl->duplicate();
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;

private:
  std::shared_ptr<PyQObject> _ctrl;
};

/**
   @brief CPHASEDAG gate class

   @ingroup py_gates
*/
class PyQGateCPhasedag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateCPhasedag(const std::string& angle = "0.0",
                   const std::string& tol = "0.0")
    : _ctrl()
    , PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateCPhasedag(std::shared_ptr<PyQObject> ctrl,
                   std::shared_ptr<PyQObject> obj,
                   const std::string& angle = "0.0",
                   const std::string& tol = "0.0")
    : _ctrl(ctrl)
    , PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateCPhasedag>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::cphasedag<QConst_t(" + tol + ")>(" + _ctrl->print() +
           "," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateCPhasedag apply(std::shared_ptr<PyQObject> ctrl,
                         std::shared_ptr<PyQObject> obj)
  {
    _ctrl = ctrl;
    _obj = obj;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateCPhasedag> tmp =
      std::make_shared<PyQGateCPhasedag>(*this);
    tmp->_ctrl = _ctrl->duplicate();
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;

private:
  std::shared_ptr<PyQObject> _ctrl;
};

/**
   @brief CPHASEK gate class

   @ingroup py_gates
*/
class PyQGateCPhaseK : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateCPhaseK(std::size_t k = 0, const std::string& tol = "0.0")
    : _ctrl()
    , PyQBase()
    , k(k)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateCPhaseK(std::shared_ptr<PyQObject> ctrl,
                 std::shared_ptr<PyQObject> obj,
                 std::size_t k = 0,
                 const std::string& tol = "0.0")
    : _ctrl(ctrl)
    , PyQBase(obj)
    , k(k)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateCPhaseK>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::cphasek<" + std::to_string(k) + ",QConst_t(" + tol +
           ")>(" + _ctrl->print() + "," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateCPhaseK apply(std::shared_ptr<PyQObject> ctrl,
                       std::shared_ptr<PyQObject> obj)
  {
    _ctrl = ctrl;
    _obj = obj;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateCPhaseK> tmp =
      std::make_shared<PyQGateCPhaseK>(*this);
    tmp->_ctrl = _ctrl->duplicate();
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::size_t k;
  std::string tol;

private:
  std::shared_ptr<PyQObject> _ctrl;
};

/**
   @brief CPHASEKDAG gate class

   @ingroup py_gates
*/
class PyQGateCPhaseKdag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateCPhaseKdag(std::size_t k = 0, const std::string& tol = "0.0")
    : _ctrl()
    , PyQBase()
    , k(k)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateCPhaseKdag(std::shared_ptr<PyQObject> ctrl,
                    std::shared_ptr<PyQObject> obj,
                    std::size_t k = 0,
                    const std::string& tol = "0.0")
    : _ctrl(ctrl)
    , PyQBase(obj)
    , k(k)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateCPhaseKdag>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::cphasekdag<" + std::to_string(k) + ",QConst_t(" +
           tol + ")>(" + _ctrl->print() + "," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateCPhaseKdag apply(std::shared_ptr<PyQObject> ctrl,
                          std::shared_ptr<PyQObject> obj)
  {
    _ctrl = ctrl;
    _obj = obj;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateCPhaseKdag> tmp =
      std::make_shared<PyQGateCPhaseKdag>(*this);
    tmp->_ctrl = _ctrl->duplicate();
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::size_t k;
  std::string tol;

private:
  std::shared_ptr<PyQObject> _ctrl;
};

/**
   @brief CY gate class

   @ingroup py_gates
*/
class PyQGateCY : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateCY()
    : _ctrl()
    , PyQBase()
  {}

  /// @brief Constructor
  PyQGateCY(std::shared_ptr<PyQObject> ctrl, std::shared_ptr<PyQObject> obj)
    : _ctrl(ctrl)
    , PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateCY>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::cy(" + _ctrl->print() + "," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateCY apply(std::shared_ptr<PyQObject> ctrl,
                  std::shared_ptr<PyQObject> obj)
  {
    _ctrl = ctrl;
    _obj = obj;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateCY> tmp = std::make_shared<PyQGateCY>(*this);
    tmp->_ctrl = _ctrl->duplicate();
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

private:
  std::shared_ptr<PyQObject> _ctrl;
};

/**
   @brief CZ gate class

   @ingroup py_gates
*/
class PyQGateCZ : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateCZ()
    : _ctrl()
    , PyQBase()
  {}

  /// @brief Constructor
  PyQGateCZ(std::shared_ptr<PyQObject> ctrl, std::shared_ptr<PyQObject> obj)
    : _ctrl(ctrl)
    , PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateCZ>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::cz(" + _ctrl->print() + "," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateCZ apply(std::shared_ptr<PyQObject> ctrl,
                  std::shared_ptr<PyQObject> obj)
  {
    _ctrl = ctrl;
    _obj = obj;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateCZ> tmp = std::make_shared<PyQGateCZ>(*this);
    tmp->_ctrl = _ctrl->duplicate();
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

private:
  std::shared_ptr<PyQObject> _ctrl;
};

/**
   @brief HADAMARD gate class

   @ingroup py_gates
*/
class PyQGateHadamard : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateHadamard() = default;

  /// @brief Constructor
  PyQGateHadamard(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateHadamard>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::hadamard(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateHadamard> tmp =
      std::make_shared<PyQGateHadamard>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief IDENTITY gate class

   @ingroup py_gates
*/
class PyQGateIdentity : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateIdentity() = default;

  /// @brief Constructor
  PyQGateIdentity(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateIdentity>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::identity(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateIdentity> tmp =
      std::make_shared<PyQGateIdentity>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief INIT gate class

   @ingroup py_gates
*/
class PyQGateInit : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateInit() = default;

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateInit>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override { return "LibKet::gate::init()"; }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateInit> tmp = std::make_shared<PyQGateInit>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief MEASURE gate class

   @ingroup py_gates
*/
class PyQGateMeasure : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateMeasure() = default;

  /// @brief Constructor
  PyQGateMeasure(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateMeasure>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::measure(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateMeasure> tmp =
      std::make_shared<PyQGateMeasure>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief MEASURE_X gate class

   @ingroup py_gates
*/
class PyQGateMeasure_X : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateMeasure_X() = default;

  /// @brief Constructor
  PyQGateMeasure_X(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateMeasure_X>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::measure_x(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateMeasure_X> tmp =
      std::make_shared<PyQGateMeasure_X>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief MEASURE_Y gate class

   @ingroup py_gates
*/
class PyQGateMeasure_Y : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateMeasure_Y() = default;

  /// @brief Constructor
  PyQGateMeasure_Y(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateMeasure_Y>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::measure_y(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateMeasure_Y> tmp =
      std::make_shared<PyQGateMeasure_Y>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief MEASURE_Z gate class

   @ingroup py_gates
*/
class PyQGateMeasure_Z : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateMeasure_Z() = default;

  /// @brief Constructor
  PyQGateMeasure_Z(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateMeasure_Z>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::measure_z(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateMeasure_Z> tmp =
      std::make_shared<PyQGateMeasure_Z>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief PAULI_X gate class

   @ingroup py_gates
*/
class PyQGatePauli_X : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGatePauli_X() = default;

  /// @brief Constructor
  PyQGatePauli_X(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGatePauli_X>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::pauli_x(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGatePauli_X> tmp =
      std::make_shared<PyQGatePauli_X>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief PAULI_Y gate class

   @ingroup py_gates
*/
class PyQGatePauli_Y : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGatePauli_Y() = default;

  /// @brief Constructor
  PyQGatePauli_Y(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGatePauli_Y>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::pauli_y(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGatePauli_Y> tmp =
      std::make_shared<PyQGatePauli_Y>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief PAULI_Z gate class

   @ingroup py_gates
*/
class PyQGatePauli_Z : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGatePauli_Z() = default;

  /// @brief Constructor
  PyQGatePauli_Z(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGatePauli_Z>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::pauli_z(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGatePauli_Z> tmp =
      std::make_shared<PyQGatePauli_Z>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief PHASE gate class

   @ingroup py_gates
*/
class PyQGatePhase : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGatePhase(const std::string& angle = "0.0", const std::string& tol = "0.0")
    : PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGatePhase(std::shared_ptr<PyQObject> obj,
               const std::string& angle = "0.0",
               const std::string& tol = "0.0")
    : PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGatePhase>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::phase<QConst_t(" + tol + ")>(QConst(" + angle + ")," +
           _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGatePhase> tmp = std::make_shared<PyQGatePhase>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;
};

/**
   @brief PREP_X gate class

   @ingroup py_gates
*/
class PyQGatePrep_X : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGatePrep_X() = default;

  /// @brief Constructor
  PyQGatePrep_X(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGatePrep_X>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::prep_x(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGatePrep_X> tmp = std::make_shared<PyQGatePrep_X>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief PREP_Y gate class

   @ingroup py_gates
*/
class PyQGatePrep_Y : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGatePrep_Y() = default;

  /// @brief Constructor
  PyQGatePrep_Y(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGatePrep_Y>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::prep_y(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGatePrep_Y> tmp = std::make_shared<PyQGatePrep_Y>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief PREP_Z gate class

   @ingroup py_gates
*/
class PyQGatePrep_Z : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGatePrep_Z() = default;

  /// @brief Constructor
  PyQGatePrep_Z(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGatePrep_Z>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::prep_z(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGatePrep_Z> tmp = std::make_shared<PyQGatePrep_Z>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief RESET gate class

   @ingroup py_gates
*/
class PyQGateReset : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateReset() = default;

  /// @brief Constructor
  PyQGateReset(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateReset>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::reset(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateReset> tmp = std::make_shared<PyQGateReset>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief ROTATE_MX90 gate class

   @ingroup py_gates
*/
class PyQGateRotate_MX90 : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_MX90() = default;

  /// @brief Constructor
  PyQGateRotate_MX90(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_MX90>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_mx90(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_MX90> tmp =
      std::make_shared<PyQGateRotate_MX90>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief ROTATE_MY90 gate class

   @ingroup py_gates
*/
class PyQGateRotate_MY90 : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_MY90() = default;

  /// @brief Constructor
  PyQGateRotate_MY90(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_MY90>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_my90(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_MY90> tmp =
      std::make_shared<PyQGateRotate_MY90>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief ROTATE_X gate class

   @ingroup py_gates
*/
class PyQGateRotate_X : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_X(const std::string& angle = "0.0",
                  const std::string& tol = "0.0")
    : PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateRotate_X(std::shared_ptr<PyQObject> obj,
                  const std::string& angle = "0.0",
                  const std::string& tol = "0.0")
    : PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_X>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_x<QConst_t(" + tol + ")>(QConst(" + angle +
           ")," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_X> tmp =
      std::make_shared<PyQGateRotate_X>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;
};

/**
   @brief ROTATE_XDAG gate class

   @ingroup py_gates
*/
class PyQGateRotate_Xdag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_Xdag(const std::string& angle = "0.0",
                     const std::string& tol = "0.0")
    : PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateRotate_Xdag(std::shared_ptr<PyQObject> obj,
                     const std::string& angle = "0.0",
                     const std::string& tol = "0.0")
    : PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_Xdag>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_xdag<QConst_t(" + tol + ")>(QConst(" + angle +
           ")," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_Xdag> tmp =
      std::make_shared<PyQGateRotate_Xdag>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;
};

/**
   @brief ROTATE_Y gate class

   @ingroup py_gates
*/
class PyQGateRotate_Y : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_Y(const std::string& angle = "0.0",
                  const std::string& tol = "0.0")
    : PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateRotate_Y(std::shared_ptr<PyQObject> obj,
                  const std::string& angle = "0.0",
                  const std::string& tol = "0.0")
    : PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_Y>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_y<QConst_t(" + tol + ")>(QConst(" + angle +
           ")," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_Y> tmp =
      std::make_shared<PyQGateRotate_Y>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;
};

/**
   @brief ROTATE_YDAG gate class

   @ingroup py_gates
*/
class PyQGateRotate_Ydag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_Ydag(const std::string& angle = "0.0",
                     const std::string& tol = "0.0")
    : PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateRotate_Ydag(std::shared_ptr<PyQObject> obj,
                     const std::string& angle = "0.0",
                     const std::string& tol = "0.0")
    : PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_Ydag>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_ydag<QConst_t(" + tol + ")>(QConst(" + angle +
           ")," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_Ydag> tmp =
      std::make_shared<PyQGateRotate_Ydag>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;
};

/**
   @brief ROTATE_Z gate class

   @ingroup py_gates
*/
class PyQGateRotate_Z : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_Z(const std::string& angle = "0.0",
                  const std::string& tol = "0.0")
    : PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateRotate_Z(std::shared_ptr<PyQObject> obj,
                  const std::string& angle = "0.0",
                  const std::string& tol = "0.0")
    : PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_Z>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_z<QConst_t(" + tol + ")>(QConst(" + angle +
           ")," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_Z> tmp =
      std::make_shared<PyQGateRotate_Z>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;
};

/**
   @brief ROTATE_ZDAG gate class

   @ingroup py_gates
*/
class PyQGateRotate_Zdag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_Zdag(const std::string& angle = "0.0",
                     const std::string& tol = "0.0")
    : PyQBase()
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Constructor
  PyQGateRotate_Zdag(std::shared_ptr<PyQObject> obj,
                     const std::string& angle = "0.0",
                     const std::string& tol = "0.0")
    : PyQBase(obj)
    , angle(angle)
    , tol(tol)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_Zdag>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_zdag<QConst_t(" + tol + ")>(QConst(" + angle +
           ")," + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_Zdag> tmp =
      std::make_shared<PyQGateRotate_Zdag>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

public:
  std::string angle, tol;
};

/**
   @brief ROTATE_X90 gate class

   @ingroup py_gates
*/
class PyQGateRotate_X90 : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_X90() = default;

  /// @brief Constructor
  PyQGateRotate_X90(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_X90>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_x90(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_X90> tmp =
      std::make_shared<PyQGateRotate_X90>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief ROTATE_Y90 gate class

   @ingroup py_gates
*/
class PyQGateRotate_Y90 : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateRotate_Y90() = default;

  /// @brief Constructor
  PyQGateRotate_Y90(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateRotate_Y90>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::rotate_y90(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateRotate_Y90> tmp =
      std::make_shared<PyQGateRotate_Y90>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief S gate class

   @ingroup py_gates
*/
class PyQGateS : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateS() = default;

  /// @brief Constructor
  PyQGateS(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateS>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::s(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateS> tmp = std::make_shared<PyQGateS>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief SDAG gate class

   @ingroup py_gates
*/
class PyQGateSdag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateSdag() = default;

  /// @brief Constructor
  PyQGateSdag(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateSdag>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::sdag(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateSdag> tmp = std::make_shared<PyQGateSdag>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief T gate class

   @ingroup py_gates
*/
class PyQGateT : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateT() = default;

  /// @brief Constructor
  PyQGateT(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateT>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::t(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateT> tmp = std::make_shared<PyQGateT>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief TDAG gate class

   @ingroup py_gates
*/
class PyQGateTdag : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateTdag() = default;

  /// @brief Constructor
  PyQGateTdag(std::shared_ptr<PyQObject> obj)
    : PyQBase(obj)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateTdag>(*this);
  }

  /// Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::tdag(" + _obj->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateTdag> tmp = std::make_shared<PyQGateTdag>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }
};

/**
   @brief SWAP gate class

   @ingroup py_gates
*/
class PyQGateSwap : public PyQBase
{
public:
  /// @brief Default constructor
  PyQGateSwap() = default;

  /// @brief Constructor
  PyQGateSwap(std::shared_ptr<PyQObject> obj, std::shared_ptr<PyQObject> other)
    : PyQBase(obj)
    , _other(other)
  {}

  /// @brief Returns the adjoint of the current gate object
  virtual std::shared_ptr<PyQObject> dag() const
  {
    return std::make_shared<PyQGateSwap>(*this);
  }

  /// @brief Prints the content of the current gate object
  std::string print() const override
  {
    return "LibKet::gate::swap(" + _obj->print() + "," + _other->print() + ")";
  }

  /// @brief Returns duplicate of the current gate object applied to the given
  /// object
  PyQGateSwap apply(std::shared_ptr<PyQObject> obj,
                    std::shared_ptr<PyQObject> other)
  {
    _obj = obj;
    _other = other;
    return *this;
  }

  /// @brief Returns duplicate of the current gate object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQGateSwap> tmp = std::make_shared<PyQGateSwap>(*this);
    tmp->_obj = _obj->duplicate();
    tmp->_other = _other->duplicate();
    return tmp;
  }

private:
  std::shared_ptr<PyQObject> _other;
};

/**
   @brief Creates LibKet quantum gate Python module
*/
void
pybind11_init_gate(pybind11::module& m);

#endif // PYTHON_API_QGATES_HPP
