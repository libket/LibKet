/** @file python_api/PyQStream.cpp

    @brief Python API quantum stream class

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include "PyQStream.hpp"

namespace py = pybind11;

/**
   @brief Creates the LibKet execution stream Python module

   @ingroup py_api   
*/
void pybind11_init_stream(pybind11::module &m)
{
  m.attr("__name__") = "pylibket.stream";
  m.attr("__version__") = LIBKET_VERSION;
  m.doc() = "LibKet: the Quantum Expression Template Library";
  
  py::class_<PyQStream, PyQBase, std::shared_ptr<PyQStream>>(m, "PyQStream");
}
