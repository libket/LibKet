/** @file libket/QGates.hpp

    @brief C++ API gates header file

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup gates Quantum gates

    @ingroup cxx_api
 */
#pragma once
#ifndef GATES_HPP
#define GATES_HPP

#include <gates/QGate.hpp>
#include <gates/QGate_Barrier.hpp>
#include <gates/QGate_CCNOT.hpp>
#include <gates/QGate_CNOT.hpp>
#include <gates/QGate_CPhase.hpp>
#include <gates/QGate_CPhasedag.hpp>
#include <gates/QGate_CPhaseK.hpp>
#include <gates/QGate_CPhaseKdag.hpp>
#include <gates/QGate_CRX.hpp>
#include <gates/QGate_CRY.hpp>
#include <gates/QGate_CRZ.hpp>
#include <gates/QGate_CUnitary2.hpp>
#include <gates/QGate_CUnitary2dag.hpp>
#include <gates/QGate_CY.hpp>
#include <gates/QGate_CZ.hpp>
#include <gates/QGate_Hadamard.hpp>
#include <gates/QGate_Identity.hpp>
#include <gates/QGate_Init.hpp>
#include <gates/QGate_Measure.hpp>
#include <gates/QGate_Measure_X.hpp>
#include <gates/QGate_Measure_Y.hpp>
#include <gates/QGate_Measure_Z.hpp>
#include <gates/QGate_OR.hpp>
#include <gates/QGate_Pauli_X.hpp>
#include <gates/QGate_Pauli_Y.hpp>
#include <gates/QGate_Pauli_Z.hpp>
#include <gates/QGate_Phase.hpp>
#include <gates/QGate_Phasedag.hpp>
#include <gates/QGate_Prep_X.hpp>
#include <gates/QGate_Prep_Y.hpp>
#include <gates/QGate_Prep_Z.hpp>
#include <gates/QGate_Reset.hpp>
#include <gates/QGate_Rotate_MX90.hpp>
#include <gates/QGate_Rotate_MY90.hpp>
#include <gates/QGate_Rotate_X.hpp>
#include <gates/QGate_Rotate_X90.hpp>
#include <gates/QGate_Rotate_Xdag.hpp>
#include <gates/QGate_Rotate_Y.hpp>
#include <gates/QGate_Rotate_Y90.hpp>
#include <gates/QGate_Rotate_Ydag.hpp>
#include <gates/QGate_Rotate_Z.hpp>
#include <gates/QGate_Rotate_Zdag.hpp>
#include <gates/QGate_RXX.hpp>
#include <gates/QGate_RYY.hpp>
#include <gates/QGate_RZZ.hpp>
#include <gates/QGate_S.hpp>
#include <gates/QGate_Sdag.hpp>
#include <gates/QGate_Sqrt_Not.hpp>
#include <gates/QGate_Sqrt_Swap.hpp>
#include <gates/QGate_Swap.hpp>
#include <gates/QGate_T.hpp>
#include <gates/QGate_Tdag.hpp>
#include <gates/QGate_Unitary2.hpp>
#include <gates/QGate_Unitary2dag.hpp>

// The 'hook' gate must be included at the end
#include <gates/QGate_Hook.hpp>

#endif // GATES_HPP
