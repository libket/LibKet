/** @file libket/utils/constexpr_for.hpp

    @brief C++ API compile-time for-loop

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup utils
 */

#pragma once
#ifndef QUTILS_CONSTEXPR_FOR_HPP
#define QUTILS_CONSTEXPR_FOR_HPP

#include <string>
#include <type_traits>
#include <utility>

namespace LibKet {
namespace utils {

namespace detail {

  /**
     @brief C++17 folding helper structure

     @note This implementation is based on the work of Arthur O'Dwyer
     https://quuxplusone.github.io/blog/2020/10/12/fold-a-function/
  */  
  template<class F, class T>
  struct Folder {
    const F& functor_;
    T value_;
  
    template<class U>
    constexpr auto operator+(Folder<F, U>&& rhs) && -> decltype(auto)
    {
      using R = decltype(functor_(static_cast<decltype(value_)>(value_),
                                  static_cast<decltype(rhs.value_)>(rhs.value_)
                                  ));
      return Folder<F, R>{functor_, functor_(static_cast<decltype(value_)>(value_),
                                             static_cast<decltype(rhs.value_)>(rhs.value_)
                                             )};
    }
  };

  /**
     @brief Left-folds a callable

     @note This implementation is based on the work of Arthur O'Dwyer
     https://quuxplusone.github.io/blog/2020/10/12/fold-a-function/
   */
  template<class F>
  constexpr auto left_fold(F functor)
  {
    return [functor = static_cast<decltype(functor)&&>(functor)](auto&&... args) -> decltype(auto)
    {
      auto result = (... + Folder<F, decltype(args)>{functor, static_cast<decltype(args)>(args) });
      return result.value_;
    };
  }

  /**
     @brief Right-folds a callable

     @note This implementation is based on the work of Arthur O'Dwyer
     https://quuxplusone.github.io/blog/2020/10/12/fold-a-function/
   */
  template<class F>
  constexpr auto right_fold(F functor)
  {
    return [functor = static_cast<decltype(functor)&&>(functor)](auto&&... args) -> decltype(auto)
    {
      auto result = (Folder<F, decltype(args)>{functor, static_cast<decltype(args)>(args) } + ...);
      return result.value_;
    };
  }

  /**
     @brief Compile-time for-loop (implementation)
  */
  template<index_t for_start,
           index_t for_end,
           index_t for_step,
           template<index_t start, index_t end, index_t step, index_t index>
           class functor,
           std::size_t... Is,
           typename functor_return_type,
           typename... functor_types>
  inline auto
  constexpr_for_impl(std::index_sequence<Is...>,
                     functor_return_type&& functor_return_arg,
                     functor_types&&... functor_args)
  {
    auto lambda = [&functor_args...](auto functor_return_arg_, auto functor_)
      -> decltype(auto) { return functor_(functor_return_arg_, functor_args...); };
    return left_fold(lambda)(functor_return_arg, functor<for_start, for_end, for_step, for_start+for_step*Is>()...);
  }

} // namespace detail

/**
   @brief Compile-time for-loop
*/
template<index_t for_start,
         index_t for_end,
         index_t for_step,
         template<index_t start, index_t end, index_t step, index_t index>
         class functor,
         typename functor_return_type,
         typename... functor_types>
inline auto
constexpr_for(functor_return_type&& functor_return_arg,
              functor_types&&... functor_args)
{
  auto index = std::make_index_sequence<(for_end-for_start)/for_step+1>{};
  return LibKet::utils::detail::constexpr_for_impl<for_start,
                                                   for_end,
                                                    for_step,
                                                    functor>(index,
                                                             std::forward<functor_return_type>(functor_return_arg),
                                                             std::forward<functor_types>(functor_args)...);  
}

} // namespace utils
} // namespace LibKet

#endif // QUTILS_CONSTEXPR_FOR_HPP
