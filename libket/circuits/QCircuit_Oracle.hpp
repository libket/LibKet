/** @file libket/circuits/QCircuit_Oracle.hpp

    @brief C++ API oracle circuit class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers

    @defgroup oracle Oracle

    @ingroup circuits
 */

#pragma once
#ifndef QCIRCUIT_ORACLE_HPP
#define QCIRCUIT_ORACLE_HPP

#include <QFilter.hpp>
#include <QGates.hpp>
#include <QUtils.hpp>
#include <circuits/QCircuit.hpp>

#include <random>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {

/**
@brief Oracle circuit class

The LibKet oracle circuit class implements
the oracle algorithm for an arbitrary number of qubits

@ingroup oracle
*/
template<typename _tol = QConst_M_ZERO_t>
class QCircuit_Oracle : public QCircuit
{
private:
public:
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

  /// Shows circuit
  template<std::size_t level = 1>
  std::string show() const noexcept
  {
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }
  
  /// Apply function
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           QBackendType _qbackend>
  inline static QExpression<_qubits, _qbackend>& apply(
    QExpression<_qubits, _qbackend>& expr) noexcept
  {
    srand(time(NULL));
    int number = rand() % 2;
    if (number == 0) {
      auto e =
        CX(sel<_filter0::template size<_qubits>() - 2>(tag<0>(_filter0{})),
           tag<1>(_filter1{}));
      return e(expr);
    } else {
      auto e =
        CX(sel<_filter0::template size<_qubits>() - 1>(tag<0>(_filter0{})),
           sel<0>(gototag<1>(
             CX(sel<_filter0::template size<_qubits>() - 1>(tag<0>(_filter0{})),
                tag<1>(_filter1{})))));
      return e(expr);
    }
  }
};

///@ingroup oracle
///@{
  
/**
@brief Oracle circuit creator

This overload of the LibKet::circuits::oracle() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::oracle();
\endcode
*/

template<typename _tol = QConst_M_ZERO_t>
inline constexpr auto
oracle() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCircuit_Oracle<_tol>>(
    filters::QFilter{}, filters::QFilter{});
}

/**
@brief Oracle circuit creator

This overload of the LibKet::circuits::oracle() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::oracle(expr);
\endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _expr0, typename _expr1>
inline constexpr auto
oracle(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCircuit_Oracle<_tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief Oracle circuit creator

This overload of the LibKet::circuits::oracle() function accepts
an expression as universal reference
*/
template<typename _tol = QConst_M_ZERO_t, typename _expr0, typename _expr1>
inline constexpr auto
oracle(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCircuit_Oracle<_tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

template<typename _tol = QConst_M_ZERO_t, typename _expr0, typename _expr1>
inline constexpr auto
oracle(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCircuit_Oracle<_tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

template<typename _tol = QConst_M_ZERO_t, typename _expr0, typename _expr1>
inline constexpr auto
oracle(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCircuit_Oracle<_tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief Oracle circuit creator

Function alias for LibKet::circuits::oracle
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
oracle(Args&&... args)
{
  return oracle<_tol>(std::forward<Args>(args)...);
}

///@}
  
/// Operator() - by constant reference
template<typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Oracle<_tol>::operator()(const T0& t0, const T1& t1) const noexcept
{
  return oracle<_tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Oracle<_tol>::operator()(T0&& t0, T1&& t1) const noexcept
{
  return oracle<_tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief Shows circuit

   @note  specialization for oracle objects

   @ingroup oracle
*/
template<std::size_t level = 1, typename _tol = QConst_M_ZERO_t>
inline static auto
show(const QCircuit_Oracle<_tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "oracle\n";

  return circuit;
}

} // namespace circuits
  
} // namespace LibKet

#endif // QCIRCUIT_ORACLE_HPP
