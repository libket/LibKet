/** @file libket/circuits/QCircuit_Arb_Ctrl.hpp

    @brief C++ API arbitrary controll circuit class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers

    @defgroup arbctrl Arbitrary control circuit

    @ingroup circuits
*/

#pragma once
#ifndef QCIRCUIT_ARB_CTRL_HPP
#define QCIRCUIT_ARB_CTRL_HPP

#include <QCircuits.hpp>
#include <QFilter.hpp>
#include <QGates.hpp>
#include <QUtils.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {
/**
@brief Arbitrary controll circuit class

The LibKet arbitrary controll circuit class implements a circuit which
uses an arbitrary amount of control bits for an operation

@ingroup arbctrl
*/
template<typename _Ugate, typename _tol = QConst_M_ZERO_t>
class QCircuit_Arb_Ctrl : public QCircuit
{
private:
  /// Arbitrary control functor
  template<index_t start, index_t end, index_t step, index_t index>
  struct arb_ctrl_loop
  {
    template<typename Expr0, typename Expr2>
    inline constexpr auto operator()(Expr0&& expr0, Expr2&& expr2) noexcept
    {
      return CCNOT(sel<index>(gototag<0>(expr0)),
                   sel<index - 2>(gototag<2>(expr2)),
                   sel<index - 1>(gototag<2>(expr2)));
    }
  };

public:
  
  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(const T0& t0,
                                   const T1& t1,
                                   const T2& t2) const noexcept;

  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(const T0& to,
                                   T1&& t1,
                                   const T2& t2) const noexcept;

  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(T0&& t0,
                                   const T1& t1,
                                   const T2& t2) const noexcept;

  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(T0&& t0,
                                   T1&& t1,
                                   const T2& t2) const noexcept;

  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(const T0& t0,
                                   const T1& t1,
                                   T2&& t2) const noexcept;

  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(const T0& to,
                                   T1&& t1,
                                   T2&& t2) const noexcept;

  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(T0&& t0,
                                   const T1& t1,
                                   T2&& t2) const noexcept;

  template<typename T0, typename T1, typename T2>
  inline constexpr auto operator()(T0&& t0, T1&& t1, T2&& t2) const noexcept;
  
  /// Shows circuit
  template<std::size_t level = 1>
  std::string show() const noexcept
  {
    std::ostringstream os;
    using ::LibKet::show;
    show<level>(*this, os);
    return os.str();
  }
  
  /// Apply function
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2,
           QBackendType _qbackend>
  inline static QExpression<_qubits, _qbackend>& apply(
    QExpression<_qubits, _qbackend>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
        _filter2::template size<_qubits>() + 1,
      "arb_ctrl (arbitrary control circuit) with n control bits can "
      "only be applied when n-1 ancilla bits are provided");
    auto e = ccnot(
      sel<0>(gototag<0>(
        utils::constexpr_for<(index_t)_filter0::template size<_qubits>() - 1,
                             2,
                             -1,
                             arb_ctrl_loop>(
             gototag<0>(_Ugate()(
               sel<_filter2::template size<_qubits>() - 1>(gototag<2>(
                 utils::constexpr_for<2,
                                      (index_t)_filter0::template size<_qubits>() - 1,
                                      1,
                                      arb_ctrl_loop>(
                   gototag<0>(ccnot(sel<0>(tag<0>(_filter0{})),
                                    sel<1>(tag<0>(_filter0{})),
                                    sel<0>(tag<2>(_filter2{})))),
                   tag<2>(_filter2{})))),
               sel<0>(tag<1>(_filter1{})))),
             tag<2>(_filter2{})))),
         sel<1>(tag<0>(_filter0{})),
         sel<0>(tag<2>(_filter2{})));
    ; // Write outer shell of loop
    return e(expr);
  }
};

///@ingroup arbctrl
///@{
  
#ifdef LIBKET_OPTIMIZE_GATES

  // TODO: Implement optimization for ARB_CTRL

#endif // LIBKET_OPTIMIZE_GATES


/**
@brief Arbitrary controll circuit creator

This overload of the LibKet::circuits:arb_ctrl() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::qft(expr);
\endcode
*/
template<typename _tol = QConst_M_ZERO_t>
inline constexpr auto
arb_ctrl() noexcept
{
  return TernaryQGate<filters::QFilter,
                      filters::QFilter,
                      filters::QFilter,
                      QCircuit_Arb_Ctrl<_tol>>(
    filters::QFilter{}, filters::QFilter{}, filters::QFilter{});
}

/**
@brief Arbitrary controll circuit creator

This overload of the LibKet::circuits:arb_ctrl() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::qft(expr);
\endcode
*/
template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1,
         typename _expr2>
inline constexpr auto
arb_ctrl(_Ugate,
         const _expr0& expr0,
         const _expr1& expr1,
         const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCircuit_Arb_Ctrl<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief Arbitrary control circuit creator

This overload of the LibKet::circuits::arb_ctrl() function accepts an
expression as universal reference
*/
template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1,
         typename _expr2>
inline constexpr auto
arb_ctrl(_Ugate,
         const _expr0& expr0,
         const _expr1& expr1,
         _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCircuit_Arb_Ctrl<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief Arbitrary control circuit creator

This overload of the LibKet::circuits::arb_ctrl() function accepts an
expression as universal reference and constant reference
*/
template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1,
         typename _expr2>
inline constexpr auto
arb_ctrl(_Ugate, _expr0&& expr0, _expr1&& expr1, const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCircuit_Arb_Ctrl<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief Arbitrary control circuit creator

This overload of the LibKet::circuits::arb_ctrl() function accepts an
expression as universal reference and constant reference
*/
template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1,
         typename _expr2>
inline constexpr auto
arb_ctrl(_Ugate, _expr0&& expr0, _expr1&& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCircuit_Arb_Ctrl<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1,
         typename _expr2>
inline constexpr auto
arb_ctrl(_Ugate, const _expr0& expr0, _expr1&& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCircuit_Arb_Ctrl<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1,
         typename _expr2>
inline constexpr auto
arb_ctrl(_Ugate,
         const _expr0& expr0,
         _expr1&& expr1,
         const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCircuit_Arb_Ctrl<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}
template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1,
         typename _expr2>
inline constexpr auto
arb_ctrl(_Ugate,
         _expr0&& expr0,
         const _expr1& expr1,
         const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCircuit_Arb_Ctrl<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

template<typename _Ugate,
         typename _tol = QConst_M_ZERO_t,
         typename _expr0,
         typename _expr1,
         typename _expr2>
inline constexpr auto
arb_ctrl(_Ugate, _expr0&& expr0, const _expr1& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QCircuit_Arb_Ctrl<_Ugate, _tol>,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief Arbitrary control circuit creator

Function alias for LibKet::circuits::arb_ctrl
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
ARB_CTRL(Args&&... args)
{
  return arb_ctrl<_tol>(std::forward<Args>(args)...);
}

///@}

/// Operator() - by constant reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCircuit_Arb_Ctrl<_Ugate, _tol>::operator()(const T0& t0,
                                            const T1& t1,
                                            const T2& t2) const noexcept
{
  return arb_ctrl<_Ugate, _tol>(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}
/// Operator() - by universal reference
template<typename _Ugate, typename _tol>
template<typename T0, typename T1, typename T2>
inline constexpr auto
QCircuit_Arb_Ctrl<_Ugate, _tol>::operator()(T0&& t0,
                                            T1&& t1,
                                            T2&& t2) const noexcept
{
  return arb_ctrl<_Ugate, _tol>(
    std::forward<T0>(t0), std::forward<T1>(t1), std::forward<T2>(t2));
}

/**
   @brief Libket show circuit type - specialization for arbitrary control circuit
   objects

   @ingroup arbctrl
*/
template<std::size_t level = 1, typename _Ugate, typename _tol = QConst_M_ZERO_t>
inline static auto
show(const QCircuit_Arb_Ctrl<_Ugate, _tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "ARB_CTRL\n";

  return circuit;
}

} // namespace circuits

} // namespace libket

#endif // QCIRCUIT_ARB_CTRL_HPP
