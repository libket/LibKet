/** @file libket/devices/QDevice_OpenQL.hpp

    @brief C++ API Circuit OpenQL class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_OPENQL_HPP
#define QDEVICE_OPENQL_HPP

#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_OPENQL
/**
   @brief OpenQL class

   This class compiles the quantum circuit using the OpenQL
   backend. It adopts the OpenQL quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_OpenQL_compiler : public QExpression<_qubits, QBackendType::OpenQL>
{
private:
  /// Name of the output directory
  const std::string output_dir;

  /// Base type
  using Base = QExpression<_qubits, QBackendType::OpenQL>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_OpenQL_compiler(
    const std::string& config_file = LibKet::getenv("OPENQL_CONFIG_FILE",
                                                    LIBKET_BINARY
                                                    "test_config_default.json"),
    const std::string& output_dir = LibKet::getenv("OPENQL_OUTPUT_DIR", "."))
    : output_dir(output_dir)
    , Base(config_file)
  {
    ql::set_option("output_dir", output_dir);
  }

  /// Constructor from JSON object
  QDevice_OpenQL_compiler(const utils::json& config)
    : QDevice_OpenQL_compiler(config.find("config_file") != config.end()
                                ? config["config_file"]
                                : LibKet::getenv("OPENQL_CONFIG_FILE"),
                              config.find("output_dir") != config.end()
                                ? config["output_dir"]
                                : LibKet::getenv("OPENQL_OUTPUT_DIR", "."))
  {
    // Set all other options explicitly
    for (auto& item : config.items()) {
      if ((item.key().compare("config_file") != 0) &&
          (item.key().compare("config_file") != 0))
        ql::set_option(item.key(), item.value());
    }
  }

  /// Apply expression to base type
  template<typename Expr>
  QDevice_OpenQL_compiler& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_OpenQL_compiler& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Compile kernel
  std::string compile()
  {
    std::string filepath = utils::getTempPath();
    ql::set_option("output_dir", filepath);

    std::string filename =
      std::to_string(std::hash<std::string>()(this->to_string()));
    std::string s = compile(filename);

    ql::set_option("output_dir", output_dir);
    return s;
  }

  /// Compile kernel
  std::string compile(const std::string& filename)
  {
    ql::Program _program(filename, Base::_platform, _qubits, _qubits);
    _program.add_kernel(Base::_kernel);
    try {
      ql::set_option("write_qasm_files", "yes");
      _program.compile();
      ql::set_option("write_qasm_files", "no");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
    }

    std::ifstream file;
    if (ql::get_option("scheduler").compare("yes"))
      file.open(filename + "_scheduled.qasm");
    else
      file.open(filename + ".qasm");

    if (file.is_open()) {
      std::stringstream ss;
      ss << file.rdbuf();
      return ss.str();
    } else
      return "Unable to open file" + filename;
  }
};

#else

/**
   @brief OpenQL class

   This class compiles the quantum circuit using the OpenQL
   backend. It adopts the OpenQL quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_OpenQL_compiler : public QDevice_Dummy
{
  using QDevice_Dummy::QDevice_Dummy;
};

#endif

#define QDeviceDefineOpenQL_compiler(_type)                                    \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_OpenQL_compiler<_qubits>                                  \
  {                                                                            \
  public:                                                                      \
    QDevice(const std::string& output_dir =                                    \
              LibKet::getenv("OPENQL_OUTPUT_DIR", "."))                        \
      : QDevice_OpenQL_compiler<_qubits>(device::QDeviceProperty<_type>::name, \
                                         output_dir)                           \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("output_dir") != config.end()                      \
                  ? config["output_dir"]                                       \
                  : LibKet::getenv("OPENQL_OUTPUT_DIR", "."))                  \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

namespace device {

// OpenQL hardware devices
QDevicePropertyDefine(QDeviceType::openql_cc_light_compiler,
                      LIBKET_BINARY "hardware_config_cc_light.json",
                      7,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::openql_cc_light17_compiler,
                      LIBKET_BINARY "hardware_config_cc_light17.json",
                      17,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::openql_qx_compiler,
                      LIBKET_BINARY "hardware_config_qx.json",
                      26,
                      true,
                      QEndianness::lsb);

} // namespace device

// OpenQL hardware devices
QDeviceDefineOpenQL_compiler(QDeviceType::openql_cc_light_compiler);
QDeviceDefineOpenQL_compiler(QDeviceType::openql_cc_light17_compiler);
QDeviceDefineOpenQL_compiler(QDeviceType::openql_qx_compiler);

} // namespace LibKet

#endif // QDEVICE_OPENQL_HPP
