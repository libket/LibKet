/** @file libket/devices/QDevice_Qiskit.hpp

    @brief C++ API Qiskit device class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_QISKIT_HPP
#define QDEVICE_QISKIT_HPP

#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_QISKIT
/**
   @brief Qiskit device class

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_Qiskit : public QExpression<_qubits, QBackendType::OpenQASMv2>
{
private:
  /// User access token
  const std::string API_token;

  /// Name of the provider's hub
  const std::string hub;

  /// Name of the provider's group
  const std::string group;

  /// Name of the provider's project
  const std::string project;

  /// Name of the backend
  const std::string backend;

  /// Number of shots to run the quantum kernel
  const std::size_t shots;

  /// Base type
  using Base = QExpression<_qubits, QBackendType::OpenQASMv2>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_Qiskit(
    const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),
    const std::string& hub = LibKet::getenv("IBMQ_HUB", "ibm-q"),
    const std::string& group = LibKet::getenv("IBMQ_GROUP", "open"),
    const std::string& project = LibKet::getenv("IBMQ_PROJECT", "main"),
    const std::string& backend = LibKet::getenv("IBMQ_BACKEND",
                                                "qasm_simulator"),
    std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
    : API_token(API_token)
    , hub(hub)
    , group(group)
    , project(project)
    , backend(backend)
    , shots(shots)
  {
    // Qiskit PulseSimulator is not supported
    if (!backend.compare("pulse_simulator"))
      LIBKET_ERROR("Qiskit PulseSimulator is not supported");
  }

  /// Constructor from JSON object
  QDevice_Qiskit(const utils::json& config)
    : QDevice_Qiskit(config.find("API_token") != config.end()
                       ? config["API_token"]
                       : LibKet::getenv("IBMQ_API_TOKEN"),
                     config.find("hub") != config.end()
                       ? config["hub"]
                       : LibKet::getenv("IBMQ_HUB", "ibm-q"),
                     config.find("group") != config.end()
                       ? config["group"]
                       : LibKet::getenv("IBMQ_GROUP", "open"),
                     config.find("project") != config.end()
                       ? config["project"]
                       : LibKet::getenv("IBMQ_PROJECT", "main"),
                     config.find("backend") != config.end()
                       ? config["backend"]
                       : LibKet::getenv("IBMQ_BACKEND", "qasm_simulator"),
                     config.find("shots") != config.end()
                       ? config["shots"].get<size_t>()
                       : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_Qiskit& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_Qiskit& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit locally on Qiskit simulator
  /// asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tdef default(o):\n"
       << "\t\timport numpy\n"
       << "\t\timport datetime\n"
       << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
       << "\t\tif isinstance(o, (datetime.date, datetime.datetime)): return "
          "o.isoformat()\n"
       << "\t\traise TypeError\n"

       << "\tdef get_provider():\n"
       << "\t\tif IBMQ.active_account(): return IBMQ.get_provider(hub='" << hub << "', group='" << group << "', project='" <<  project << "')\n"
       << "\t\telse: return IBMQ.enable_account(token='" << API_token
       << "', hub='" << hub << "', group='" << group << "', project='" << project << "')\n"
      
       << "\timport json\n"
       << "\tfrom qiskit import QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, Aer, execute\n";

    if (backend == "qasm_simulator" or
        backend == "statevector_simulator" or
        backend == "unitary_simulator") {
      // Import IBMQ to retrieve hardware configurations and noise model
      ss << "\tfrom qiskit import IBMQ\n"
         << "\tfrom qiskit.providers.aer import noise\n";

      // Authentication to IBMQ
      if (API_token.empty())
        ss << "\tif IBMQ.active_account():\n"
           << "\t\tIBMQ.disable_account()\n"
           << "\tprovider = IBMQ.load_account()\n";
      else
        ss << "\tprovider = get_provider()\n";
      
      // Select hardware backend to simulate
      ss << "\tbackend = provider.get_backend('" << backend << "')\n";
    } else {
      // Import Aer for idealistic simulation
      ss << "\tbackend = Aer.get_backend('" << backend << "')\n";
    }

    // Prepare quantum circuit
    ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    if (backend == "qasm_simulator" or
        backend == "statevector_simulator" or
        backend == "unitary_simulator") {
      // Prepare noise model for hardware simulator
      ss << "\tproperties = backend.properties()\n"
         << "\tcoupling_map = backend.configuration().coupling_map\n"
         << "\tnoise_model = noise.NoiseModel.from_backend(backend)\n"
         << "\tbasis_gates = noise_model.basis_gates\n"
         << "\tbackend_simulator = Aer.get_backend('qasm_simulator')\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute noisy circuit locally
      ss << "\tjob = execute(qc, backend_simulator, coupling_map=coupling_map, "
            "noise_model=noise_model, basis_gates=basis_gates, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute circuit locally
      ss << "\tjob = execute(qc, backend=backend, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
    }
    ss << "\tresult = job.result()\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result.to_dict(), default=default)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit locally on Qiskit simulator
  /// synchronously and return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit locally on Qiskit simulator
  /// synchronously and return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

  /// Export circuit to ASCII string
  std::string print_circuit(const std::string& script_init = "",
                            const std::string& script_before = "",
                            const std::string& script_after = "",
                            QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\timport json\n"
       << "\tfrom qiskit import QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, Aer, transpile\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    if (backend == "qasm_simulator" or
        backend == "statevector_simulator" or
        backend == "unitary_simulator") {
      // Import IBMQ to retrieve hardware configurations and noise model
      ss << "\tfrom qiskit import IBMQ\n"
         << "\tfrom qiskit.providers.aer import noise\n";

      // Authentication to IBMQ
      ss << "\tif IBMQ.active_account():\n\t\tIBMQ.disable_account()\n";
      if (API_token.empty())
        ss << "\tprovider = IBMQ.load_account()\n";
      else
        ss << "\tprovider = IBMQ.enable_account(token='" << API_token
           << "', hub='" << hub << "', group='" << group << "', project='"
           << project << "')\n";

      // Select hardware backend to simulate
      ss << "\tbackend = provider.get_backend('" << backend << "')\n";
    } else {
      // Import Aer for idealistic simulation
      ss << "\tbackend = Aer.get_backend('" << backend << "')\n";
    }

    // Prepare quantum circuit
    ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    if (backend == "qasm_simulator" or
        backend == "statevector_simulator" or
        backend == "unitary_simulator") {
      // Prepare noise model for hardware simulator
      ss << "\tproperties = backend.properties()\n"
         << "\tcoupling_map = backend.configuration().coupling_map\n"
         << "\tnoise_model = noise.NoiseModel.from_backend(backend)\n"
         << "\tbasis_gates = noise_model.basis_gates\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Transpile circuit
      ss << "\tqc = transpile(qc, backend, coupling_map=coupling_map, "
            "basis_gates=basis_gates)\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");
    }

    // Draw circuit to ASCII console output
    ss << "\tresult = circuit_drawer(qc, output='text').single_string()\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }


  /// Export circuit to LaTeX using the XYZ package
  std::string to_latex(const std::string& script_init = "",
                       const std::string& script_before = "",
                       const std::string& script_after = "",
                       QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\timport json\n"
       << "\tfrom qiskit import QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, Aer, transpile\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    if (backend.compare("qasm_simulator") *
        backend.compare("statevector_simulator") *
        backend.compare("unitary_simulator")) {
      // Import IBMQ to retrieve hardware configurations and noise model
      ss << "\tfrom qiskit import IBMQ\n"
         << "\tfrom qiskit.providers.aer import noise\n";

      // Authentication to IBMQ
      ss << "\tif IBMQ.active_account():\n\t\tIBMQ.disable_account()\n";
      if (API_token.empty())
        ss << "\tprovider = IBMQ.load_account()\n";
      else
        ss << "\tprovider = IBMQ.enable_account(token='" << API_token
           << "', hub='" << hub << "', group='" << group << "', project='"
           << project << "')\n";

      // Select hardware backend to simulate
      ss << "\tbackend = provider.get_backend('" << backend << "')\n";
    } else {
      // Import Aer for idealistic simulation
      ss << "\tbackend = Aer.get_backend('" << backend << "')\n";
    }

    // Prepare quantum circuit
    ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    if (backend.compare("qasm_simulator") *
        backend.compare("statevector_simulator") *
        backend.compare("unitary_simulator")) {
      // Prepare noise model for hardware simulator
      ss << "\tproperties = backend.properties()\n"
         << "\tcoupling_map = backend.configuration().coupling_map\n"
         << "\tnoise_model = noise.NoiseModel.from_backend(backend)\n"
         << "\tbasis_gates = noise_model.basis_gates\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Transpile circuit
      ss << "\tqc = transpile(qc, backend, coupling_map=coupling_map, "
            "basis_gates=basis_gates)\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");
    }

    // Draw circuit to latex source
    ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    std::size_t _value = 0;
    std::string _key = "0x0";

    for (auto& _item : _counts.items()) {
      if (_item.value() > _value) {
        _value = _item.value();
        _key = _item.key();
      }
    }

    return stoi(_key, 0, 16);
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["time_taken"].get<Rep>());
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (auto& _item : _counts.items()) {
      _histogram[stoi(_item.key(), 0, 16)] = (T)_item.value();
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return result["job_id"].get<std::string>();
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return result["success"].get<bool>();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    struct tm tm;
    strptime(
      result["date"].get<std::string>().c_str(), "%Y-%m-%dT%H:%M:%S", &tm);
    return mktime(&tm);
  }
};

#else

/**
   @brief Qiskit device class

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_Qiskit : public QDevice_Dummy
{
  using QDevice_Dummy::QDevice_Dummy;
};

#endif

#define _QDeviceDefineQiskit(_device, _name, _qubits)                   \
  namespace device {                                                    \
    QDevicePropertyDefine(_device, _name, _qubits,                      \
                          true,                                         \
                          QEndianness::lsb);                            \
  }                                                                     \
                                                                        \
  template<std::size_t __qubits>                                        \
  class QDevice<_device,                                                \
                __qubits,                                               \
                device::QDeviceProperty<_device>::simulator,            \
                device::QDeviceProperty<_device>::endianness>           \
    : public QDevice_Qiskit<__qubits>                                           \
  {                                                                            \
  public:                                                                      \
    QDevice(                                                                   \
      const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),         \
      const std::string& hub = LibKet::getenv("IBMQ_HUB", "ibm-q"),            \
      const std::string& group = LibKet::getenv("IBMQ_GROUP", "open"),         \
      const std::string& project = LibKet::getenv("IBMQ_PROJECT", "main"),     \
      std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))     \
      : QDevice_Qiskit<__qubits>(API_token,                             \
                                 hub,                                   \
                                 group,                                 \
                                 project,                               \
                                 device::QDeviceProperty<_device>::name, \
                                 shots)                                 \
      {                                                                 \
        static_assert(__qubits <= device::QDeviceProperty<_device>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
                                                                        \
    QDevice(const utils::json& config)                                  \
      : QDevice(config.find("API_token") != config.end()                \
                ? config["API_token"]                                   \
                : LibKet::getenv("IBMQ_API_TOKEN"),                     \
                config.find("hub") != config.end()                      \
                ? config["hub"]                                         \
                : LibKet::getenv("IBMQ_GROUP", "ibm-q"),                \
                config.find("group") != config.end()                    \
                ? config["group"]                                       \
                : LibKet::getenv("IBMQ_GROUP", "open"),                 \
                config.find("project") != config.end()                  \
                ? config["project"]                                     \
                : LibKet::getenv("IBMQ_PROJECT", "main"),               \
                config.find("shots") != config.end()                    \
                ? config["shots"].get<size_t>()                         \
                : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))      \
      {                                                                 \
        static_assert(__qubits <= device::QDeviceProperty<_device>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
  };

#define QDeviceDefineQiskit(_name, _qubits)                     \
  _QDeviceDefineQiskit(QDeviceType::qiskit_##_name##_simulator, \
                       "ibmq_"#_name, _qubits)

// Qiskit devices
QDeviceDefineQiskit(almaden, 20);
QDeviceDefineQiskit(armonk, 1);
QDeviceDefineQiskit(athens, 5);
QDeviceDefineQiskit(belem, 5);
QDeviceDefineQiskit(boeblingen, 20);
QDeviceDefineQiskit(bogota, 5);
QDeviceDefineQiskit(brooklyn, 65);
QDeviceDefineQiskit(burlington, 5);
QDeviceDefineQiskit(cairo, 27);
QDeviceDefineQiskit(cambridge, 28);
QDeviceDefineQiskit(casablanca, 5);
QDeviceDefineQiskit(dublin, 27);
QDeviceDefineQiskit(essex, 5);
QDeviceDefineQiskit(guadalupe, 16);
QDeviceDefineQiskit(hanoi, 27);
QDeviceDefineQiskit(jakarta, 7);
QDeviceDefineQiskit(johannesburg, 20);
QDeviceDefineQiskit(kolkata, 27);
QDeviceDefineQiskit(lagos, 7);
QDeviceDefineQiskit(lima, 5);
QDeviceDefineQiskit(london, 5);
QDeviceDefineQiskit(manhattan, 65);
QDeviceDefineQiskit(manila, 5);
QDeviceDefineQiskit(melbourne, 15);
QDeviceDefineQiskit(montreal, 27);
QDeviceDefineQiskit(mumbai, 27);
QDeviceDefineQiskit(nairobi, 7);
QDeviceDefineQiskit(ourense, 5);
QDeviceDefineQiskit(paris, 27);
QDeviceDefineQiskit(peekskill, 27);
QDeviceDefineQiskit(poughkeepsie, 20);
QDeviceDefineQiskit(quito, 5);
QDeviceDefineQiskit(rochester, 53);
QDeviceDefineQiskit(rome, 5);
QDeviceDefineQiskit(rueschlikon, 16);
QDeviceDefineQiskit(santiago, 5);
QDeviceDefineQiskit(singapore, 20);
QDeviceDefineQiskit(sydney, 27);
QDeviceDefineQiskit(tenerife, 5);
QDeviceDefineQiskit(tokyo, 20);
QDeviceDefineQiskit(toronto, 27);
QDeviceDefineQiskit(valencia, 5);
QDeviceDefineQiskit(vigo, 5);
QDeviceDefineQiskit(yorktown, 5);
QDeviceDefineQiskit(washington, 127);
QDeviceDefineQiskit(perth, 7);

_QDeviceDefineQiskit(QDeviceType::qiskit_pulse_simulator, "pulse_simulator", 20);
_QDeviceDefineQiskit(QDeviceType::qiskit_qasm_simulator, "qasm_simulator", 31);
_QDeviceDefineQiskit(QDeviceType::qiskit_statevector_simulator, "statevector_simulator", 31);
_QDeviceDefineQiskit(QDeviceType::qiskit_unitary_simulator, "unitary_simulator", 15);

_QDeviceDefineQiskit(QDeviceType::qiskit_aer_density_matrix_simulator, "aer_simulator_density_matrix", 15);
_QDeviceDefineQiskit(QDeviceType::qiskit_aer_extended_stabilizer_simulator, "aer_simulator_extended_stabilizer", 63);
_QDeviceDefineQiskit(QDeviceType::qiskit_aer_matrix_product_state_simulator, "aer_simulator_matrix_product_state", 63);
_QDeviceDefineQiskit(QDeviceType::qiskit_aer_simulator, "aer_simulator", 31);
_QDeviceDefineQiskit(QDeviceType::qiskit_aer_stabilizer_simulator, "aer_simulator_stabilizer", 10000);
_QDeviceDefineQiskit(QDeviceType::qiskit_aer_statevector_simulator, "aer_statevector_simulator", 31);
_QDeviceDefineQiskit(QDeviceType::qiskit_aer_superop_simulator, "aer_simulator_superop", 7);
_QDeviceDefineQiskit(QDeviceType::qiskit_aer_unitary_simulator, "aer_simulator_unitary", 15);

} // namespace LibKet

#endif // QDEVICE_QISKIT_HPP
