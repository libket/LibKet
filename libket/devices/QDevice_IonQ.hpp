/** @file libket/devices/QDevice_IonQ.hpp

    @brief C++ API IonQ device class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller, Huub Donkers
*/

#pragma once
#ifndef QDEVICE_IONQ_HPP
#define QDEVICE_IONQ_HPP

#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_IONQ
  /**
     @brief IonQ device class

     @ingroup devices
  */
  template<std::size_t _qubits>
  class QDevice_IonQ : public QExpression<_qubits, QBackendType::OpenQASMv2>
  {
  private:
    /// User access token
    const std::string API_token;

    /// Name of the backend
    const std::string backend;

    /// Number of shots to run the quantum kernel
    const std::size_t shots;

    /// Base type
    using Base = QExpression<_qubits, QBackendType::OpenQASMv2>;

  public:
    /// Constructors from base class
    using Base::Base;

    /// Constructor from parameter list
    QDevice_IonQ(
                 const std::string& API_token = LibKet::getenv("IONQ_API_TOKEN"),
                 const std::string& backend = LibKet::getenv("IONQ_BACKEND", "ionq_simulator"),
                 std::size_t shots = std::atoi(LibKet::getenv("IONQ_SHOTS", "1024")))
      : API_token(API_token)
      , backend(backend)
      , shots(shots)
    {}

    /// Constructor from JSON object
    QDevice_IonQ(const utils::json& config)
      : QDevice_IonQ(config.find("API_token") != config.end()
                     ? config["API_token"]
                     : LibKet::getenv("IONQ_API_TOKEN"),
                     config.find("backend") != config.end()
                     ? config["backend"]
                     : LibKet::getenv("IONQ_BACKEND", "ionq_simulator"),
                     config.find("shots") != config.end()
                     ? config["shots"].get<size_t>()
                     : std::atoi(LibKet::getenv("IONQ_SHOTS", "1024")))
    {}

    /// Apply expression to base type
    template<typename Expr>
    QDevice_IonQ& operator()(const Expr& expr)
    {
      expr(*reinterpret_cast<Base*>(this));
      return *this;
    }

    /// Apply string-based expression to base type
    QDevice_IonQ& operator()(const std::string& expr)
    {
      gen_expression(expr, *reinterpret_cast<Base*>(this));
      return *this;
    }

    /// Execute quantum circuit remotely on IonQ backend
    /// asynchronously and return pointer to job
    QJob<QJobType::Python>* execute_async(
                                          std::size_t shots = 0,
                                          const std::string& script_init = "",
                                          const std::string& script_before = "",
                                          const std::string& script_after = "",
                                          QStream<QJobType::Python>* stream = NULL)
    {
      std::stringstream ss;

      ss << "def run():\n";

      // User-defined script to be performed before initialization
      if (!script_init.empty())
        ss << utils::string_ident(script_init, "\t");

      // Standard imports
      ss << "\tdef default(o):\n"
         << "\t\timport numpy\n"
         << "\t\timport datetime\n"
         << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
         << "\t\tif isinstance(o, (datetime.date, datetime.datetime)): return o.isoformat()\n"
         << "\t\traise TypeError\n"
      
         << "\timport json\n"
         << "\tfrom qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit, execute\n";

      // Authentication to IonQ 
      if (API_token.empty()){
        ss << "\treturn json.dumps('Unable to retreive API token')\n";
      }else{
        ss << "\tdef get_provider():\n"
           << "\t\tfrom qiskit_ionq import IonQProvider\n"
           << "\t\treturn IonQProvider('" << API_token <<"')\n";
      }

      if (backend != "ionq_simulator") {
        // Raise warning for IonQ QPU 
        ss << "\treturn json.dumps('No support for " << backend <<" yet')\n";
      } else {
        // Import IonQ simulator backend
        ss <<"\tprovider = get_provider()\n"
           <<"\tbackend = provider.get_backend('" << backend << "')\n";
      }

      // Prepare quantum circuit
      ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
         << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
         << "\tqc = QuantumCircuit(qreg, creg)\n"
         << "\tqasm = '''\n"
         << Base::to_string() << "'''\n"
         << "\tqc = qc.from_qasm_str(qasm)\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute circuit locally
      ss << "\tjob = execute(qc, backend=backend, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
    
      ss << "\tresult = job.result()\n";

      // User-defined script to be performed right after execution
      if (!script_after.empty())
        ss << utils::string_ident(script_after, "\t");

      // Return result and add timestamp
      ss << "\tfrom datetime import datetime\n"
         << "\tresult_dict = result.to_dict()\n"
         << "\tresult_dict['date'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f')\n"
         << "\treturn json.dumps(result_dict, default=default)\n";

      QDebug << ss.str();

      try {
        if (stream != NULL)
          return stream->run(ss.str(), "run", "", "");
        else
          return _qstream_python.run(ss.str(), "run", "", "");
      } catch (std::exception& e) {
        QInfo << e.what() << std::endl;
        return NULL;
      }
    }

    /// Execute quantum circuit remotely on IonQ backend
    /// synchronously and return pointer to job
    QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                    const std::string& script_init = "",
                                    const std::string& script_before = "",
                                    const std::string& script_after = "",
                                    QStream<QJobType::Python>* stream = NULL)
    {
      return execute_async(
                           shots, script_init, script_before, script_after, stream)
        ->wait();
    }

    /// Execute quantum circuit remotely on IonQ backend
    /// synchronously and return result
    utils::json eval(std::size_t shots = 0,
                     const std::string& script_init = "",
                     const std::string& script_before = "",
                     const std::string& script_after = "",
                     QStream<QJobType::Python>* stream = NULL)
    {
      return execute_async(
                           shots, script_init, script_before, script_after, stream)
        ->get();
    }

    /// Export circuit to ASCII string
    std::string print_circuit(const std::string& script_init = "",
                              const std::string& script_before = "",
                              const std::string& script_after = "",
                              QStream<QJobType::Python>* stream = NULL)
    {
      std::stringstream ss;

      ss << "def run():\n";

      // User-defined script to be performed before initialization
      if (!script_init.empty())
        ss << utils::string_ident(script_init, "\t");

      // Standard imports
      ss << "\tdef default(o):\n"
         << "\t\timport numpy\n"
         << "\t\timport datetime\n"
         << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
         << "\t\tif isinstance(o, (datetime.date, datetime.datetime)): return o.isoformat()\n"
         << "\t\traise TypeError\n"
      
         << "\timport json\n"
         << "\tfrom qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit, execute\n"
         << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

      // Authentication to IonQ 
      if (API_token.empty()){
        ss << "\treturn json.dumps('Unable to retreive API token')\n";
      }else{
        ss << "\tdef get_provider():\n"
           << "\t\tfrom qiskit_ionq import IonQProvider\n"
           << "\t\treturn IonQProvider('" << API_token <<"')\n";
      }

      if (backend != "ionq_simulator") {
        // Raise warning for IonQ QPU 
        ss << "\treturn json.dumps('No support for " << backend <<" yet')\n";
      } else {
        // Import IonQ simulator backend
        ss <<"\tprovider = get_provider()\n"
           <<"\tbackend = provider.get_backend('" << backend << "')\n";
      }

      // Prepare quantum circuit
      ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
         << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
         << "\tqc = QuantumCircuit(qreg, creg)\n"
         << "\tqasm = '''\n"
         << Base::to_string() << "'''\n"
         << "\tqc = qc.from_qasm_str(qasm)\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");    

      // Draw circuit to ASCII console output
      ss << "\tresult = circuit_drawer(qc, output='text').single_string()\n";

      // User-defined script to be performed right after execution
      if (!script_after.empty())
        ss << utils::string_ident(script_after, "\t");

      // Return result
      ss << "\treturn json.dumps(result)\n";

      QDebug << ss.str();

      try {
        if (stream != NULL)
          return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
        else
          return (_qstream_python.run(ss.str(), "run", "", "")->get())
            .get<std::string>();
      } catch (std::exception& e) {
        QInfo << e.what() << std::endl;
        return NULL;
      }
    }

    /// Export circuit to LaTeX using the XYZ package
    std::string to_latex(const std::string& script_init = "",
                         const std::string& script_before = "",
                         const std::string& script_after = "",
                         QStream<QJobType::Python>* stream = NULL)
    {
      std::stringstream ss;

      ss << "def run():\n";

      // User-defined script to be performed before initialization
      if (!script_init.empty())
        ss << utils::string_ident(script_init, "\t");

      // Standard imports
      ss << "\tdef default(o):\n"
         << "\t\timport numpy\n"
         << "\t\timport datetime\n"
         << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
         << "\t\tif isinstance(o, (datetime.date, datetime.datetime)): return o.isoformat()\n"
         << "\t\traise TypeError\n"
      
         << "\timport json\n"
         << "\tfrom qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit, execute\n"
         << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

      // Authentication to IonQ 
      if (API_token.empty()){
        ss << "\treturn json.dumps('Unable to retreive API token')\n";
      }else{
        ss << "\tdef get_provider():\n"
           << "\t\tfrom qiskit_ionq import IonQProvider\n"
           << "\t\treturn IonQProvider('" << API_token <<"')\n";
      }

      if (backend != "ionq_simulator") {
        // Raise warning for IonQ QPU 
        ss << "\treturn json.dumps('No support for " << backend <<" yet')\n";
      } else {
        // Import IonQ simulator backend
        ss <<"\tprovider = get_provider()\n"
           <<"\tbackend = provider.get_backend('" << backend << "')\n";
      }

      // Prepare quantum circuit
      ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
         << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
         << "\tqc = QuantumCircuit(qreg, creg)\n"
         << "\tqasm = '''\n"
         << Base::to_string() << "'''\n"
         << "\tqc = qc.from_qasm_str(qasm)\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");    

      // Draw circuit to latex source
      ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";

      // User-defined script to be performed right after execution
      if (!script_after.empty())
        ss << utils::string_ident(script_after, "\t");

      // Return result
      ss << "\treturn json.dumps(result)\n";

      QDebug << ss.str();

      try {
        if (stream != NULL)
          return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
        else
          return (_qstream_python.run(ss.str(), "run", "", "")->get())
            .get<std::string>();
      } catch (std::exception& e) {
        QInfo << e.what() << std::endl;
        return NULL;
      }
    }

  public:
    /// Get state with highest probability from JSON object
    template<QResultType _type>
    static auto get(const utils::json& result) ->
      typename std::enable_if<_type == QResultType::best, std::size_t>::type
    {
      assert(get<QResultType::status>(result));
      auto _counts = result["results"][0]["data"]["counts"];
      std::size_t _value = 0;
      std::string _key = "0x0";

      for (auto& _item : _counts.items()) {
        if (_item.value() > _value) {
          _value = _item.value();
          _key = _item.key();
        }
      }

      return stoi(_key, 0, 16);
    }

    /// Get duration from JSON object
    template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
    static auto get(const utils::json& result) ->
      typename std::enable_if<_type == QResultType::duration,
                              std::chrono::duration<Rep, Period>>::type
    {
      assert(get<QResultType::status>(result));
      return std::chrono::duration<Rep, Period>(result["time_taken"].get<Rep>());
    }

    /// Get histogram from JSON object
    template<QResultType _type, class T = std::size_t>
    static auto get(const utils::json& result) ->
      typename std::enable_if<_type == QResultType::histogram,
                              QArray<(1 << _qubits), T, QEndianness::lsb>>::type
    {
      assert(get<QResultType::status>(result));
      auto _counts = result["results"][0]["data"]["counts"];
      QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

      for (auto& _item : _counts.items()) {
        _histogram[stoi(_item.key(), 0, 16)] = (T)_item.value();
      }

      return _histogram;
    }

    /// Get unique identifier from JSON object
    template<QResultType _type>
    static auto get(const utils::json& result) ->
      typename std::enable_if<_type == QResultType::id, std::string>::type
    {
      assert(get<QResultType::status>(result));
      return result["job_id"].get<std::string>();
    }

    /// Get success status from JSON object
    template<QResultType _type>
    static auto get(const utils::json& result) ->
      typename std::enable_if<_type == QResultType::status, bool>::type
    {
      return result["success"].get<bool>();
    }

    /// Get time stamp from JSON object
    template<QResultType _type>
    static auto get(const utils::json& result) ->
      typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
    {
      assert(get<QResultType::status>(result));

      struct tm tm;
      strptime(
               result["date"].get<std::string>().c_str(), "%Y-%m-%dT%H:%M:%S", &tm);
      return mktime(&tm);
    }
  };

#else

  /**
     @brief IonQ device class

     @ingroup devices
  */
  template<std::size_t _qubits>
  class QDevice_IonQ : public QDevice_Dummy
  {};

#endif

#define _QDeviceDefineIonQ(_device, _name, _qubits)                     \
  namespace device {                                                    \
    QDevicePropertyDefine(_device, _name, _qubits,                      \
                          true,                                         \
                          QEndianness::lsb);                            \
  }                                                                     \
                                                                        \
  template<std::size_t __qubits>                                        \
  class QDevice<_device,                                                \
                __qubits,                                               \
                device::QDeviceProperty<_device>::simulator,            \
                device::QDeviceProperty<_device>::endianness>           \
    : public QDevice_IonQ<__qubits>                                     \
  {                                                                     \
  public:                                                               \
    QDevice(const std::string& API_token = LibKet::getenv("IONQ_API_TOKEN"), \
            std::size_t shots = std::atoi(LibKet::getenv("IONQ_SHOTS", "1024"))) \
      : QDevice_IonQ<__qubits>(API_token,                               \
                               device::QDeviceProperty<_device>::name,  \
                               shots)                                   \
      {                                                                 \
        static_assert(__qubits <= device::QDeviceProperty<_device>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
                                                                        \
    QDevice(const utils::json& config)                                  \
      : QDevice(config.find("API_token") != config.end()                \
                ? config["API_token"]                                   \
                : LibKet::getenv("IONQ_API_TOKEN"),                     \
                config.find("shots") != config.end()                    \
                ? config["shots"].get<size_t>()                         \
                : std::atoi(LibKet::getenv("IONQ_SHOTS", "1024")))      \
      {                                                                 \
        static_assert(__qubits <= device::QDeviceProperty<_device>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
  };

// IonQ devices
_QDeviceDefineIonQ(QDeviceType::ionq_simulator, "ionq_simulator", 11);
_QDeviceDefineIonQ(QDeviceType::ionq_harmony, "ionq_harmony", 11);
_QDeviceDefineIonQ(QDeviceType::ionq_aria_1, "ionq_aria-1", 25);
_QDeviceDefineIonQ(QDeviceType::ionq_aria_2, "ionq_aria-2", 25);
_QDeviceDefineIonQ(QDeviceType::ionq_forte, "ionq_forte", 32);

} // namespace LibKet

#endif // QDevice_IonQ_HPP
