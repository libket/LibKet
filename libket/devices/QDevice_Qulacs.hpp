/** @file libket/devices/QDevice_Qulacs.hpp

    @brief C++ API Qulacs device class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_QULACS_HPP
#define QDEVICE_QULACS_HPP

#include <string>
#include <vector>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_QULACS
/**
   @brief Qulacs device class

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_Qulacs : public QExpression<_qubits, QBackendType::Qulacs>
{
private:
    /// Number of shots
  const std::size_t shots;
  
  /// Base type
  using Base = QExpression<_qubits, QBackendType::Qulacs>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_Qulacs(std::size_t shots = std::atoi(LibKet::getenv("QULACS_SHOTS",
                                                              "1024")))
    : shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_Qulacs(const utils::json& config)
    : QDevice_Qulacs(config.find("shots") != config.end()
                     ? config["shots"].get<size_t>()
                     : std::atoi(LibKet::getenv("QULACS_SHOTS", "1024")))
  {}
  
  /// Apply expression to base type
  template<typename Expr>
  QDevice_Qulacs& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_Qulacs& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }
  
  /// Execute quantum circuit locally on Qulacs device asynchronously
  /// and return pointer to job
  QJob<QJobType::CXX>* execute_async(
    std::size_t shots = 0,
    std::function<void(QDevice_Qulacs*)> func_init = NULL,
    std::function<void(QDevice_Qulacs*)> func_before = NULL,
    std::function<void(QDevice_Qulacs*)> func_after = NULL,
    QStream<QJobType::CXX>* stream = NULL)
  {
    auto runner = [&, shots]() -> void {
      if (func_init)
        func_init(this);
      if (func_before)
        func_before(this);
      Base::_circuit.update_quantum_state(&(Base::_state));
      if (func_after)
        func_after(this);
    };

    if (stream != NULL)
      return stream->run(runner);
    else
      return _qstream_cxx.run(runner);
  }

  /// Execute quantum circuit locally on Qulacs device synchronously
  /// and return pointer to job
  QJob<QJobType::CXX>* execute(
    std::size_t shots = 0,
    std::function<void(QDevice_Qulacs*)> func_init = NULL,
    std::function<void(QDevice_Qulacs*)> func_before = NULL,
    std::function<void(QDevice_Qulacs*)> func_after = NULL,
    QStream<QJobType::CXX>* stream = NULL)
  {
    return execute_async(shots, func_init, func_before, func_after, stream)
      ->wait();
  }

  /// Execute quantum circuit locally on Qulacs device synchronously
  /// and return result
  QuantumState& eval(std::size_t shots = 0,
                     std::function<void(QDevice_Qulacs*)> func_init = NULL,
                     std::function<void(QDevice_Qulacs*)> func_before = NULL,
                     std::function<void(QDevice_Qulacs*)> func_after = NULL,
                     QStream<QJobType::CXX>* stream = NULL)
  {
    execute_async(shots, func_init, func_before, func_after, stream)->wait();
    return Base::_state;
  }

public:
  /// Get state with highest probability from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>());
    return 0;
  }

  /// Get duration from internal data object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get() ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>());
    return std::chrono::duration<Rep, Period>(0);
  }

  /// Get histogram from internal data object
  template<QResultType _type, class T = std::size_t>
  static auto get() ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>());
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    return _histogram;
  }

  /// Get unique identifier from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>());
    return std::string("0");
  }

  /// Get success status from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return true;
  }

  /// Get time stamp from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>());
    return std::time(nullptr);
  }
};

#else

/**
   @brief Qulacs device class

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_Qulacs : public QDevice_Dummy
{
  using QDevice_Dummy::QDevice_Dummy;
};

#endif

#define QDeviceDefineQulacs(_type)                                      \
  template<std::size_t _qubits>                                         \
  class QDevice<_type,                                                  \
                _qubits,                                                \
                device::QDeviceProperty<_type>::simulator,              \
                device::QDeviceProperty<_type>::endianness>             \
    : public QDevice_Qulacs<_qubits>                                    \
  {                                                                     \
  public:                                                               \
    QDevice(std::size_t shots = std::atoi(LibKet::getenv("QULACS_SHOTS", \
                                                         "1024")))      \
      : QDevice_Qulacs<_qubits>(shots)                                  \
      {                                                                 \
        static_assert(_qubits <= device::QDeviceProperty<_type>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
                                                                        \
    QDevice(const utils::json& config)                                  \
      : QDevice(config.find("shots") != config.end()                    \
                ? config["shots"].get<size_t>()                         \
                : std::atoi(LibKet::getenv("QULACS_SHOTS", "1024")))    \
      {                                                                 \
        static_assert(_qubits <= device::QDeviceProperty<_type>::qubits, \
                      "#qubits exceeds device capacity");               \
      }                                                                 \
  };
  
namespace device {

QDevicePropertyDefine(QDeviceType::qulacs, "Qulacs", 44, true, QEndianness::lsb);

} // namespace device

QDeviceDefineQulacs(QDeviceType::qulacs);

} // namespace LibKet

#endif // QDEVICE_QULACS_HPP
