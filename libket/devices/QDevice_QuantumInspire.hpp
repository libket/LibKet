/** @file libket/devices/QDevice_QuantumInspire.hpp

    @brief C++ API Quantum-Inspire device class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_QUANTUMINSPIRE_HPP
#define QDEVICE_QUANTUMINSPIRE_HPP

#include <chrono>
#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_QUANTUMINSPIRE
/**
   @brief Quantum-Inspire device class

   This class executes quantum circuits remotely on the
   Quantum-Inspire simulator made accessible through QuTech's
   Quantum-Inspire cloud services. It adopts the commonQASM v1.0
   quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_QuantumInspire_simulator
  : public QExpression<_qubits, QBackendType::cQASMv1>
{
private:
  /// User's name for authentication
  const std::string username;

  /// User's password for authentication
  const std::string password;

  /// URL of the quantum-inspire platform
  const std::string url;

  /// Name of the backend
  const std::string backend;

  /// Name of the project
  const std::string projectname;

  /// Number of shots to run the quantum kernel
  const std::size_t shots;

  /// Value of the depolarizing channel error
  const double depolarizing_channel;

  /// Full state projection mode
  const bool full_state_projection;

  /// Base type
  using Base = QExpression<_qubits, QBackendType::cQASMv1>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_QuantumInspire_simulator(
    const std::string& username = LibKet::getenv("QI_USERNAME"),
    const std::string& password = LibKet::getenv("QI_PASSWORD"),
    const std::string& url = LibKet::getenv("QI_URL",
                                            "https://api.quantum-inspire.com"),
    const std::string& backend = LibKet::getenv("QI_BACKEND",
                                                "QX single-node simulator"),
    const std::string& projectname = LibKet::getenv("QI_PROJECTNAME"),
    const std::size_t& shots = std::atoi(LibKet::getenv("QI_SHOTS", "1024")),
    const double& depolarizing_channel =
      std::atof(LibKet::getenv("QI_DEPOLARIZING_CHANNEL", "0.0")),
    const bool& full_state_projection = (bool)
      std::atoi(LibKet::getenv("QI_FULL_STATE_PROJECTION", "0")))
    : username(username)
    , password(password)
    , url(url)
    , backend(backend)
    , projectname(projectname)
    , shots(shots)
    , depolarizing_channel(depolarizing_channel)
    , full_state_projection(full_state_projection)
  {}

  /// Constructor from JSON object
  QDevice_QuantumInspire_simulator(const utils::json& config)
    : QDevice_QuantumInspire_simulator(
        config.find("username") != config.end() ? config["username"]
                                                : LibKet::getenv("QI_USERNAME"),
        config.find("password") != config.end() ? config["password"]
                                                : LibKet::getenv("QI_PASSWORD"),
        config.find("url") != config.end()
          ? config["url"]
          : LibKet::getenv("QI_URL", "https://api.quantum-inspire.com"),
        config.find("backend") != config.end()
          ? config["backend"]
          : LibKet::getenv("QI_BACKEND", "QX single-node simulator"),
        config.find("projectname") != config.end()
          ? config["projectname"]
          : LibKet::getenv("QI_PROJECTNAME"),
        config.find("shots") != config.end()
          ? config["shots"].get<size_t>()
          : std::atoi(LibKet::getenv("QI_SHOTS", "1024")),
        config.find("depolarizing_channel") != config.end()
          ? config["depolarizing_channel"].get<double>()
          : std::atof(LibKet::getenv("QI_DEPOLARIZING_CHANNEL", "0.0")),
        config.find("full_state_projection") != config.end()
          ? config["full_state_projection"].get<bool>()
          : std::atoi(LibKet::getenv("QI_PROJECTNAME", "0")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_QuantumInspire_simulator& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_QuantumInspire_simulator& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit remotely on Quantum Inspire
  /// asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    ss << "\timport json\n"
       << "\tfrom getpass import getpass\n"
       << "\tfrom coreapi.auth import BasicAuthentication\n"
       << "\tfrom quantuminspire.api import QuantumInspireAPI\n"
       << "\tusername = '" << username << "'\n"
       << "\tpassword = '" << password << "'\n"
       << "\tserver_url = r'" << url << "'\n"
       << "\tauth = BasicAuthentication(username, password)\n"
       << "\tqi = QuantumInspireAPI(server_url, auth)\n"
       << "\tqasm = '''\n"
       << Base::to_string();

    if (depolarizing_channel > 0.0)
      ss << "error_model depolarizing_channel, " << depolarizing_channel
         << "\n";

    ss << "'''\n";

    // User-defined project name (if not set the project will be deleted
    // immediately after execution)
    if (!projectname.empty())
      ss << "\tqi.project_name = '" << projectname << "'\n";

    ss << "\tbackend_type = qi.get_backend_type_by_name('" << backend << "')\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    ss << "\tresult = qi.execute_qasm(qasm, backend_type=backend_type, "
          "number_of_shots="
       << utils::to_string(shots > 0 ? shots : this->shots)
       << ", full_state_projection = "
       << (full_state_projection ? "True" : "False") << ")\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit remotely on Quantum Inspire
  /// synchronously and return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit remotely on Quantum Inspire
  /// synchronously and return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["histogram"];
    std::size_t _value = 0;
    std::string _key = "0";

    for (auto& _item : _counts.items()) {
      if (_item.value() > _value) {
        _value = _item.value();
        _key = _item.key();
      }
    }

    return stoi(_key);
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    return std::chrono::duration<Rep, Period>(
      result["execution_time_in_seconds"].get<Rep>());
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = real_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["histogram"];
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (auto& _item : _counts.items()) {
      _histogram[stoi(_item.key())] = (T)_item.value();
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return std::to_string(result["id"].get<std::size_t>());
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return result["raw_text"].get<std::string>().empty();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    struct tm tm;
    strptime(result["created_at"].get<std::string>().c_str(),
             "%Y-%m-%dT%H:%M:%S",
             &tm);
    return mktime(&tm);
  }
};

#else

/**
   @brief Quantum-Inspire device class

   This class executes quantum circuits remotely on the
   Quantum-Inspire simulator made accessible through QuTech's
   Quantum-Inspire cloud services. It adopts the commonQASM v1.0
   quantum assembly language.

   @ingroup devices
*/
template<std::size_t _qubits>
class QDevice_QuantumInspire_simulator : public QDevice_Dummy
{
  using QDevice_Dummy::QDevice_Dummy;
};

#endif

#define QDeviceDefineQI(_type)                                                 \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_QuantumInspire_simulator<_qubits>                         \
  {                                                                            \
  public:                                                                      \
    QDevice(const std::string& username = LibKet::getenv("QI_USERNAME"),       \
            const std::string& password = LibKet::getenv("QI_PASSWORD"),       \
            const std::string& url =                                           \
              LibKet::getenv("QI_URL", "https://api.quantum-inspire.com"),     \
            const std::string& projectname = LibKet::getenv("QI_PROJECTNAME"), \
            const std::size_t& shots = std::atoi(LibKet::getenv("QI_SHOTS",    \
                                                                "1024")),      \
            const double& depolarizing_channel =                               \
              std::atof(LibKet::getenv("QI_DEPOLARIZING_CHANNEL", "0.0")),     \
            const bool& full_state_projection = (bool)                         \
              std::atoi(LibKet::getenv("QI_FULL_STATE_PROJECTION", "0")))      \
      : QDevice_QuantumInspire_simulator<_qubits>(                             \
          username,                                                            \
          password,                                                            \
          url,                                                                 \
          device::QDeviceProperty<_type>::name,                                \
          projectname,                                                         \
          shots,                                                               \
          depolarizing_channel,                                                \
          full_state_projection)                                               \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(                                                               \
          config.find("username") != config.end()                              \
            ? config["username"]                                               \
            : LibKet::getenv("QI_USERNAME"),                                   \
          config.find("password") != config.end()                              \
            ? config["password"]                                               \
            : LibKet::getenv("QI_PASSWORD"),                                   \
          config.find("url") != config.end()                                   \
            ? config["url"]                                                    \
            : LibKet::getenv("QI_URL", "https://api.quantum-inspire.com"),     \
          config.find("projectname") != config.end()                           \
            ? config["projectname"]                                            \
            : LibKet::getenv("QI_PROJECTNAME"),                                \
          config.find("shots") != config.end()                                 \
            ? config["shots"].get<size_t>()                                    \
            : std::atoi(LibKet::getenv("QI_SHOTS", "1024")),                   \
          config.find("depolarizing_channel") != config.end()                  \
            ? config["depolarizing_channel"].get<double>()                     \
            : std::atof(LibKet::getenv("QI_DEPOLARIZING_CHANNEL", "0.0")),     \
          config.find("full_state_projection") != config.end()                 \
            ? config["full_state_projection"].get<bool>()                      \
            : std::atoi(LibKet::getenv("QI_PROJECTNAME", "0")))                \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

namespace device {

QDevicePropertyDefine(QDeviceType::qi_26_simulator,
                      "QX single-node simulator",
                      26,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qi_34_simulator,
                      "QX-34-L",
                      34,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qi_spin2,
                      "Spin-2",
                      2,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qi_starmon5,
                      "Starmon-5",
                      5,
                      false,
                      QEndianness::lsb);

} // namespace device

QDeviceDefineQI(QDeviceType::qi_26_simulator);
QDeviceDefineQI(QDeviceType::qi_34_simulator);

QDeviceDefineQI(QDeviceType::qi_spin2);
QDeviceDefineQI(QDeviceType::qi_starmon5);

} // namespace LibKet

#endif // QDEVICE_QUANTUMINSPIRE_HPP
