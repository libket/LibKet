/** @file libket/gates/QGate_CRZ.hpp

    @brief C++ API quantum CRZ (controlled-RotZ gate) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller & Huub Donkers

    @defgroup crz CRZ gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_CRZ_HPP
#define QGATE_CRZ_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QCRZdag;

/**
@brief CRZ (controlled-RotZ) gate class

The CRZ (controlled-RotZ) gate class implements the quantum
controlled-rotZ gate for an arbitrary number of quantum bits

The CRZ (controlled-Z) gate is a two-qubit operation, where the first
qubit is usually referred to as the control qubit and the second qubit
as the target qubit. The CRZ (controlled-rotZ) gates leaves the control 
qubit unchanged and performs a Z-rotation by angle \f$\theta\f$ on the target 
qubit only when the control qubit is in state \f$\left|1\right>\f$.

The unitary matrix reads

\f[
\text{CRZ} =
\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & e^{-i\frac{\theta}{2}} & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & e^{i\frac{\theta}{2}}
\end{pmatrix}
\f]

@ingroup crz
*/
template<typename _angle, typename _tol = QConst_M_ZERO_t>
class QCRZ : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  BINARY_GATE_DEFAULT_DECL_AT(QCRZ, QCRZdag);
  
  ///@{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CTRL(RZ[" + _angle::to_string() + "]) q[" +
                           utils::to_string(std::get<0>(i)) + "],q[" +
                           utils::to_string(std::get<1>(i)) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cirq.ControlledGate(cirq.rz(rads=" + _angle::to_string() +
                           ")).on(q[" + utils::to_string(std::get<0>(i)) +
                           "],q[" + utils::to_string(std::get<1>(i)) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "c-Rz q[";
      for (auto i : _filter0::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(expr))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(expr).end() - 1) ? "," : "], ");
      _expr += _angle::to_string() + "\n";
      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("crz(" + _angle::to_string(false) + ") q[" +
                           utils::to_string(std::get<0>(i)) + "], q[" +
                           utils::to_string(std::get<1>(i)) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel([&]() {
          expr.kernel().controlled_rz(
            std::get<0>(i), std::get<1>(i), _angle::value());
        });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("\tdef c-Rz, 1, 'Rz'\n \tc-Rz q" + utils::to_string(std::get<0>(i)) +
                           ",q" + utils::to_string(std::get<1>(i)) + " # " +
                           _angle::to_string() + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        quest::controlledRotateZ(
          expr.reg(), std::get<0>(i), std::get<1>(i), (qreal)_angle::value());
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QULACS
  /// @brief Apply function
  /// @ingroup QULACS
  ///
  /// @note specialization for LibKet::QBackendType::Qulacs backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Qulacs>& apply(
    QExpression<_qubits, QBackendType::Qulacs>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      //      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        //quest::controlledRotateZ(
        //expr.reg(), std::get<0>(i), std::get<1>(i), (qreal)_angle::value());
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("CONTROLLED RZ(" + _angle::to_string(true) + ") " +
                           utils::to_string(std::get<0>(i)) + " " +
                           utils::to_string(std::get<1>(i)) + "\n");
    }
    return expr;
  }
#endif
  
#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CRZ gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel(new qx::bin_ctrl(std::get<0>(i), new qx::rz(std::get<1>(i), (double)_angle::value())));
    }
    return expr;
  }
#endif
  ///@}
};

/**
@brief CRZ (controlled phase shift) gate creator
@ingroup crz

This overload of the LibKet::gates::crz() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::crz();
\endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _angle>
inline constexpr auto crz(_angle) noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCRZ<_angle, _tol>>(
    filters::QFilter{}, filters::QFilter{});
}

namespace detail {
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename _expr1,
         typename = void>
struct crz_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QCRZ<_angle, _tol>,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

/// @defgroup crz_aliases Aliases
/// @ingroup crz
///@{

#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0_0,
         typename __expr0_1,
         typename __filter0,
         typename __expr1_0,
         typename __expr1_1,
         typename __filter1>
struct crz_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0_0, __expr0_1, QCRZ<_angle, _tol>, __filter0>,
  BinaryQGate<__expr1_0, __expr1_1, QCRZ<_angle, _tol>, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0_0, __expr0_1, QCRZ<_angle, _tol>, __filter0>&
      expr0,
    const BinaryQGate<__expr1_0, __expr1_1, QCRZ<_angle, _tol>, __filter1>&
      expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return crz_impl<_tol,
                       _angle,
                       decltype(typename filters::getFilter<__filter0>::type{}(
                         all(expr1.expr1(all(
                           expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
                       typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return crz_impl<
      _tol,
      _angle,
      typename filters::getFilter<__filter0>::type,
      decltype(typename filters::getFilter<__filter1>::type{}(all(
        expr0.expr0(all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct crz_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct crz_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct crz_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct crz_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct crz_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct crz_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct crz_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct crz_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct crz_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCRZ here */
    !std::is_same<typename gates::getGate<_expr0>::type, QCRZ<_angle,_tol> >::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct crz_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct crz_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CRZ gate is handled explicitly */
    (!std::is_base_of<
      QCRZ<_angle, _tol>,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct crz_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCRZ here */
    !std::is_same<typename gates::getGate<_expr1>::type,
                  QCRZ<_angle, _tol>>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct crz_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief CRZ (controlled phase shift) creator

   This overload of the LibKet::gates::crz() function
   eliminates the double-application of the CRZ gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct crz_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CRZ gate is handled explicitly */
    (!std::is_base_of<
      QCRZ<_angle, _tol>,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCRZ<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CRZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief CRZ (controlled phase shift) creator

This overload of the LibKet::gates::crz() function accepts
two expressions as constant reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
crz(_angle, const _expr0& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::crz_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief CRZ (controlled phase shift) creator

This overload of the LibKet::gates::crz() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
crz(_angle, const _expr0& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::crz_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief CRZ (controlled phase shift) creator

This overload of the LibKet::gates::crz() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
crz(_angle, _expr0&& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::crz_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief CRZ (controlled phase shift) creator

This overload of the LibKet::gates::crz() function accepts
two expression as universal reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
crz(_angle, _expr0&& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::crz_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief CRZ (controlled phase shift) gate creator

Function alias for LibKet::gates::crz()
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
CRZ(Args&&... args)
{
  return crz<_tol>(std::forward<Args>(args)...);
}

BINARY_GATE_DEFAULT_IMPL_AT(QCRZ, crz);
///@}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CRZ_HPP
