/** @file libket/gates/QGate.h

    @brief C++ API: Quantum gate macros

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_H
#define QGATE_H

#include <gates/QGate_Binary.h>
#include <gates/QGate_Ternary.h>
#include <gates/QGate_Unary.h>

/// @brief Macro creates function alias
#define GATE_ALIAS(_func, _alias)                   \
  template<typename... Args>                        \
  inline constexpr auto                             \
  _alias(Args&&... args)                            \
  {                                                 \
    return _func(std::forward<Args>(args)...);      \
  }

/// @brief Macro creates function alias with tolerance as template
/// parameter
#define GATE_ALIAS_T(_func, _alias)                                     \
  template<typename _tol = QConst_M_ZERO_t, typename... Args>           \
  inline constexpr auto                                                 \
  _alias(Args&&... args)                                                \
  {                                                                     \
    return _func<_tol>(std::forward<Args>(args)...);                    \
  }

/// @brief Macro creates function alias with two functors as template
/// parameters
#define GATE_ALIAS_FTORS(_func, _alias)                                  \
  template<typename _functor, typename _functor_dag = _functor, typename... Args> \
  inline constexpr auto                                                 \
  _alias(Args&&... args)                                                \
  {                                                                     \
    return _func<_functor, _functor_dag>(std::forward<Args>(args)...);  \
  }

/// @brief Macro creates function alias with one functor and tolerance
/// as template parameter
#define GATE_ALIAS_FTOR_T(_func, _alias)                                \
  template<typename _functor, typename _tol = QConst_M_ZERO_t, typename... Args> \
  inline constexpr auto                                                 \
  _alias(Args&&... args)                                                \
  {                                                                     \
    return _func<_functor, _tol>(std::forward<Args>(args)...);          \
  }
#endif // QGATE_HPP
