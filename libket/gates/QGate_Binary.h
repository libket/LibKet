/** @file libket/gates/QGate_Binary.h

    @brief C++ API: Quantum gate macros for binary gates

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_BINARY_H
#define QGATE_BINARY_H

/// @brief Macro declares default (member) functions for binary gates
#define BINARY_GATE_DEFAULT_DECL(_gate, _dagger)                        \
  using self_t   = _gate;                                               \
  using dagger_t = _dagger;                                             \
                                                                        \
  inline constexpr auto operator()() const noexcept;                    \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;    \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string show() const noexcept;                             \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string dot() const noexcept;                              \
                                                                        \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro declares default (member) functions for binary gates
/// with angle and tolerance as template parameters
#define BINARY_GATE_DEFAULT_DECL_AT(_gate, _dagger)                     \
  using self_t   = _gate<_angle,_tol>;                                  \
  using dagger_t = _dagger<decltype(-_angle{}),_tol>;                   \
                                                                        \
  inline constexpr auto operator()() const noexcept;                    \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;    \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string show() const noexcept;                             \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string dot() const noexcept;                              \
                                                                        \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro declares default (member) functions for binary gates
/// with one functor as template parameter
#define BINARY_GATE_DEFAULT_DECL_FTOR(_gate, _dagger)                   \
  using self_t   = _gate<_functor, _tol>;                               \
  using dagger_t = _dagger<_functor, _tol>;                             \
                                                                        \
  inline constexpr auto operator()() const noexcept;                    \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;    \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string show() const noexcept;                             \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string dot() const noexcept;                              \
                                                                        \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro declares default (member) functions for binary gates
/// with two functors as template parameters
#define BINARY_GATE_DEFAULT_DECL_FTORS(_gate, _dagger)                  \
  using self_t   = _gate<_functor,_functor_dag>;                        \
  using dagger_t = _dagger<_functor_dag,_functor>;                      \
                                                                        \
  inline constexpr auto operator()() const noexcept;                    \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;    \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string show() const noexcept;                             \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string dot() const noexcept;                              \
                                                                        \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro declares default (member) functions for binary gates
/// with integer angle and tolerance as template parameters
#define BINARY_GATE_DEFAULT_DECL_KT(_gate, _dagger)                     \
  using self_t   = _gate<k,_tol>;                                       \
  using dagger_t = _dagger<k,_tol>;                                     \
                                                                        \
  inline constexpr auto operator()() const noexcept;                    \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept; \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;    \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string show() const noexcept;                             \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline std::string dot() const noexcept;                              \
                                                                        \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro implements default (member) functions for binary gates
#define BINARY_GATE_DEFAULT_IMPL(_class, _func)                         \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class& gate)                      \
  {                                                                     \
    os << #_func"(";                                                    \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline static auto                                                    \
  show(const _class& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"\n";                                                  \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline static auto                                                    \
  dot(const _class& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"\n";                                                  \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  inline static constexpr auto                                          \
  dagger(const _class& gate) noexcept                                   \
  {                                                                     \
    return _class::dagger_t();                                          \
  }                                                                     \
                                                                        \
  inline constexpr auto                                                 \
  _class::operator()() const noexcept                                   \
  {                                                                     \
    return LibKet::gates::_func();                                      \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class::operator()(const T0& t0, const T1& t1) const noexcept         \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1));                  \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class::operator()(T0&& t0, const T1& t1) const noexcept              \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1));                  \
  }                                                                     \
  template<typename T0, typename T1>                                    \
                                                                        \
  inline constexpr auto                                                 \
  _class::operator()(const T0& t0, T1&& t1) const noexcept              \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1));                  \
  }                                                                     \
                                                                        \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class::operator()(T0&& t0, T1&& t1) const noexcept                   \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T0>(t0),                   \
                                std::forward<T1>(t1));                  \
  }                                                                     \
                                                                        \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class::show() const noexcept                                         \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class::dot() const noexcept                                          \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  inline constexpr auto                                                 \
  _class::dagger() const noexcept                                       \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default (member) functions for binary
/// gates with angle and tolerance as template parameters
#define BINARY_GATE_DEFAULT_IMPL_AT(_class, _func)                      \
  template<typename _angle, typename _tol = QConst_M_ZERO_t>            \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class<_angle, _tol>& gate)        \
  {                                                                     \
    os << #_func"<" << _tol::to_type() << ">(" << _angle::to_obj() << ","; \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _angle, typename _tol>       \
  inline static auto                                                    \
  show(const _class<_angle, _tol>& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"<" << _angle::to_type() << "," << _tol::to_type() << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _angle, typename _tol>       \
  inline static auto                                                    \
  dot(const _class<_angle, _tol>& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"<" << _angle::to_type() << "," << _tol::to_type() << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  inline static constexpr auto                                          \
  dagger(const _class<_angle, _tol>& gate) noexcept                     \
  {                                                                     \
    return _class<_angle, _tol>::dagger_t();                            \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::operator()() const noexcept                     \
  {                                                                     \
    return LibKet::gates::_func<_tol>(_angle{});                        \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::operator()(const T0& t0, const T1& t1) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<_tol>(_angle{},                         \
                                      std::forward<T0>(t0),             \
                                      std::forward<T1>(t1));            \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::operator()(T0&& t0, const T1& t1) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<_tol>(_angle{},                         \
                                      std::forward<T0>(t0),             \
                                      std::forward<T1>(t1));            \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::operator()(const T0& t0, T1&& t1) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<_tol>(_angle{},                         \
                                      std::forward<T0>(t0),             \
                                      std::forward<T1>(t1));            \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::operator()(T0&& t0, T1&& t1) const noexcept     \
  {                                                                     \
    return LibKet::gates::_func<_tol>(_angle{},                         \
                                      std::forward<T0>(t0),             \
                                      std::forward<T1>(t1));            \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_angle, _tol>::show() const noexcept                           \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_angle, _tol>::dot() const noexcept                            \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::dagger() const noexcept                         \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default (member) functions for binary
/// gates with one functor as template parameter
#define BINARY_GATE_DEFAULT_IMPL_FTOR(_class, _func)                    \
  template<typename _functor, typename _tol = QConst_M_ZERO_t>          \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class<_functor, _tol>& gate)      \
  {                                                                     \
    os << #_func"<" << _functor::hash << "," << _tol::to_type() << ">("; \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _functor, typename _tol>     \
  inline static auto                                                    \
  show(const _class<_functor, _tol>& gate,                              \
       std::ostream& os, const std::string& prefix = "")                \
  {                                                                     \
    os << #_class"<" << _functor::hash << "," << _tol::to_type() << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _functor, typename _tol>     \
  inline static auto                                                    \
  dot(const _class<_functor, _tol>& gate,                               \
      std::ostream& os, const std::string& prefix = "")                 \
  {                                                                     \
    os << #_class"<" << _functor::hash << "," << _tol::to_type() << ">\n"; \
    return gate;                                                        \
  }                                                                   \
                                                                        \
  template<typename _functor, typename _tol>                            \
  inline static constexpr auto                                          \
  dagger(const _class<_functor, _tol>& gate) noexcept                   \
  {                                                                     \
    return _class<_functor, _tol>::dagger_t();                          \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  inline constexpr auto                                                 \
  _class<_functor,_tol>::operator()() const noexcept                    \
  {                                                                     \
    return LibKet::gates::_func<_functor, _tol>();                      \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_functor, _tol>::operator()(const T0& t0, const T1& t1) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<_functor, _tol>(                        \
                                          std::forward<T0>(t0),         \
                                          std::forward<T1>(t1));        \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_functor, _tol>::operator()(T0&& t0, const T1& t1) const noexcept    \
  {                                                                     \
    return LibKet::gates::_func<_functor, _tol>(                        \
                                          std::forward<T0>(t0),         \
                                          std::forward<T1>(t1));        \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_functor, _tol>::operator()(const T0& t0, T1&& t1) const noexcept    \
  {                                                                     \
    return LibKet::gates::_func<_functor, _tol>(                        \
                                          std::forward<T0>(t0),         \
                                          std::forward<T1>(t1));        \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_functor, _tol>::operator()(T0&& t0, T1&& t1) const noexcept         \
  {                                                                     \
    return LibKet::gates::_func<_functor, _tol>(                        \
                                          std::forward<T0>(t0),         \
                                          std::forward<T1>(t1));        \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_functor, _tol>::show() const noexcept                         \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_functor, _tol>::dot() const noexcept                          \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  inline constexpr auto                                                 \
  _class<_functor, _tol>::dagger() const noexcept                       \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default (member) functions for binary
/// gates with two functors as template parameters
#define BINARY_GATE_DEFAULT_IMPL_FTORS(_class, _func)                   \
  template<typename _functor, typename _functor_dag = _functor>         \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class<_functor, _functor_dag>& gate) \
  {                                                                     \
    os << #_func"<" << _functor::hash << "," << _functor_dag::hash << ">("; \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _functor, typename _functor_dag> \
  inline static auto                                                    \
  show(const _class<_functor, _functor_dag>& gate,                      \
       std::ostream& os, const std::string& prefix = "")                \
  {                                                                     \
    os << #_class"<" << _functor::hash << "," << _functor_dag::hash << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _functor, typename _functor_dag> \
  inline static auto                                                    \
  dot(const _class<_functor, _functor_dag>& gate,                       \
      std::ostream& os, const std::string& prefix = "")                 \
  {                                                                     \
    os << #_class"<" << _functor::hash << "," << _functor_dag::hash << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  inline static constexpr auto                                          \
  dagger(const _class<_functor, _functor_dag>& gate) noexcept           \
  {                                                                     \
    return _class<_functor, _functor_dag>::dagger_t();                  \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  inline constexpr auto                                                 \
  _class<_functor,_functor_dag>::operator()() const noexcept            \
  {                                                                     \
    return LibKet::gates::_func<_functor,_functor_dag>();               \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_functor, _functor_dag>::operator()(const T0& t0, const T1& t1) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<_functor, _functor_dag>(                \
                                      std::forward<T0>(t0),             \
                                      std::forward<T1>(t1));            \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<_functor, _functor_dag>::operator()(T0&& t0, const T1& t1) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<_functor, _functor_dag>(                \
                                      std::forward<T0>(t0),             \
                                      std::forward<T1>(t1));            \
  }                                                                     \
                                                                        \
template<typename _functor, typename _functor_dag>                      \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
 _class<_functor, _functor_dag>::operator()(const T0& t0, T1&& t1) const noexcept    \
  {                                                                     \
    return LibKet::gates::_func<_functor, _functor_dag>(                \
                                      std::forward<T0>(t0),             \
                                      std::forward<T1>(t1));            \
  }                                                                     \
                                                                        \
template<typename _functor, typename _functor_dag>                      \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
 _class<_functor, _functor_dag>::operator()(T0&& t0, T1&& t1) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<_functor, _functor_dag>(                \
                                      std::forward<T0>(t0),             \
                                      std::forward<T1>(t1));            \
  }                                                                     \
                                                                        \
template<typename _functor, typename _functor_dag>                      \
  template<std::size_t level>                                           \
  inline std::string                                                    \
 _class<_functor, _functor_dag>::show() const noexcept                  \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
template<typename _functor, typename _functor_dag>                      \
  template<std::size_t level>                                           \
  inline std::string                                                    \
 _class<_functor, _functor_dag>::dot() const noexcept                   \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
template<typename _functor, typename _functor_dag>                      \
  inline constexpr auto                                                 \
 _class<_functor, _functor_dag>::dagger() const noexcept                \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default (member) functions for binary
/// gates with integer angle and tolerance as template parameters
#define BINARY_GATE_DEFAULT_IMPL_KT(_class, _func)                      \
  template<std::size_t k, typename _tol = QConst_M_ZERO_t>              \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class<k, _tol>& gate)             \
  {                                                                     \
    os << #_func"<" << k << "," << _tol::to_type() << ">(";             \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, std::size_t k, typename _tol>         \
  inline static auto                                                    \
  show(const _class<k, _tol>& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"<" << k << "," << _tol::to_type() << ">\n";           \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, std::size_t k, typename _tol>         \
  inline static auto                                                    \
  dot(const _class<k, _tol>& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"<" << k << "," << _tol::to_type() << ">\n";           \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  inline static constexpr auto                                          \
  dagger(const _class<k, _tol>& gate) noexcept                          \
  {                                                                     \
    return _class<k, _tol>::dagger_t();                                 \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  inline constexpr auto                                                 \
  _class<k, _tol>::operator()() const noexcept                          \
  {                                                                     \
    return LibKet::gates::_func<k, _tol>();                             \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<k, _tol>::operator()(const T0& t0, const T1& t1) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<k, _tol>(std::forward<T0>(t0),          \
                                         std::forward<T1>(t1));         \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<k, _tol>::operator()(T0&& t0, const T1& t1) const noexcept     \
  {                                                                     \
    return LibKet::gates::_func<k, _tol>(std::forward<T0>(t0),          \
                                         std::forward<T1>(t1));         \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<k, _tol>::operator()(const T0& t0, T1&& t1) const noexcept     \
  {                                                                     \
    return LibKet::gates::_func<k, _tol>(std::forward<T0>(t0),          \
                                         std::forward<T1>(t1));         \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  template<typename T0, typename T1>                                    \
  inline constexpr auto                                                 \
  _class<k, _tol>::operator()(T0&& t0, T1&& t1) const noexcept          \
  {                                                                     \
    return LibKet::gates::_func<k, _tol>(std::forward<T0>(t0),          \
                                         std::forward<T1>(t1));         \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<k, _tol>::show() const noexcept                                \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<k, _tol>::dot() const noexcept                                 \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<std::size_t k, typename _tol>                                \
  inline constexpr auto                                                 \
  _class<k, _tol>::dagger() const noexcept                              \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default creator functions for binary gates
#define BINARY_GATE_DEFAULT_CREATOR(_class, _func)                      \
  template<typename _expr0, typename _expr1>                            \
  inline constexpr auto                                                 \
  _func(const _expr0& expr0, const _expr1& expr1) noexcept              \
  {                                                                     \
    return LibKet::gates::detail::_func##_impl<_expr0, _expr1>::apply(expr0, expr1); \
  }                                                                     \
                                                                        \
  template<typename _expr0, typename _expr1>                            \
  inline constexpr auto                                                 \
  _func(const _expr0& expr0, _expr1&& expr1) noexcept                   \
  {                                                                     \
    return LibKet::gates::detail::_func##_impl<_expr0, _expr1>::apply(expr0, expr1); \
  }                                                                     \
                                                                        \
  template<typename _expr0, typename _expr1>                            \
  inline constexpr auto                                                 \
  _func(_expr0&& expr0, const _expr1& expr1) noexcept                   \
  {                                                                     \
    return LibKet::gates::detail::_func##_impl<_expr0, _expr1>::apply(expr0, expr1); \
  }                                                                     \
                                                                        \
  template<typename _expr0, typename _expr1>                            \
  inline constexpr auto                                                 \
  _func(_expr0&& expr0, _expr1&& expr1) noexcept                        \
  {                                                                     \
    return LibKet::gates::detail::_func##_impl<_expr0, _expr1>::apply(expr0, expr1); \
  }

/// @brief Macro implements default creator functions for binary gates
/// with one functor and a tolerance as template paramete
#define BINARY_GATE_DEFAULT_CREATOR_FTOR_T(_class, _func)               \
  template<typename _functor, typename _tol = QConst_M_ZERO_t,          \
          typename _expr0, typename _expr1>                             \
  inline constexpr auto                                                 \
  _func(const _expr0& expr0, const _expr1& expr1) noexcept              \
  {                                                                     \
    return BinaryQGate<_expr0,                                          \
                       _expr1,                                          \
                       _class<_functor, _tol>,                                          \
                       decltype(typename filters::getFilter<_expr0>::type{} \
                                <<                                      \
                                typename filters::getFilter<_expr1>::type{})> \
                      (expr0, expr1);                                   \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol = QConst_M_ZERO_t,          \
          typename _expr0, typename _expr1>                             \
  inline constexpr auto                                                 \
  _func(const _expr0& expr0, _expr1&& expr1) noexcept                   \
  {                                                                     \
    return BinaryQGate<_expr0,                                          \
                       _expr1,                                          \
                       _class<_functor, _tol>,                                          \
                       decltype(typename filters::getFilter<_expr0>::type{} \
                                <<                                      \
                                typename filters::getFilter<_expr1>::type{})> \
                      (expr0, expr1);                                   \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol = QConst_M_ZERO_t,          \
          typename _expr0, typename _expr1>                             \
  inline constexpr auto                                                 \
  _func(_expr0&& expr0, const _expr1& expr1) noexcept                   \
  {                                                                     \
    return BinaryQGate<_expr0,                                          \
                       _expr1,                                          \
                       _class<_functor, _tol>,                                          \
                       decltype(typename filters::getFilter<_expr0>::type{} \
                                <<                                      \
                                typename filters::getFilter<_expr1>::type{})> \
                      (expr0, expr1);                                   \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol = QConst_M_ZERO_t,          \
          typename _expr0, typename _expr1>                             \
  inline constexpr auto                                                 \
  _func(_expr0&& expr0, _expr1&& expr1) noexcept                        \
  {                                                                     \
    return BinaryQGate<_expr0,                                          \
                       _expr1,                                          \
                       _class<_functor, _tol>,                                          \
                       decltype(typename filters::getFilter<_expr0>::type{} \
                                <<                                      \
                                typename filters::getFilter<_expr1>::type{})> \
                      (expr0, expr1);                                   \
  }  

#ifdef LIBKET_OPTIMIZE_GATES
#define BINARY_GATE_OPTIMIZE_CREATOR_CTRL(_class, _func)                \
  namespace detail {                                                    \
    template<typename _expr0,                                           \
             typename _expr1,                                           \
             typename = void>                                           \
    struct _func##_impl                                                 \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0, const _expr1& expr1)                   \
      {                                                                 \
        return BinaryQGate<_expr0,                                      \
                           _expr1,                                      \
                           _class,                                      \
                           decltype(typename filters::getFilter<_expr0>::type{} \
                                    <<                                  \
                                    typename filters::getFilter<_expr1>::type{})> \
          (expr0, expr1);                                               \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0_0,                                        \
             typename __expr0_1,                                        \
             typename __filter0,                                        \
             typename __expr1_0,                                        \
             typename __expr1_1,                                        \
             typename __filter1>                                        \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0_0, __expr0_1, _class, __filter0>,              \
     BinaryQGate<__expr1_0, __expr1_1, _class, __filter1>,              \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<__expr0_0>::type,                  \
        typename filters::getFilter<__expr1_0>::type>::value &&         \
        std::is_same                                                    \
        <typename filters::getFilter<__expr0_1>::type,                  \
        typename filters::getFilter<__expr1_1>::type>::value))>::type>  \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0_0, __expr0_1, _class, __filter0>& expr0, \
            const BinaryQGate<__expr1_0, __expr1_1, _class, __filter1>& expr1) \
      {                                                                 \
        return (l2r_evaluation::value                                   \
                ?                                                       \
                _func##_impl<decltype(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(all(expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))), \
                typename filters::getFilter<__filter1>::type>::         \
                apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))), \
                      typename filters::getFilter<__filter1>::type{})   \
                :                                                       \
                _func##_impl<typename filters::getFilter<__filter0>::type, \
                decltype(typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>:: \
                apply(typename filters::getFilter<__filter0>::type{},   \
                      typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(all(expr0.expr1(all(expr1.expr0(all(expr1.expr1))))))))) \
                );                                                      \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename _expr0,                                           \
             typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter>                                         \
    struct _func##_impl                                                 \
    <_expr0,                                                            \
     BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr0>::type,                     \
        typename filters::getFilter<__expr0>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr1>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr0>::type>::value &&            \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr0>::type>::value &&           \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr1>::type>::value)>::type>     \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0,                                        \
            const BinaryQGate<__expr0, __expr1, _class, __filter>& expr1) \
      {                                                                 \
        return (typename filters::getFilter<_expr0>::type{}             \
                <<                                                      \
                typename filters::getFilter<__filter>::type{});         \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename _expr0,                                           \
             typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter>                                         \
    struct _func##_impl                                                 \
    <_expr0,                                                            \
     BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr0>::type,                     \
        typename filters::getFilter<__expr0>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr1>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr0>::type>::value &&            \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr0>::type>::value &&   \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr1>::type>::value)>::type>     \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0,                                        \
            const BinaryQGate<__expr0, __expr1, _class, __filter>& expr1) \
      {                                                                 \
        return ((typename filters::getFilter<_expr0>::type{}            \
                 <<                                                     \
                 typename filters::getFilter<__filter>::type{})(all(expr1.expr0))); \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename _expr0,                                           \
             typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter>                                         \
    struct _func##_impl                                                 \
    <_expr0,                                                            \
     BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr0>::type,                     \
        typename filters::getFilter<__expr0>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr1>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr0>::type>::value &&            \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr0>::type>::value &&           \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr1>::type>::           \
       value)>::type>                                                   \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0,                                        \
            const BinaryQGate<__expr0, __expr1, _class, __filter>& expr1) \
      {                                                                 \
        return ((typename filters::getFilter<_expr0>::type{}            \
                 <<                                                     \
                 typename filters::getFilter<__filter>::type{})(all(expr1.expr1))); \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter,                                         \
             typename _expr1>                                           \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     _expr1,                                                            \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr1>::type,                     \
        typename filters::getFilter<__expr1>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr0>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr1>::type>::value &&            \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr0>::type>::value &&           \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr1>::type>::value)>::type>     \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0, __expr1, _class, __filter>& expr0, \
            const _expr1& expr1)                                        \
      {                                                                 \
        return (typename filters::getFilter<__filter>::type{}           \
                <<                                                      \
                typename filters::getFilter<_expr1>::type{});           \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter,                                         \
             typename _expr1>                                           \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     _expr1,                                                            \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr1>::type,                     \
        typename filters::getFilter<__expr1>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr0>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr1>::type>::value &&             \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr0>::type>::value &&   \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr1>::type>::value)>::type>     \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0, __expr1, _class, __filter>& expr0, \
            const _expr1& expr1)                                        \
      {                                                                 \
        return ((typename filters::getFilter<__filter>::type{}          \
                 <<                                                     \
                 typename filters::getFilter<_expr1>::type{})(all(expr0.expr0))); \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter,                                         \
             typename _expr1>                                           \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     _expr1,                                                            \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr1>::type,                     \
        typename filters::getFilter<__expr1>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr0>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr1>::type>::value &&            \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr0>::type>::value &&           \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr1>::type>::value)>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0, __expr1, _class, __filter>& expr0, \
            const _expr1& expr1)                                        \
      {                                                                 \
        return ((typename filters::getFilter<__filter>::type{}          \
                 <<                                                     \
                 typename filters::getFilter<_expr1>::type{})(all(expr0.expr1))); \
      }                                                                 \
    };                                                                  \
  }                                                                     \
  BINARY_GATE_OPTIMIZE_CREATOR_CTRL_L2R(_class, _func)

#ifdef LIBKET_L2R_EVALUATION
#define BINARY_GATE_OPTIMIZE_CREATOR_CTRL_L2R(_class, _func)            \
  namespace detail {                                                    \
    template<typename _expr0,                                           \
             typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter>                                         \
    struct _func##_impl                                                 \
    <_expr0,                                                            \
     BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr0>::type,                     \
        typename filters::getFilter<__expr0>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr1>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr0>::type>::value &&            \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr0>::type>::value &&   \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr1>::type>::value)>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0,                                        \
            const BinaryQGate<__expr0, __expr1,_class, __filter>& expr1) \
      {                                                                 \
        return (typename filters::getFilter<_expr0>::type{}             \
                <<                                                      \
                typename filters::getFilter<__filter>::type{})          \
          (all(expr1.expr1(all(expr1.expr0))));                         \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter,                                         \
             typename _expr1>                                           \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     _expr1,                                                            \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr1>::type,                     \
        typename filters::getFilter<__expr1>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr0>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr1>::type>::value &&            \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr0>::type>::value &&   \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr1>::type>::value)>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0, __expr1, _class, __filter>& expr0, \
            const _expr1& expr1)                                        \
      {                                                                 \
        return (typename filters::getFilter<__filter>::type{}           \
                <<                                                      \
                typename filters::getFilter<_expr1>::type{})            \
          (all(expr0.expr1(all(expr0.expr0))));                         \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename _expr0,                                           \
             typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter>                                         \
    struct _func##_impl                                                 \
    <_expr0,                                                            \
     BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr0>::type,                     \
        typename filters::getFilter<__expr0>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr1>::type>::value)) &&         \
       (std::is_base_of                                                 \
        <QGate, typename std::template decay<_expr0>::type>::value &&   \
        std::is_base_of                                                 \
        <filters::QFilter,                                              \
        typename std::template decay<__expr0>::type>::value &&          \
        std::is_base_of                                                 \
        <filters::QFilter,                                              \
        typename std::template decay<__expr1>::type>::value) &&         \
       !std::is_same<typename gates::getGate<_expr0>::type, _class>::value>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
        apply(const _expr0& expr0,                                      \
              const BinaryQGate<__expr0, __expr1, _class, __filter>& expr1) \
      {                                                                 \
        return ((typename filters::getFilter<_expr0>::type{}            \
                 <<                                                     \
                 typename filters::getFilter<__filter>::type{})(all(expr0))); \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename _expr0,                                           \
             typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter>                                         \
    struct _func##_impl                                                 \
    <_expr0,                                                            \
     BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr0>::type,                     \
        typename filters::getFilter<__expr0>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr1>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <QGate, typename std::template decay<_expr0>::type>::value &&    \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr0>::type>::value &&   \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr1>::type>::value)>::type>     \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0,                                        \
            const BinaryQGate<__expr0, __expr1, _class, __filter>& expr1) \
      {                                                                 \
        return ((typename filters::getFilter<_expr0>::type{}            \
                 <<                                                     \
                 typename filters::getFilter<__filter>::type{})         \
                (all(expr1.expr0(all(expr0)))));                        \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename _expr0,                                           \
             typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter>                                         \
    struct _func##_impl                                                 \
    <_expr0,                                                            \
     BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr0>::type,                     \
        typename filters::getFilter<__expr0>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr1>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <QGate, typename std::template decay<_expr0>::type>::value &&    \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr0>::type>::value &&           \
       std::is_base_of                                                  \
       <QGate,                                                          \
       typename std::template decay<__expr1>::type>::value) &&          \
      (!std::is_base_of                                                 \
       <_class,                                                         \
       typename std::template decay<_expr0>::type::gate_t>::value)>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0,                                        \
            const BinaryQGate<__expr0, __expr1, _class, __filter>& expr1) \
      {                                                                 \
        return ((typename filters::getFilter<_expr0>::type{}            \
                 <<                                                     \
                 typename filters::getFilter<__filter>::type{})         \
                (all(expr1.expr1(all(expr0)))));                        \
      }                                                                 \
    };                                                                  \
  }                                                
#else
#define BINARY_GATE_OPTIMIZE_CREATOR_CTRL_L2R(_class, _func)    \
  namespace detail {                                                    \
    template<typename _expr0,                                           \
             typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter>                                         \
    struct _func##_impl                                                 \
    <_expr0,                                                            \
     BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr0>::type,                     \
        typename filters::getFilter<__expr0>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr1>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr0>::type>::value &&            \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr0>::type>::value &&   \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr1>::type>::value)>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0,                                        \
            const BinaryQGate<__expr0, __expr1,_class, __filter>& expr1) \
      {                                                                 \
        return (typename filters::getFilter<_expr0>::type{}             \
                <<                                                      \
                typename filters::getFilter<__filter>::type{})          \
          (all(expr1.expr0(all(expr1.expr1))));                         \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter,                                         \
             typename _expr1>                                           \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     _expr1,                                                            \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr1>::type,                     \
        typename filters::getFilter<__expr1>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr0>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<_expr1>::type>::value &&            \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr0>::type>::value &&   \
       std::is_base_of                                                  \
       <QGate, typename std::template decay<__expr1>::type>::value)>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0, __expr1, _class, __filter>& expr0, \
            const _expr1& expr1)                                        \
      {                                                                 \
        return (typename filters::getFilter<__filter>::type{}           \
                <<                                                      \
                typename filters::getFilter<_expr1>::type{})            \
          (all(expr0.expr0(all(expr0.expr1))));                         \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter,                                         \
             typename _expr1>                                           \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     _expr1,                                                            \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr1>::type,                     \
        typename filters::getFilter<__expr1>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr0>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <QGate, typename std::template decay<_expr1>::type>::value &&    \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr0>::type>::value &&           \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr1>::type>::value) &&          \
      !std::is_same<typename gates::getGate<_expr1>::type, _class>::value>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0, __expr1, _class, __filter>& expr0, \
            const _expr1& expr1)                                        \
      {                                                                 \
        return ((typename filters::getFilter<__filter>::type{}          \
                 <<                                                     \
                 typename filters::getFilter<_expr1>::type{})(all(expr1))); \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter,                                         \
             typename _expr1>                                           \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     _expr1,                                                            \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr1>::type,                     \
        typename filters::getFilter<__expr1>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
         typename filters::getFilter<__expr0>::type>::value)) &&        \
         (std::is_base_of                                               \
          <QGate, typename std::template decay<_expr1>::type>::value && \
          std::is_base_of                                               \
          <QGate, typename std::template decay<__expr0>::type>::value && \
          std::is_base_of                                               \
          <filters::QFilter,                                            \
          typename std::template decay<__expr1>::type>::value)>::type>  \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0, __expr1, _class, __filter>& expr0, \
            const _expr1& expr1)                                        \
      {                                                                 \
        return ((typename filters::getFilter<__filter>::type{}          \
                 <<                                                     \
                 typename filters::getFilter<_expr1>::type{})           \
                (all(expr0.expr0(all(expr1)))));                        \
      }                                                                 \
    };                                                                  \
                                                                        \
    template<typename __expr0,                                          \
             typename __expr1,                                          \
             typename __filter,                                         \
             typename _expr1>                                           \
    struct _func##_impl                                                 \
    <BinaryQGate<__expr0, __expr1, _class, __filter>,                   \
     _expr1,                                                            \
     typename std::enable_if                                            \
     <((std::is_same                                                    \
        <typename filters::getFilter<_expr1>::type,                     \
        typename filters::getFilter<__expr1>::type>::value &&           \
        std::is_same                                                    \
        <typename filters::getFilter<__filter>::type,                   \
        typename filters::getFilter<__expr0>::type>::value)) &&         \
      (std::is_base_of                                                  \
       <QGate, typename std::template decay<_expr1>::type>::value &&    \
       std::is_base_of                                                  \
       <filters::QFilter,                                               \
       typename std::template decay<__expr0>::type>::value &&           \
       std::is_base_of                                                  \
       <QGate,                                                          \
       typename std::template decay<__expr1>::type>::value) &&          \
      (!std::is_base_of                                                 \
       <_class,                                                         \
       typename std::template decay<_expr1>::type::gate_t>::value)>::type> \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const BinaryQGate<__expr0, __expr1, _class, __filter>& expr0, \
            const _expr1& expr1)                                        \
      {                                                                 \
        return ((typename filters::getFilter<__filter>::type{}          \
                 <<                                                     \
                 typename filters::getFilter<_expr1>::type{})           \
                (all(expr0.expr1(all(expr1)))));                        \
      }                                                                 \
    };                                                                  \
  }                                                                
#endif
#else
#define BINARY_GATE_OPTIMIZE_CREATOR_CTRL(_class, _func)                \
  namespace detail {                                                    \
    template<typename _expr0,                                           \
             typename _expr1,                                           \
             typename = void>                                           \
    struct _func##_impl                                                 \
    {                                                                   \
      inline static constexpr auto                                      \
      apply(const _expr0& expr0, const _expr1& expr1)                   \
      {                                                                 \
        return BinaryQGate<_expr0,                                      \
                           _expr1,                                      \
                           _class,                                      \
                           decltype(typename filters::getFilter<_expr0>::type{} \
                                    <<                                  \
                                    typename filters::getFilter<_expr1>::type{})> \
          (expr0, expr1);                                               \
      }                                                                 \
    };                                                                  \
  } // namespace detail
#endif

#endif // QGATE_BINARY_H
