/** @file libket/gates/QGate_CNOT.hpp

    @brief C++ API quantum CNOT (controlled-X) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup cnot CNOT gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_CNOT_HPP
#define QGATE_CNOT_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief CNOT (controlled-X) gate class

The CNOT (controlled-X) gate class implements the quantum CNOT
gate for an arbitrary number of quantum bits.

The CNOT (controlled-X) gate is a two-qubit operation, where the first
qubit is usually referred to as the control qubit and the second qubit
as the target qubit. It maps the basis state \f$\left|00\right>\f$ to
\f$\left|00\right>\f$, \f$\left|01\right>\f$ to \f$\left|01\right>\f$,
\f$\left|10\right>\f$ to \f$\left|11\right>\f$ and
\f$\left|11\right>\f$ to \f$\left|10\right>\f$.

The CNOT (controlled-X) gates leaves the control qubit unchanged and
performs a Pauli-X gate on the target qubit only when the control
qubit is in state \f$\left|1\right>\f$.

The unitary matrix reads

\f[
\text{CNOT} =
\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & 1 & 0 & 0\\
0 & 0 & 0 & 1\\
0 & 0 & 1 & 0
\end{pmatrix}
\f]

@ingroup cnot
*/
class QCNOT : public QGate
{
public:
  BINARY_GATE_DEFAULT_DECL(QCNOT, QCNOT);
  
  /// @{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("CNOT q[" + utils::to_string(std::get<0>(i)) + "],q[" +
                         utils::to_string(std::get<1>(i)) + "]\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("cirq.CNOT.on(q[" + utils::to_string(std::get<0>(i)) +
                         "],q[" + utils::to_string(std::get<1>(i)) + "])\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    std::string _expr = "cnot q[";
    for (auto i : _filter0::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(expr))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("cx q[" + utils::to_string(std::get<0>(i)) + "], q[" +
                         utils::to_string(std::get<1>(i)) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel(
        [&]() { expr.kernel().cnot(std::get<0>(i), std::get<1>(i)); });

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("\tcnot q" + utils::to_string(std::get<0>(i)) + ",q" +
                         utils::to_string(std::get<1>(i)) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      quest::controlledNot(expr.reg(), std::get<0>(i), std::get<1>(i));

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel("CNOT " + utils::to_string(std::get<0>(i)) + " " +
                         utils::to_string(std::get<1>(i)) + "\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QULACS
  /// @brief Apply function
  /// @ingroup QULACS
  ///
  /// @note specialization for LibKet::QBackendType::Qulacs backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Qulacs>& apply(
    QExpression<_qubits, QBackendType::Qulacs>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.circuit().add_CNOT_gate(std::get<0>(i), std::get<1>(i));

    return expr;
  }
#endif
  
#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CNOT (controlled-X) gate can only be applied to quantum "
                  "objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
      expr.append_kernel(new qx::cnot(std::get<0>(i), std::get<1>(i)));

    return expr;
  }
#endif
  /// @}
};

using QCX = QCNOT;
  
/**
   @brief CNOT (controlled-X) gate creator
   @ingroup cnot
   
   This overload of the LibKet::gates::CNOT() function can be used as
   terminal, i.e. the inner-most gate in a quantum expression
   
   \code
   auto expr = gates::cnot();
   \endcode
*/
inline constexpr auto
cnot() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCNOT>(
    filters::QFilter{}, filters::QFilter{});
}

BINARY_GATE_OPTIMIZE_CREATOR_CTRL(QCNOT, cnot);
BINARY_GATE_DEFAULT_CREATOR(QCNOT, cnot);
GATE_ALIAS(cnot, CNOT);
GATE_ALIAS(cnot, CX);
GATE_ALIAS(cnot, cx);
GATE_ALIAS(cnot, cnotdag);
GATE_ALIAS(cnot, CNOTdag);
GATE_ALIAS(cnot, CXdag);
GATE_ALIAS(cnot, cxdag);
BINARY_GATE_DEFAULT_IMPL(QCNOT, cnot);

} // namespace gates

} // namespace LibKet

#endif // QGATE_CNOT_HPP
