/** @file libket/gates/QGate_OR.hpp

    @brief C++ API quantum OR (Logical OR gate) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @defgroup qor OR gate
    @ingroup  ternarygates
 */

#pragma once
#ifndef QGATE_OR_HPP
#define QGATE_OR_HPP

#include <QExpression.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief OR (Logical OR) gate class

The OR (Logical OR) gate class implements the quantum OR
(Logical OR) gate for an arbitrary number of quantum bits

The OR (Logical OR) gate is a three-qubit operation, where the first
qubit and the second qubit are usually referred to as the control
qubits and the third qubit as the target qubit. It maps the basis
state \f$\left|000\right>\f$ to \f$\left|000\right>\f$,
\f$\left|001\right>\f$ to \f$\left|001\right>\f$,
\f$\left|010\right>\f$ to \f$\left|011\right>\f$,
\f$\left|011\right>\f$ to \f$\left|010\right>\f$,
\f$\left|100\right>\f$ to \f$\left|101\right>\f$,
\f$\left|101\right>\f$ to \f$\left|100\right>\f$,
\f$\left|110\right>\f$ to \f$\left|111\right>\f$ and
\f$\left|111\right>\f$ to \f$\left|110\right>\f$

The OR (Logical OR) gates leaves the control qubit unchanged and
performs a Pauli-X gate on the target qubit when one of the control
qubits is in state \f$\left|1\right>\f$.

The unitary matrix reads

\f[
\text{OR} =
\begin{pmatrix}
1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\\
0 & 1 & 0 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 0 & 1 & 0 & 0 & 0 & 0\\
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\
0 & 0 & 0 & 0 & 1 & 0 & 0 & 0\\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 1\\
0 & 0 & 0 & 0 & 0 & 0 & 1 & 0
\end{pmatrix}
\f]

Since the OR gate is not standard, it is implemented as 
@verbatim
q_0:──■────■───────
      │    │       
q_1:──■────┼────■──
    ┌─┴─┐┌─┴─┐┌─┴─┐
q_2:┤ X ├┤ X ├┤ X ├
    └───┘└───┘└───┘
@endverbatim


@ingroup qor
*/
class QOR : public QGate
{
public:
  TERNARY_GATE_DEFAULT_DECL(QOR, QOR);
  
  ///@{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    for (auto i : utils::zip(
           _filter0::range(expr), _filter1::range(expr), _filter2::range(expr)))
      expr.append_kernel("CCNOT q[" + utils::to_string(std::get<0>(i)) +
                         "],q[" + utils::to_string(std::get<1>(i)) + "],q[" +
                         utils::to_string(std::get<2>(i)) + "]\n CNOT q[" 
                         + utils::to_string(std::get<0>(i)) + "],q[" +
                         utils::to_string(std::get<2>(i)) + "]\n CNOT q[" 
                         + utils::to_string(std::get<1>(i)) + "],q[" +
                         utils::to_string(std::get<2>(i)) + "]\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    for (auto i : utils::zip(
           _filter0::range(expr), _filter1::range(expr), _filter2::range(expr)))
      expr.append_kernel("cirq.CCNOT.on(q[" + utils::to_string(std::get<0>(i)) +
                         "],q[" + utils::to_string(std::get<1>(i)) + "],q[" +
                         utils::to_string(std::get<2>(i)) + "])\n" + 
                         "cirq.CNOT.on(q[" + utils::to_string(std::get<0>(i)) +
                         "],q[" + utils::to_string(std::get<2>(i)) + "])\n" + 
                         "cirq.CNOT.on(q[" + utils::to_string(std::get<1>(i)) +
                         "],q[" + utils::to_string(std::get<2>(i)) + "])\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    std::string _expr0 = "toffoli q[";
    for (auto i : _filter0::range(expr))
      _expr0 += utils::to_string(i) +
               (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(expr))
      _expr0 += utils::to_string(i) +
               (i != *(_filter1::range(expr).end() - 1) ? "," : "], q[");
    for (auto i : _filter2::range(expr))
      _expr0 += utils::to_string(i) +
               (i != *(_filter2::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr0);

    std::string _expr1 = "cnot q[";
    for (auto i : _filter0::range(expr))
      _expr1 += utils::to_string(i) +
               (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
    for (auto i : _filter2::range(expr))
      _expr1 += utils::to_string(i) +
               (i != *(_filter2::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr1);

    std::string _expr2 = "cnot q[";
    for (auto i : _filter1::range(expr))
      _expr2 += utils::to_string(i) +
               (i != *(_filter1::range(expr).end() - 1) ? "," : "], q[");
    for (auto i : _filter2::range(expr))
      _expr2 += utils::to_string(i) +
               (i != *(_filter2::range(expr).end() - 1) ? "," : "]\n");
    expr.append_kernel(_expr2);

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    for (auto i : utils::zip(
           _filter0::range(expr), _filter1::range(expr), _filter2::range(expr)))
      expr.append_kernel("ccx q[" + utils::to_string(std::get<0>(i)) + "], q[" +
                         utils::to_string(std::get<1>(i)) + "], q[" +
                         utils::to_string(std::get<2>(i)) + "];\n cx q[" 
                         + utils::to_string(std::get<0>(i)) + "], q[" +
                         utils::to_string(std::get<2>(i)) + "];\n cx q[" 
                         + utils::to_string(std::get<1>(i)) + "], q[" +
                         utils::to_string(std::get<2>(i)) + "];\n");

    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr), _filter2::range(expr))){
      expr.append_kernel([&]() {expr.kernel().toffoli(std::get<0>(i), std::get<1>(i), std::get<2>(i));});
      expr.append_kernel([&]() { expr.kernel().cnot(std::get<0>(i), std::get<2>(i)); });
      expr.append_kernel([&]() { expr.kernel().cnot(std::get<1>(i), std::get<2>(i)); });
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr), _filter2::range(expr))){
      expr.append_kernel("\ttoffoli q" + utils::to_string(std::get<0>(i)) +
                         ",q" + utils::to_string(std::get<1>(i)) + ",q" +
                         utils::to_string(std::get<2>(i)) + "\n");
      expr.append_kernel("\tcnot q" + utils::to_string(std::get<0>(i)) + ",q" +
                         utils::to_string(std::get<2>(i)) + "\n");
      expr.append_kernel("\tcnot q" + utils::to_string(std::get<1>(i)) + ",q" +
                         utils::to_string(std::get<2>(i)) + "\n");
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    for (auto i : utils::zip(_filter0::range(expr),
                             _filter1::range(expr),
                             _filter2::range(expr))) {
      int ctrl[] = { static_cast<int>(std::get<0>(i)),
                     static_cast<int>(std::get<1>(i)) };
      int targ[] = { static_cast<int>(std::get<2>(i))};
      quest:multiControlledMultiQubitNot(expr.reg(),
                                        ctrl,
                                        2,
                                        targ,
                                        1);
      quest::controlledNot(expr.reg(), std::get<0>(i), std::get<2>(i));
      quest::controlledNot(expr.reg(), std::get<1>(i), std::get<2>(i));
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr), _filter2::range(expr))){
      expr.append_kernel("CCNOT " + utils::to_string(std::get<0>(i)) + " " +
                         utils::to_string(std::get<1>(i)) + " " +
                         utils::to_string(std::get<2>(i)) + "\n");
      expr.append_kernel("CNOT " + utils::to_string(std::get<0>(i)) + " " +
                         utils::to_string(std::get<2>(i)) + "\n");
      expr.append_kernel("CNOT " + utils::to_string(std::get<1>(i)) + " " +
                         utils::to_string(std::get<2>(i)) + "\n");                               
    }

    return expr;
  }
#endif

#ifdef LIBKET_WITH_QULACS
  /// @brief Apply function
  /// @ingroup QULACS
  ///
  /// @note specialization for LibKet::QBackendType::Qulacs backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::Qulacs>& apply(
    QExpression<_qubits, QBackendType::Qulacs>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    for (auto i : utils::zip(_filter0::range(expr),
                             _filter1::range(expr),
                             _filter2::range(expr))) {
      // int ctrl[] = { static_cast<int>(std::get<0>(i)),
      //                static_cast<int>(std::get<1>(i)) };
      // int targ[] = { static_cast<int>(std::get<2>(i))};
      // quest:multiControlledMultiQubitNot(expr.reg(),
      //                                   ctrl,
      //                                   2,
      //                                   targ,
      //                                   1);
      expr.circuit().add_CNOT_gate(std::get<0>(i), std::get<2>(i));
      expr.circuit().add_CNOT_gate(std::get<1>(i), std::get<2>(i));
    }

    return expr;
  }
#endif
  
#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           typename _filter2>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() ==
          _filter1::template size<_qubits>() &&
        _filter0::template size<_qubits>() ==
          _filter2::template size<_qubits>(),
      "OR (Logical OR) gate can only be applied to quantum objects of the "
      "same size");
    for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr), _filter2::range(expr))){
      expr.append_kernel(new qx::toffoli(std::get<0>(i), std::get<1>(i), std::get<2>(i)));
      expr.append_kernel(new qx::cnot(std::get<0>(i), std::get<2>(i)));
      expr.append_kernel(new qx::cnot(std::get<1>(i), std::get<2>(i)));
    }

    return expr;
  }
#endif
  ///@}
};


/**
@brief OR (Logical OR) gate creator
@ingroup qor

This overload of the LibKet::gates::qor() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::qor();
\endcode
*/
inline constexpr auto
qor() noexcept
{
  return TernaryQGate<filters::QFilter,
                      filters::QFilter,
                      filters::QFilter,
                      QOR>(
    filters::QFilter{}, filters::QFilter{}, filters::QFilter{});
}

/// @defgroup qor_aliases Aliases
/// @ingroup qor
///@{

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function
eliminates the double-application of the OR gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
qor(const UnaryQGate<_expr, QOR, typename filters::getFilter<_expr>::type>&
        expr) noexcept
{
  return expr.expr;
}

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function
eliminates the double-application of the OR gate
*/
template<typename _expr>
inline constexpr auto
qor(UnaryQGate<_expr, QOR, typename filters::getFilter<_expr>::type>&&
        expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function accepts
three expressions as constant reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
qor(const _expr0& expr0, const _expr1& expr1, const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QOR,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function accepts the
first and second expression as constant reference, and the
third expression as universal reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
qor(const _expr0& expr0, const _expr1& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QOR,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function accepts the
first expression as constant reference, the second expression
as universal reference, and the third expression as constant
reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
qor(const _expr0& expr0, _expr1&& expr1, const _expr2 expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QOR,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function accepts the
first expression as constant reference and the second and third
expression as universal reference

*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
qor(const _expr0& expr0, _expr1&& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QOR,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function accepts the
first expression as universal reference and the second and
third expression as constant reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
qor(_expr0&& expr0, const _expr1& expr1, const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QOR,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function accepts the
first expression as universal reference, the second expression
as constant reference, and the third expression as universal
reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
qor(_expr0&& expr0, const _expr1& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QOR,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function accepts the
first and second expression as universal reference, and the
third expression as constant reference
*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
qor(_expr0&& expr0, _expr1&& expr1, const _expr2& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QOR,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

/**
@brief OR (Logical OR) gate creator

This overload of the LibKet::gates::qor() function accepts all
three expressions as universal reference

*/
template<typename _expr0, typename _expr1, typename _expr2>
inline constexpr auto
qor(_expr0&& expr0, _expr1&& expr1, _expr2&& expr2) noexcept
{
  return TernaryQGate<_expr0,
                      _expr1,
                      _expr2,
                      QOR,
                      decltype(typename filters::getFilter<_expr0>::type{}
                               << typename filters::getFilter<_expr1>::type{}
                               << typename filters::getFilter<_expr2>::type{})>(
    expr0, expr1, expr2);
}

GATE_ALIAS(qor, OR);
GATE_ALIAS(qor, ORdag);
TERNARY_GATE_DEFAULT_IMPL(QOR, qor);
///@}

} // namespace gates

} // namespace LibKet

#endif // QGATE_OR_HPP
