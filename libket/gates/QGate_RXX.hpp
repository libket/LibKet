/** @file libket/gates/QGate_RXX.hpp

    @brief C++ API quantum RXX (XX-rotation) class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller, Huub Donkers

    @defgroup rxx RXX gate
    @ingroup  binarygates
 */

#pragma once
#ifndef QGATE_RXX_HPP
#define QGATE_RXX_HPP

#include <QConst.hpp>
#include <QExpression.hpp>
#include <QFilter.hpp>
#include <QVar.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QRxxdag;
  
/**
@brief RXX (XX-rotation) gate class

The RXX (XX-rotation) gate class implements the
quantum XX-rotation gate for an arbitrary number of quantum
bits

The RXX (XX-rotation) gate is a two-qubit operation,
where the first qubit is usually referred to as the control qubit and
the second qubit as the target qubit. It maps the basis state
\f$\left|00\right>\f$ to \f$\left|00\right>\f$, \f$\left|01\right>\f$
to \f$\left|01\right>\f$, \f$\left|10\right>\f$ to
\f$\left|10\right>\f$ and \f$\left|11\right>\f$ to
\f$\exp(i\theta)\left|11\right>\f$.

The RXX (XX-rotation) gates leaves the control qubit
unchanged and performs a Phase gate on the target qubit only when the
control qubit is in state \f$\left|1\right>\f$.

The unitary matrix reads

\f[
\text{RXX}(\theta) =
\begin{pmatrix}
cos(\theta/2) & 0 & 0 & -isin(\theta/2)\\                                          \
0 & cos(\theta/2) & -isin(\theta/2) & 0\\
0 & -isin(\theta/2) & cos(\theta/2) & 0\\
-isin(\theta/2) & 0 & 0 & cos(\theta/2)
\end{pmatrix}
\f]

If the quantum programming language does not natively support the RXX
gate, it is implemented as:
@verbatim
     ┌───┐                       ┌───┐
q_0: ┤ H ├──■─────────────────■──┤ H ├
     ├───┤┌─┴─┐┌───────────┐┌─┴─┐├───┤
q_1: ┤ H ├┤ X ├┤ Rz(theta) ├┤ X ├┤ H ├
     └───┘└───┘└───────────┘└───┘└───┘
@endverbatim   

@ingroup rxx
*/
template<typename _angle, typename _tol = QConst_M_ZERO_t>
class QRxx : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  BINARY_GATE_DEFAULT_DECL_AT(QRxx, QRxxdag);
  
  ///@{
#ifdef LIBKET_WITH_AQASM
  /// @brief Apply function
  /// @ingroup AQASM
  ///
  /// @note specialization for LibKet::QBackendType::AQASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::AQASM>& apply(
    QExpression<_qubits, QBackendType::AQASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("H q[" + utils::to_string(std::get<0>(i)) + "]\n" + 
                           "H q[" + utils::to_string(std::get<1>(i)) + "]\n" + 
                           "CNOT q[" + utils::to_string(std::get<0>(i)) + "], q[" + utils::to_string(std::get<1>(i)) + "]\n" +
                           "RZ["+ _angle::to_string() +"] q[" + utils::to_string(std::get<1>(i)) + "]\n" +
                           "CNOT q[" + utils::to_string(std::get<0>(i)) + "], q[" + utils::to_string(std::get<1>(i)) + "]\n" +
                           "H q[" + utils::to_string(std::get<0>(i)) + "]\n" + 
                           "H q[" + utils::to_string(std::get<1>(i)) + "]\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// @brief Apply function
  /// @ingroup CIRQ
  ///
  /// @note specialization for LibKet::QBackendType::Cirq backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Cirq>& apply(
    QExpression<_qubits, QBackendType::Cirq>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("cirq.XXPowGate(exponent=" + _angle::to_string() +
                           ").on(q[" + utils::to_string(std::get<0>(i)) +
                           "],q[" + utils::to_string(std::get<1>(i)) + "])\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// @brief Apply function
  /// @ingroup CQASM
  ///
  /// @note specialization for LibKet::QBackendType::cQASMv1 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::cQASMv1>& apply(
    QExpression<_qubits, QBackendType::cQASMv1>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      //H
      std::string _expr = "h q[";
      for (auto i : _filter0::range(expr)) _expr += utils::to_string(i) + (i != *(_filter0::range(expr).end() - 1) ? "," : "] ");
      _expr += "\n";
      _expr += "h q[";
      for (auto i : _filter1::range(expr)) _expr += utils::to_string(i) + (i != *(_filter1::range(expr).end() - 1) ? "," : "] "); 
      _expr += "\n";

      //CNOT
      _expr += "cnot q["; 
      for (auto i : _filter0::range(expr)) _expr += utils::to_string(i) + (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(expr)) _expr += utils::to_string(i) + (i != *(_filter1::range(expr).end() - 1) ? "," : "] ");
      _expr += "\n";

      //RZ
      _expr += "rz q[";
      for (auto i : _filter1::range(expr)) _expr += utils::to_string(i) + (i != *(_filter1::range(expr).end() - 1) ? "," : "], ");
      _expr += _angle::to_string() + "\n";

      //CNOT
      _expr += "cnot q["; 
      for (auto i : _filter0::range(expr)) _expr += utils::to_string(i) + (i != *(_filter0::range(expr).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(expr)) _expr += utils::to_string(i) + (i != *(_filter1::range(expr).end() - 1) ? "," : "] ");
      _expr += "\n";

      //H
      _expr += "h q[";
      for (auto i : _filter0::range(expr)) _expr += utils::to_string(i) + (i != *(_filter0::range(expr).end() - 1) ? "," : "] ");
      _expr += "\n";
      _expr += "h q[";
      for (auto i : _filter1::range(expr)) _expr += utils::to_string(i) + (i != *(_filter1::range(expr).end() - 1) ? "," : "] "); 
      _expr += "\n";

      expr.append_kernel(_expr);
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// @brief Apply function
  /// @ingroup OPENQASM
  ///
  /// @note specialization for LibKet::QBackendType::OpenQASMv2 backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQASMv2>& apply(
    QExpression<_qubits, QBackendType::OpenQASMv2>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("rxx(" + _angle::to_string(false) + ") q[" +
                           utils::to_string(std::get<0>(i)) + "], q[" +
                           utils::to_string(std::get<1>(i)) + "];\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// @brief Apply function
  /// @ingroup OPENQL
  ///
  /// @note specialization for LibKet::QBackendType::OpenQL backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::OpenQL>& apply(
    QExpression<_qubits, QBackendType::OpenQL>& expr) noexcept
  {
    std::cerr << "The CR/RXX gate is not implemented for OpenQL!!!\n";
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel([&]() {
          expr.kernel().h(std::get<0>(i));
          expr.kernel().h(std::get<1>(i));
          expr.kernel().cnot(std::get<0>(i), std::get<1>(i));
          expr.kernel().rz(std::get<1>(i), _angle::value());
          expr.kernel().cnot(std::get<0>(i), std::get<1>(i));
          expr.kernel().h(std::get<0>(i));
          expr.kernel().h(std::get<1>(i));          
        });
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// @brief Apply function
  /// @ingroup QASM
  ///
  /// @note specialization for LibKet::QBackendType::QASM backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QASM>& apply(
    QExpression<_qubits, QBackendType::QASM>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("\tZZ q" + utils::to_string(std::get<0>(i)) +
                           ",q" + utils::to_string(std::get<1>(i)) + " # " +
                           _angle::to_string() + "\n");
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// @brief Apply function
  /// @ingroup QUEST
  ///
  /// @note specialization for LibKet::QBackendType::QuEST backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QuEST>& apply(
    QExpression<_qubits, QBackendType::QuEST>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr))){
        quest::hadamard(expr.reg(), std::get<0>(i));
        quest::hadamard(expr.reg(), std::get<1>(i));
        quest::controlledNot(expr.reg(), std::get<0>(i), std::get<1>(i));
        quest::rotateZ(expr.reg(), std::get<1>(i), (qreal)_angle::value());
        quest::controlledNot(expr.reg(), std::get<0>(i), std::get<1>(i));
        quest::hadamard(expr.reg(), std::get<0>(i));
        quest::hadamard(expr.reg(), std::get<1>(i));
      }
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// @brief Apply function
  /// @ingroup QUIL
  ///
  /// @note specialization for LibKet::QBackendType::Quil backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Quil>& apply(
    QExpression<_qubits, QBackendType::Quil>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr)))
        expr.append_kernel("H " + utils::to_string(std::get<0>(i)) + "\n" +
                           "H " + utils::to_string(std::get<1>(i)) + "\n" +
                           "CNOT " + utils::to_string(std::get<0>(i)) + " " + utils::to_string(std::get<1>(i)) + "\n" +
                           "RZ(" + _angle::to_string(true) + ") " + utils::to_string(std::get<1>(i)) + "\n" +
                           "CNOT " + utils::to_string(std::get<0>(i)) + " " + utils::to_string(std::get<1>(i)) + "\n" +
                           "H " + utils::to_string(std::get<0>(i)) + "\n" +
                           "H " + utils::to_string(std::get<1>(i)) + "\n"
                          );
    }
    return expr;
  }
#endif

#ifdef LIBKET_WITH_QULACS
  /// @brief Apply function
  /// @ingroup QULACS
  ///
  /// @note specialization for LibKet::QBackendType::Qulacs backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::Qulacs>& apply(
    QExpression<_qubits, QBackendType::Qulacs>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr))){
        expr.circuit().add_H_gate(std::get<0>(i));
        expr.circuit().add_H_gate(std::get<1>(i));
        expr.circuit().add_CNOT_gate(std::get<0>(i), std::get<1>(i));
        expr.circuit().add_RZ_gate(std::get<1>(i), (double)_angle::value());
        expr.circuit().add_CNOT_gate(std::get<0>(i), std::get<1>(i));
        expr.circuit().add_H_gate(std::get<0>(i));
        expr.circuit().add_H_gate(std::get<1>(i));
      }
    }
    return expr;
  }
#endif
  
#ifdef LIBKET_WITH_QX
  /// @brief Apply function
  /// @ingroup QX
  ///
  /// @note specialization for LibKet::QBackendType::QX backend
  ///
  /// @param  expr QExpression object on input
  /// @result      QExpression object on output
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QExpression<_qubits, QBackendType::QX>& apply(
    QExpression<_qubits, QBackendType::QX>& expr) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "RXX gate can only be applied to quantum objects of the same size");
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : utils::zip(_filter0::range(expr), _filter1::range(expr))){
        expr.append_kernel(new qx::hadamard(std::get<0>(i)));
        expr.append_kernel(new qx::hadamard(std::get<1>(i)));
        expr.append_kernel(new qx::cnot(std::get<0>(i), std::get<1>(i)));
        expr.append_kernel(new qx::rz(std::get<1>(i), (double)_angle::value()));
        expr.append_kernel(new qx::cnot(std::get<0>(i), std::get<1>(i)));
        expr.append_kernel(new qx::hadamard(std::get<0>(i)));
        expr.append_kernel(new qx::hadamard(std::get<1>(i)));
      }
    }
    return expr;
  }
#endif
  ///@}
};

/**
@brief RXX (XX-rotation) gate creator
@ingroup rxx

This overload of the LibKet::gates::rxx() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::rxx();
\endcode
*/
template<typename _tol = QConst_M_ZERO_t, typename _angle>
inline constexpr auto rxx(_angle) noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QRxx<_angle, _tol>>(
    filters::QFilter{}, filters::QFilter{});
}

namespace detail {
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename _expr1,
         typename = void>
struct rxx_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QRxx<_angle, _tol>,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

/// @defgroup rxx_aliases Aliases
/// @ingroup rxx
///@{

#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0_0,
         typename __expr0_1,
         typename __filter0,
         typename __expr1_0,
         typename __expr1_1,
         typename __filter1>
struct rxx_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0_0, __expr0_1, QRxx<_angle, _tol>, __filter0>,
  BinaryQGate<__expr1_0, __expr1_1, QRxx<_angle, _tol>, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0_0, __expr0_1, QRxx<_angle, _tol>, __filter0>&
      expr0,
    const BinaryQGate<__expr1_0, __expr1_1, QRxx<_angle, _tol>, __filter1>&
      expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return rxx_impl<_tol,
                       _angle,
                       decltype(typename filters::getFilter<__filter0>::type{}(
                         all(expr1.expr1(all(
                           expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
                       typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return rxx_impl<
      _tol,
      _angle,
      typename filters::getFilter<__filter0>::type,
      decltype(typename filters::getFilter<__filter1>::type{}(all(
        expr0.expr0(all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct rxx_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct rxx_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct rxx_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct rxx_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct rxx_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct rxx_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct rxx_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct rxx_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct rxx_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QRxx here */
    !std::is_same<typename gates::getGate<_expr0>::type, QRxx<_angle,_tol> >::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct rxx_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename _expr0,
         typename __expr0,
         typename __expr1,
         typename __filter>
struct rxx_impl<
  _tol,
  _angle,
  _expr0,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* RXX gate is handled explicitly */
    (!std::is_base_of<
      QRxx<_angle, _tol>,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct rxx_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QRxx here */
    !std::is_same<typename gates::getGate<_expr1>::type,
                  QRxx<_angle, _tol>>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct rxx_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief RXX (XX-rotation) creator

   This overload of the LibKet::gates::rxx() function
   eliminates the double-application of the RXX gate
*/
template<typename _tol,
         typename _angle,
         typename __expr0,
         typename __expr1,
         typename __filter,
         typename _expr1>
struct rxx_impl<
  _tol,
  _angle,
  BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* RXX gate is handled explicitly */
    (!std::is_base_of<
      QRxx<_angle, _tol>,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QRxx<_angle, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the RXX gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief RXX (XX-rotation) creator

This overload of the LibKet::gates::rxx() function accepts
two expressions as constant reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
rxx(_angle, const _expr0& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::rxx_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief RXX (XX-rotation) creator

This overload of the LibKet::gates::rxx() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
rxx(_angle, const _expr0& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::rxx_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief RXX (XX-rotation) creator

This overload of the LibKet::gates::rxx() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
rxx(_angle, _expr0&& expr0, const _expr1& expr1) noexcept
{
  return LibKet::gates::detail::rxx_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief RXX (XX-rotation) creator

This overload of the LibKet::gates::rxx() function accepts
two expression as universal reference
*/
template<typename _tol = QConst_M_ZERO_t,
         typename _angle,
         typename _expr0,
         typename _expr1>
inline constexpr auto
rxx(_angle, _expr0&& expr0, _expr1&& expr1) noexcept
{
  return LibKet::gates::detail::rxx_impl<_tol, _angle, _expr0, _expr1>::
    apply(expr0, expr1);
}

/**
@brief RXX (XX-rotation) gate creator

Function alias for LibKet::gates::rxx()
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
RXX(Args&&... args)
{
  return rxx<_tol>(std::forward<Args>(args)...);
}

/**
@brief RXX (XX-rotation) gate creator

Function alias for LibKet::gates::rxx()
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
xx(Args&&... args)
{
  return rxx<_tol>(std::forward<Args>(args)...);
}

/**
@brief RXX (XX-rotation) gate creator

Function alias for LibKet::gates::rxx()
*/
template<typename _tol = QConst_M_ZERO_t, typename... Args>
inline constexpr auto
XX(Args&&... args)
{
  return rxx<_tol>(std::forward<Args>(args)...);
}

BINARY_GATE_DEFAULT_IMPL_AT(QRxx, rxx);
///@}

} // namespace gates

} // namespace LibKet

#endif // QGATE_RXX_HPP
