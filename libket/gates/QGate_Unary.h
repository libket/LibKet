/** @file libket/gates/QGate_Unary.h

    @brief C++ API: Quantum gate macros for unary gates

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_UNARY_H
#define QGATE_UNARY_H

/// @brief Macro declares default (member) functions for unary gates
#define UNARY_GATE_DEFAULT_DECL(_gate, _dagger)                 \
  using self_t   = _gate;                                       \
  using dagger_t = _dagger;                                     \
                                                                \
  inline constexpr auto operator()() const noexcept;            \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(const T& t) const noexcept;  \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(T&& t) const noexcept;       \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string show() const noexcept;                     \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string dot() const noexcept;                      \
                                                                \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro declares default (member) functions for unary gates
/// with an angle and a tolerance as template parameters
#define UNARY_GATE_DEFAULT_DECL_AT(_gate, _dagger)              \
  using self_t   = _gate<_angle, _tol>;                         \
  using dagger_t = _dagger<decltype(-_angle{}), _tol>;          \
                                                                \
  inline constexpr auto operator()() const noexcept;            \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(const T& t) const noexcept;  \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(T&& t) const noexcept;       \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string show() const noexcept;                     \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string dot() const noexcept;                      \
                                                                \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro declares default (member) functions for unary gates
/// with one functor and a tolerance as template parameters
#define UNARY_GATE_DEFAULT_DECL_FTOR_T(_gate, _dagger)          \
  using self_t   = _gate<_functor, _tol>;                       \
  using dagger_t = _dagger<_functor, _tol>;                     \
                                                                \
  inline constexpr auto operator()() const noexcept;            \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(const T& t) const noexcept;  \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(T&& t) const noexcept;       \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string show() const noexcept;                     \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string dot() const noexcept;                      \
                                                                \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro declares default (member) functions for unary gates
/// with two functors as template parameters
#define UNARY_GATE_DEFAULT_DECL_FTORS(_gate, _dagger)           \
  using self_t   = _gate<_functor, _functor_dag>;               \
  using dagger_t = _dagger<_functor_dag, _functor>;             \
                                                                \
  inline constexpr auto operator()() const noexcept;            \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(const T& t) const noexcept;  \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(T&& t) const noexcept;       \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string show() const noexcept;                     \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string dot() const noexcept;                      \
                                                                \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro declares default (member) functions for unary gates
/// with a tolerance as template parameter
#define UNARY_GATE_DEFAULT_DECL_T(_gate, _dagger)               \
  using self_t   = _gate<_tol>;                                 \
  using dagger_t = _dagger<_tol>;                               \
                                                                \
  inline constexpr auto operator()() const noexcept;            \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(const T& t) const noexcept;  \
                                                                \
  template<typename T>                                          \
  inline constexpr auto operator()(T&& t) const noexcept;       \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string show() const noexcept;                     \
                                                                \
  template<std::size_t level = 1>                               \
  inline std::string dot() const noexcept;                      \
                                                                \
  inline constexpr auto dagger() const noexcept;

/// @brief Macro implements default (member) functions for unary gates
#define UNARY_GATE_DEFAULT_IMPL(_class, _func)                          \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class& gate)                      \
  {                                                                     \
    os << #_func"(";                                                    \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline static auto                                                    \
  show(const _class& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"\n";                                                  \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1>                                       \
  inline static auto                                                    \
  dot(const _class& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"\n";                                                  \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  inline static constexpr auto                                          \
  dagger(const _class& gate) noexcept                                   \
  {                                                                     \
    return _class::dagger_t();                                          \
  }                                                                     \
                                                                        \
  inline constexpr auto                                                 \
  _class::operator()() const noexcept                                   \
  {                                                                     \
    return LibKet::gates::_func();                                      \
  }                                                                     \
                                                                        \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class::operator()(const T& t) const noexcept                         \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T>(t));                    \
  }                                                                     \
                                                                        \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class::operator()(T&& t) const noexcept                              \
  {                                                                     \
    return LibKet::gates::_func(std::forward<T>(t));                    \
  }                                                                     \
                                                                        \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class::show() const noexcept                                         \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class::dot() const noexcept                                          \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  inline constexpr auto                                                 \
  _class::dagger() const noexcept                                       \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default (member) functions for unary gates
/// with angle and tolerance as template parameters
#define UNARY_GATE_DEFAULT_IMPL_AT(_class, _func)                       \
  template<typename _angle, typename _tol = QConst_M_ZERO_t>            \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class<_angle, _tol>& gate)        \
  {                                                                     \
    os << #_func"<" << _tol::to_type() << ">(" << _angle::to_obj() << ","; \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _angle, typename _tol>       \
  inline static auto                                                    \
  show(const _class<_angle, _tol>& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"<" << _angle::to_type() << "," << _tol::to_type() << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _angle, typename _tol>       \
  inline static auto                                                    \
  dot(const _class<_angle, _tol>& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"<" << _angle::to_type() << "," << _tol::to_type() << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  inline static constexpr auto                                          \
  dagger(const _class<_angle, _tol>& gate) noexcept                     \
  {                                                                     \
    return _class<_angle, _tol>::dagger_t();                            \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::operator()() const noexcept                     \
  {                                                                     \
    return LibKet::gates::_func<_tol>(_angle{});                        \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::operator()(const T& t) const noexcept           \
  {                                                                     \
    return LibKet::gates::_func<_tol>(_angle{}, std::forward<T>(t));    \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::operator()(T&& t) const noexcept                \
  {                                                                     \
    return LibKet::gates::_func<_tol>(_angle{}, std::forward<T>(t));    \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_angle, _tol>::show() const noexcept                           \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_angle, _tol>::dot() const noexcept                            \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _angle, typename _tol>                              \
  inline constexpr auto                                                 \
  _class<_angle, _tol>::dagger() const noexcept                         \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default (member) functions for unary gates
/// with one functor and a tolerance  as template parameters
#define UNARY_GATE_DEFAULT_IMPL_FTOR_T(_class, _func)                   \
  template<typename _functor, typename _tol = QConst_M_ZERO_t>          \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class<_functor, _tol>& gate)      \
  {                                                                     \
    os << #_func"<" << _functor::hash << "," << _tol::to_type() << ">("; \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _functor, typename _tol>     \
  inline static auto                                                    \
  show(const _class<_functor, _tol>& gate,                              \
       std::ostream& os, const std::string& prefix = "")                \
  {                                                                     \
    os << #_class"<" << _functor::hash << "," << _tol::to_type() << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _functor, typename _tol>     \
  inline static auto                                                    \
  dot(const _class<_functor, _tol>& gate,                               \
      std::ostream& os, const std::string& prefix = "")                 \
  {                                                                     \
    os << #_class"<" << _functor::hash << "," << _tol::to_type() << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  inline static constexpr auto                                          \
  dagger(const _class<_functor, _tol>& gate) noexcept                   \
  {                                                                     \
    return _class<_functor, _tol>::dagger_t();                          \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  inline constexpr auto                                                 \
  _class<_functor,_tol>::operator()() const noexcept                    \
  {                                                                     \
    return LibKet::gates::_func<_functor, _tol>();                      \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class<_functor, _tol>::operator()(const T& t) const noexcept         \
  {                                                                     \
    return LibKet::gates::_func<_functor, _tol>(std::forward<T>(t));    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class<_functor, _tol>::operator()(T&& t) const noexcept              \
  {                                                                     \
    return LibKet::gates::_func<_functor, _tol>(std::forward<T>(t));    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_functor, _tol>::show() const noexcept                         \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_functor, _tol>::dot() const noexcept                          \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol>                            \
  inline constexpr auto                                                 \
  _class<_functor, _tol>::dagger() const noexcept                       \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default (member) functions for unary gates
/// with two functors as template parameters
#define UNARY_GATE_DEFAULT_IMPL_FTORS(_class, _func)                    \
  template<typename _functor, typename _functor_dag = _functor>         \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class<_functor, _functor_dag>& gate) \
  {                                                                     \
    os << #_func"<" << _functor::hash << "," << _functor_dag::hash << ">("; \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _functor, typename _functor_dag> \
  inline static auto                                                    \
  show(const _class<_functor, _functor_dag>& gate,                      \
       std::ostream& os, const std::string& prefix = "")                \
  {                                                                     \
    os << #_class"<" << _functor::hash << "," << _functor_dag::hash << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _functor, typename _functor_dag> \
  inline static auto                                                    \
  dot(const _class<_functor, _functor_dag>& gate,                       \
      std::ostream& os, const std::string& prefix = "")                 \
  {                                                                     \
    os << #_class"<" << _functor::hash << "," << _functor_dag::hash << ">\n"; \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  inline static constexpr auto                                          \
  dagger(const _class<_functor, _functor_dag>& gate) noexcept           \
  {                                                                     \
    return _class<_functor, _functor_dag>::dagger_t();                  \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  inline constexpr auto                                                 \
  _class<_functor, _functor_dag>::operator()() const noexcept           \
  {                                                                     \
    return LibKet::gates::_func<_functor, _functor_dag>();              \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class<_functor, _functor_dag>::operator()(const T& t) const noexcept \
  {                                                                     \
    return LibKet::gates::_func<_functor, _functor_dag>(std::forward<T>(t)); \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class<_functor, _functor_dag>::operator()(T&& t) const noexcept      \
  {                                                                     \
    return LibKet::gates::_func<_functor, _functor_dag>(std::forward<T>(t)); \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_functor, _functor_dag>::show() const noexcept                 \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_functor, _functor_dag>::dot() const noexcept                  \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag>                    \
  inline constexpr auto                                                 \
  _class<_functor, _functor_dag>::dagger() const noexcept               \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default (member) functions for unary gates
/// with a tolerance as template parameter
#define UNARY_GATE_DEFAULT_IMPL_T(_class, _func)                        \
  template<typename _tol = QConst_M_ZERO_t>                             \
  std::ostream&                                                         \
  operator<<(std::ostream& os, const _class<_tol>& gate)                \
  {                                                                     \
    os << #_func"<" << _tol::to_type() << ">(";                         \
    return os;                                                          \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _tol>                        \
  inline static auto                                                    \
  show(const _class<_tol>& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"<" << _tol::to_type() << ">\n";                       \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<std::size_t level = 1, typename _tol>                        \
  inline static auto                                                    \
  dot(const _class<_tol>& gate, std::ostream& os, const std::string& prefix = "") \
  {                                                                     \
    os << #_class"<" << _tol::to_type() << ">\n";                       \
    return gate;                                                        \
  }                                                                     \
                                                                        \
  template<typename _tol>                                               \
  inline static constexpr auto                                          \
  dagger(const _class<_tol>& gate) noexcept                             \
  {                                                                     \
    return _class<_tol>::dagger_t();                                    \
  }                                                                     \
                                                                        \
  template<typename _tol>                                               \
  inline constexpr auto                                                 \
  _class<_tol>::operator()() const noexcept                             \
  {                                                                     \
    return LibKet::gates::_func<_tol>();                                \
  }                                                                     \
                                                                        \
  template<typename _tol>                                               \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class<_tol>::operator()(const T& t) const noexcept                   \
  {                                                                     \
    return LibKet::gates::_func<_tol>(std::forward<T>(t));              \
  }                                                                     \
                                                                        \
  template<typename _tol>                                               \
  template<typename T>                                                  \
  inline constexpr auto                                                 \
  _class<_tol>::operator()(T&& t) const noexcept                        \
  {                                                                     \
    return LibKet::gates::_func<_tol>(std::forward<T>(t));              \
  }                                                                     \
                                                                        \
  template<typename _tol>                                               \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_tol>::show() const noexcept                                   \
  {                                                                     \
    using ::LibKet::show;                                               \
    std::ostringstream os;                                              \
    show<level>(*this, os);                                             \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _tol>                                               \
  template<std::size_t level>                                           \
  inline std::string                                                    \
  _class<_tol>::dot() const noexcept                                    \
  {                                                                     \
    using ::LibKet::dot;                                                \
    std::ostringstream os;                                              \
    dot<level>(*this, os);                                              \
    return os.str();                                                    \
  }                                                                     \
                                                                        \
  template<typename _tol>                                               \
  inline constexpr auto                                                 \
  _class<_tol>::dagger() const noexcept                                 \
  {                                                                     \
    using ::LibKet::dagger;                                             \
    return dagger(*this);                                               \
  }

/// @brief Macro implements default creator functions for unary gates
#define UNARY_GATE_DEFAULT_CREATOR(_class, _func)                       \
  template<typename _expr>                                              \
  inline constexpr auto                                                 \
  _func(const _expr& expr) noexcept                                     \
  {                                                                     \
  return UnaryQGate<_expr,                                              \
                    _class,                                             \
                    typename filters::getFilter<_expr>::type>(expr);    \
  }                                                                     \
                                                                        \
  template<typename _expr>                                              \
  inline constexpr auto                                                 \
  _func(_expr&& expr) noexcept                                          \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class,                                           \
                      typename filters::getFilter<_expr>::type>(expr);  \
  }

/// @brief Macro implements default creator functions for unary gates
/// with an angle and a tolerance as template parameters
#define UNARY_GATE_DEFAULT_CREATOR_AT(_class, _func)                    \
  template<typename _tol = QConst_M_ZERO_t, typename _angle, typename _expr> \
  inline constexpr auto                                                 \
  _func(_angle, const _expr& expr) noexcept                             \
  {                                                                     \
  return UnaryQGate<_expr,                                              \
                    _class<_angle, _tol>,                               \
                    typename filters::getFilter<_expr>::type>(expr);    \
  }                                                                     \
                                                                        \
  template<typename _tol = QConst_M_ZERO_t, typename _angle, typename _expr> \
  inline constexpr auto                                                 \
  _func(_angle, _expr&& expr) noexcept                                  \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<_angle, _tol>,                             \
                      typename filters::getFilter<_expr>::type>(expr);  \
  }

/// @brief Macro implements default creator functions for unary gates
/// with one functor and a tolerance as template paramete
#define UNARY_GATE_DEFAULT_CREATOR_FTOR_T(_class, _func)                \
  template<typename _functor, typename _tol = QConst_M_ZERO_t, typename _expr> \
  inline constexpr auto                                                 \
  _func(const _expr& expr) noexcept                                     \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<_functor, _tol>,                           \
                    typename filters::getFilter<_expr>::type>(expr);    \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _tol = QConst_M_ZERO_t, typename _expr> \
  inline constexpr auto                                                 \
  _func(_expr&& expr) noexcept                                          \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<_functor, _tol>,                           \
                      typename filters::getFilter<_expr>::type>(expr);  \
  }

/// @brief Macro implements default creator functions for unary gates
/// with two functors as template parameters
#define UNARY_GATE_DEFAULT_CREATOR_FTORS(_class, _func, _func_dag)      \
  template<typename _functor, typename _functor_dag = _functor, typename _expr> \
  inline constexpr auto                                                 \
  _func(const _expr& expr) noexcept                                     \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<_functor, _functor_dag>,                   \
                      typename filters::getFilter<_expr>::type>(expr);  \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag = _functor, typename _expr> \
  inline constexpr auto                                                 \
  _func(_expr&& expr) noexcept                                          \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<_functor, _functor_dag>,                   \
                      typename filters::getFilter<_expr>::type>(expr);  \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag = _functor, typename _expr> \
  inline constexpr auto                                                 \
  _func_dag(const _expr& expr) noexcept                                 \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<_functor_dag, _functor>,                   \
                      typename filters::getFilter<_expr>::type>(expr);  \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag = _functor, typename _expr> \
  inline constexpr auto                                                 \
  _func_dag(_expr&& expr) noexcept                                      \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<_functor_dag, _functor>,                   \
                      typename filters::getFilter<_expr>::type>(expr);  \
  }

/// @brief Macro implements default creator functions for unary gates
/// with a tolerance as template parameters
#define UNARY_GATE_DEFAULT_CREATOR_T(_class, _func)                     \
  template<typename _tol = QConst_M_ZERO_t, typename _expr>             \
  inline constexpr auto                                                 \
  _func(const _expr& expr) noexcept                                     \
  {                                                                     \
  return UnaryQGate<_expr,                                              \
                    _class<_tol>,                                       \
                    typename filters::getFilter<_expr>::type>(expr);    \
  }                                                                     \
                                                                        \
  template<typename _tol = QConst_M_ZERO_t, typename _expr>             \
  inline constexpr auto                                                 \
  _func(_expr&& expr) noexcept                                          \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<_tol>,                                     \
                      typename filters::getFilter<_expr>::type>(expr);  \
  }

/// @brief Macro implements optimized creator functions for unary
/// gates whereby a double application of the gate is replaced by a
/// single application, e.g.,
///
/// LibKet::gates::identity(LibKet::gates::identity(expr))
///
/// becomes
///
/// LibKet::gates::identity(expr)
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_SINGLE(_class, _func)           \
  template<typename _expr>                                          \
  inline constexpr auto                                             \
  _func(const UnaryQGate<_expr,                                     \
        _class,                                                     \
        typename filters::getFilter<_expr>::type>&                  \
        expr) noexcept                                              \
  {                                                                 \
    return expr;                                                    \
  }                                                                 \
                                                                    \
  template<typename _expr>                                          \
  inline constexpr auto                                             \
  _func(UnaryQGate<_expr,                                           \
        _class,                                                     \
        typename filters::getFilter<_expr>::type>&&                 \
        expr) noexcept                                              \
  {                                                                 \
    return expr;                                                    \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_SINGLE(_class, _func)
#endif

/// @brief Macro implements optimized creator functions for unary
/// gates with an angle and a tolerence as template argument whereby a
/// application of the two consecutive gate with two different angles
/// is replaced by a single application of the inner gate with the
/// sum of the inner and the outer angle as argument, e.g.
///
/// rx(1.0, rx(2.0, expr))
///
/// becomes
///
/// rx(3.0, expr)
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_AT_ADD_SINGLE(_class, _func)        \
  template<typename _tol = QConst_M_ZERO_t, typename _angle,            \
           typename _expr, typename __angle,                            \
           typename = typename std::enable_if<!std::is_same<_angle,     \
                                                            decltype(-__angle{})>::value, \
                                              void>::type>              \
  inline constexpr auto                                                 \
  _func(__angle,                                                         \
        const UnaryQGate<_expr,                                         \
        _class<_angle, _tol>,                                           \
        typename filters::getFilter<_expr>::type>&                      \
        expr) noexcept                                                  \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<decltype(_angle{}+__angle{}), _tol>,        \
                      typename filters::getFilter<_expr>::type>(expr.expr); \
  }                                                                     \
                                                                        \
  template<typename _tol = QConst_M_ZERO_t, typename _angle,            \
           typename _expr, typename __angle,                            \
           typename = typename std::enable_if<!std::is_same<_angle,     \
                                                            decltype(-__angle{})>::value, \
                                              void>::type>              \
  inline constexpr auto                                                 \
  _func(__angle,                                                         \
        UnaryQGate<_expr,                                               \
        _class<_angle, _tol>,                                           \
        typename filters::getFilter<_expr>::type>&&                     \
        expr) noexcept                                                  \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<decltype(_angle{}+__angle{}), _tol>,        \
                      typename filters::getFilter<_expr>::type>(expr.expr); \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_AT_ADD_SINGLE(_class, _func)
#endif

/// @brief Macro implements optimized creator functions for unary
/// gates with an angle and a tolerence as template argument whereby a
/// application of the two consecutive gate with two different angles
/// is replaced by a single application of the inner gate with the
/// difference between the inner and the outer angle as argument, e.g.
///
/// rx(1.0, rxphase(2.0, expr))
///
/// becomes
///
/// rx(-1.0, expr)
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_AT_SUB_SINGLE(_class, _func)        \
  template<typename _tol = QConst_M_ZERO_t, typename _angle,            \
           typename _expr, typename __angle,                            \
           typename = typename std::enable_if<!std::is_same<_angle,     \
                                                            __angle>::value, \
                                              void>::type>              \
  inline constexpr auto                                                 \
  _func(__angle,                                                        \
        const UnaryQGate<_expr,                                         \
        _class<_angle, _tol>,                                           \
        typename filters::getFilter<_expr>::type>&                      \
        expr) noexcept                                                  \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<decltype(_angle{}-__angle{}), _tol>,       \
                      typename filters::getFilter<_expr>::type>(expr.expr); \
  }                                                                     \
                                                                        \
  template<typename _tol = QConst_M_ZERO_t, typename _angle,            \
           typename _expr, typename __angle,                            \
           typename = typename std::enable_if<!std::is_same<_angle,     \
                                                            __angle>::value, \
                                              void>::type>              \
  inline constexpr auto                                                 \
  _func(__angle,                                                        \
        UnaryQGate<_expr,                                               \
        _class<_angle, _tol>,                                           \
        typename filters::getFilter<_expr>::type>&&                     \
        expr) noexcept                                                  \
  {                                                                     \
    return UnaryQGate<_expr,                                            \
                      _class<decltype(_angle{}-__angle{}), _tol>,       \
                      typename filters::getFilter<_expr>::type>(expr.expr); \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_AT_SUB_SINGLE(_class, _func)
#endif

/// @brief Macro implements optimized creator functions for unary
/// gates whereby a double application of the gate yields the
/// identity, e.g.,
///
/// LibKet::gates::hadamard(LibKet::gates::hadamard(expr))
///
/// becomes
///
/// expr
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_IDENTITY(_class, _func)         \
  template<typename _expr>                                          \
  inline constexpr auto                                             \
  _func(const UnaryQGate<_expr,                                     \
        _class,                                                     \
        typename filters::getFilter<_expr>::type>&                  \
        expr) noexcept                                              \
  {                                                                 \
    return expr.expr;                                               \
  }                                                                 \
                                                                    \
  template<typename _expr>                                          \
  inline constexpr auto                                             \
  _func(UnaryQGate<_expr,                                           \
        _class,                                                     \
        typename filters::getFilter<_expr>::type>&&                 \
        expr) noexcept                                              \
  {                                                                 \
    return expr.expr;                                               \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_IDENTITY(_class, _func)
#endif

/// @brief Macro implements optimized creator functions for unary
/// gates with an angle and a tolerance as template parameters whereby
/// a double application of the same gate with the same angle but with
/// opposite sign yields the identity, e.g.,
///
/// LibKet::gates::rx(QConst(1.0),LibKet::gates::rx(-QConst(1.0),expr))
///
/// becomes
///
/// expr
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_AT_NEGATIVE_IDENTITY(_class, _func)      \
  template<typename _tol = QConst_M_ZERO_t, typename _angle, typename _expr> \
  inline constexpr auto                                                 \
  _func(_angle,                                                         \
        const UnaryQGate<_expr,                                         \
        _class<decltype(-_angle{}), _tol>,                              \
        typename filters::getFilter<_expr>::type>&                      \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }                                                                     \
                                                                        \
  template<typename _tol = QConst_M_ZERO_t, typename _angle, typename _expr> \
  inline constexpr auto                                                 \
  _func(_angle,                                                         \
        UnaryQGate<_expr,                                               \
        _class<decltype(-_angle{}), _tol>,                              \
        typename filters::getFilter<_expr>::type>&&                     \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_AT_NEGATIVE_IDENTITY(_class, _func)
#endif

/// @brief Macro implements optimized creator functions for unary
/// gates with an angle and a tolerance as template parameters whereby
/// a double application of the gate and its adjoint with the same
/// angle yields the identity, e.g.,
///
/// LibKet::gates::rx(QConst(1.0),LibKet::gates::rxdag(QConst(1.0),expr))
///
/// becomes
///
/// expr
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_AT_SAME_IDENTITY(_class, _func)      \
  template<typename _tol = QConst_M_ZERO_t, typename _angle, typename _expr> \
  inline constexpr auto                                                 \
  _func(_angle,                                                         \
        const UnaryQGate<_expr,                                         \
        _class<_angle, _tol>,                                           \
        typename filters::getFilter<_expr>::type>&                      \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }                                                                     \
                                                                        \
  template<typename _tol = QConst_M_ZERO_t, typename _angle, typename _expr> \
  inline constexpr auto                                                 \
  _func(_angle,                                                         \
        UnaryQGate<_expr,                                               \
        _class<_angle, _tol>,                                           \
        typename filters::getFilter<_expr>::type>&&                     \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_AT_SAME_IDENTITY(_class, _func)
#endif

/// @brief Macro implements optimized creator functions for unary
/// gates with one functor as template argument whereby a double
/// application of the gate with the same functor yields the identity,
/// e.g.,
///
/// LibKet::gates::hook<ftor>(LibKet::gates::hook<ftor>(expr))
///
/// becomes
///
/// expr
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_FTOR_IDENTITY(_class, _func)        \
  template<typename _functor, typename _expr>                           \
  inline constexpr auto                                                 \
  _func(const UnaryQGate<_expr,                                         \
        _class<_functor>,                                               \
        typename filters::getFilter<_expr>::type>&                      \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }                                                                     \
                                                                        \
  template<typename _functortypename _expr>                             \
  inline constexpr auto                                                 \
  _func(UnaryQGate<_expr,                                               \
        _class<_functor>,                                               \
        typename filters::getFilter<_expr>::type>&&                     \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_FTOR_IDENTITY(_class, _func)
#endif

/// @brief Macro implements optimized creator functions for unary
/// gates with two functors as template argument whereby a double
/// application of the gate with the functor and its adjoint yields
/// the identity, e.g.,
///
/// LibKet::gates::hook<ftor,ftor_dag>(LibKet::gates::hook<ftor,ftor_dag>(expr))
///
/// becomes
///
/// expr
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_FTORS_IDENTITY(_class, _func)       \
  template<typename _functor, typename _functor_dag = _functor, typename _expr> \
  inline constexpr auto                                                 \
  _func(const UnaryQGate<_expr,                                         \
        _class<_functor_dag, _functor>,                                 \
        typename filters::getFilter<_expr>::type>&                      \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }                                                                     \
                                                                        \
  template<typename _functor, typename _functor_dag = _functor, typename _expr> \
  inline constexpr auto                                                 \
  _func(UnaryQGate<_expr,                                               \
        _class<_functor_dag, _functor>,                                 \
        typename filters::getFilter<_expr>::type>&&                     \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_FTORS_IDENTITY(_class, _func)
#endif

/// @brief Macro implements optimized creator functions for unary
/// gates with a tolerance as template parameter whereby a double
/// application of the same gate yields the identity, e.g.,
///
/// LibKet::gates::sqrt_swap<tol>(LibKet::gates::sqrt_swap<tol>(expr))
///
/// becomes
///
/// expr
#ifdef LIBKET_OPTIMIZE_GATES
#define UNARY_GATE_OPTIMIZE_CREATOR_T_IDENTITY(_class, _func)           \
  template<typename _tol = QConst_M_ZERO_t, typename _expr>             \
  inline constexpr auto                                                 \
  _func(const UnaryQGate<_expr,                                         \
        _class<_tol>,                                                   \
        typename filters::getFilter<_expr>::type>&                      \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }                                                                     \
                                                                        \
  template<typename _tol = QConst_M_ZERO_t, typename _expr>             \
  inline constexpr auto                                                 \
  _func(UnaryQGate<_expr,                                               \
        _class<_tol>,                                                   \
        typename filters::getFilter<_expr>::type>&&                     \
        expr) noexcept                                                  \
  {                                                                     \
    return expr.expr;                                                   \
  }
#else
#define UNARY_GATE_OPTIMIZE_CREATOR_T_IDENTITY(_class, _func)
#endif

#endif // QGATE_UNARY_H
