/** @file libket/QVar.hpp

    @brief C++ API quantum variable class

    @copyright This file is part of the LibKet library (C++ API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller

    @ingroup cxx_api
 */

#pragma once
#ifndef QVAR_HPP
#define QVAR_HPP

#include <QConst.hpp>
#include <QUtils.hpp>

namespace LibKet {

  /**
     @brief
     Quantum variable
  */
  template<std::size_t _index=0, std::size_t _size=1>
  struct QVar_t : public LibKet::constvalue_t<>
  {
  public:
    /// Default constructor
    QVar_t() {}

    /// Value constructor
    QVar_t(real_t value) { _value = value; }
    
    /// Returns value as string
    inline static std::string to_string(bool symbol=false)
    {      
      return (symbol
              ? "qvar"+std::to_string(_index)+"_"+std::to_string(_size-1)
              : std::to_string(_value));
    }
    
    /// Returns type as string
    inline static std::string to_type()
    {
      return "QVar_t<" + std::to_string(_index) + "," + std::to_string(_size) + ">(" + to_string() + ")";
    }
    
    /// Returns object as string
    inline static std::string to_obj()
    {
      return "QVar<" + std::to_string(_index) + "," + std::to_string(_size) + ">(" + to_string() + ")";
    }
    
    /// Returns value as float
    inline static float to_float()
    {
      return std::stof(to_string());
    }    
    
    /// Returns value as double
    inline static float to_double()
    {
      return std::stod(to_string());
    }
    
    /// Returns value as long double
    inline static float to_long_double()
    {
      return std::stold(to_string());
    }
    
    /// Returns value by reference
    inline static real_t value()
    {
      return _value;
    }

    /// Operator=()
    inline QVar_t& operator=(real_t value)
    {
      _value = value;
      return *this;
    }

    /// Operator+()
    template<std::size_t __index>
    inline QVar_t& operator+(const QVar_t<__index, _size>& other)
    {
      _value += other.value();
      return *this;
    }

    /// Operator-()
    template<std::size_t __index>
    inline QVar_t& operator-(const QVar_t<__index, _size>& other)
    {
      _value -= other.value();
      return *this;
    }

    /// Operator*()
    template<std::size_t __index>
    inline QVar_t& operator*(const QVar_t<__index, _size>& other)
    {
      _value *= other.value();
      return *this;
    }

    /// Operator/()
    template<std::size_t __index>
    inline QVar_t& operator/(const QVar_t<__index, _size>& other)
    {
      _value /= other.value();
      return *this;
    }
    
  private:
    static real_t _value;
  };

  // Initialize non-constant value
  template<std::size_t _index, std::size_t _size>
  real_t QVar_t<_index, _size>::_value = 0.0;
  
  /// Operator+()
  template<std::size_t _index, std::size_t _size>
  inline static auto operator+(const QVar_t<_index, _size>& obj)
  {
    return QVar_t<_index, _size>(obj.value());
  }

  /// Operator-()
  template<std::size_t _index, std::size_t _size>
  inline static auto operator-(const QVar_t<_index, _size>& obj)
  {
    return QVar_t<_index, _size>(-obj.value());
  }

  /// Operator==()
  template<std::size_t _index, std::size_t _size, std::size_t __index>
  inline static bool operator==(const QVar_t<_index, _size>& obj, const QVar_t<__index, _size>& other)
  {
    return (obj.value() == other.value());
  }

  /// Operator!=()
  template<std::size_t _index, std::size_t _size, std::size_t __index>
  inline static bool operator!=(const QVar_t<_index, _size>& obj, const QVar_t<__index, _size>& other)
  {
    return (obj.value() != other.value());
  }

  /// Operator<=()
  template<std::size_t _index, std::size_t _size, std::size_t __index>
  inline static bool operator<=(const QVar_t<_index, _size>& obj, const QVar_t<__index, _size>& other)
  {
    return (obj.value() <= other.value());
  }

  /// Operator>=()
  template<std::size_t _index, std::size_t _size, std::size_t __index>
  inline static bool operator>=(const QVar_t<_index, _size>& obj, const QVar_t<__index, _size>& other)
  {
    return (obj.value() >= other.value());
  }
  
  /// Operator<()
  template<std::size_t _index, std::size_t _size, std::size_t __index>
  inline static bool operator<(const QVar_t<_index, _size>& obj, const QVar_t<__index, _size>& other)
  {
    return (obj.value() < other.value());
  }

  /// Operator>()
  template<std::size_t _index, std::size_t _size, std::size_t __index>
  inline static bool operator>(const QVar_t<_index, _size>& obj, const QVar_t<__index, _size>& other)
  {
    return (obj.value() > other.value());
  }

  /// Tolerance function: true if abs(first argument) > abs(second argument)
  template<std::size_t _index, std::size_t _size, std::size_t __index>
  inline static bool
  tolerance(const QVar_t<_index, _size>& obj, const QVar_t<__index, _size>& other)
  {
    return (std::abs(obj.value()) >
            std::abs(other.value()));
  }
  
  /// Tolerance function: true if abs(first argument) > abs(second argument)
  template<std::size_t _index, char... Cs>
  inline static bool
  tolerance(const QVar_t<_index, 1>& obj, const constvalue_t<Cs...>&)
  {
    return (std::abs(obj.value()) >
            std::abs(constvalue_t<Cs...>::value()));
  }

  /// Tolerance function: true if abs(first argument) > abs(second argument)
  template<char... Cs, std::size_t _index>
  inline static bool
  tolerance(const constvalue_t<Cs...>&, const QVar_t<_index, 1>& obj)
  {
    return (std::abs(constvalue_t<Cs...>::value()) >
            std::abs(obj.value()));
  }

  /// Tolerance function: true if abs(value) > abs(second argument)
  template<std::size_t _index>
  inline static bool
  tolerance(const real_t& value, const QVar_t<_index, 1>& obj)
  {
    return (std::abs(value) > std::abs(obj.value()));
  }

  /// Serialization operator
  template<std::size_t _index, std::size_t _size>
  std::ostream&
  operator<<(std::ostream& os, const QVar_t<_index, _size>& obj)
  {
    os << obj.to_obj();
    return os;
  }

  /// Global variable counter
  static constexpr utils::counter<__COUNTER__, 0> __QVAR_COUNTER__;

  /// Macro instantiates a new variable
#define QVar(...) QVar_t<__QVAR_COUNTER__.next<__COUNTER__>()>(__VA_ARGS__);
  
} // namespace LibKet

#endif // QVAR_HPP
