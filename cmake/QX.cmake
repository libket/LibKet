########################################################################
# QX.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

include(FetchContent)

########################################################################
# QX
########################################################################

FetchContent_Declare(qx-simulator
  GIT_REPOSITORY https://github.com/QuTech-Delft/qx-simulator
  )

FetchContent_MakeAvailable(qx-simulator)
FetchContent_GetProperties(qx-simulator)
include_directories(${qx-simulator_SOURCE_DIR}/include)

list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   qx)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES qx)
