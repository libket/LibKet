########################################################################
# OpenQL.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

include(FetchContent)

########################################################################
# OpenQL
########################################################################

FetchContent_Declare(openql
  GIT_REPOSITORY https://github.com/QuTech-Delft/OpenQL.git
  GIT_TAG v0.11.1
  )

FetchContent_MakeAvailable(openql)
FetchContent_GetProperties(openql)
include_directories(${openql_SOURCE_DIR}/include)
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   ql)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES ql)
