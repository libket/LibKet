########################################################################
# Qulacs.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

include(FetchContent)

########################################################################
# Qulacs
########################################################################

FetchContent_Declare(qulacs
  URL https://github.com/qulacs/qulacs/archive/refs/tags/v0.6.1.zip
  FIND_PACKAGE_ARGS
  )

set(USE_OMP    ${LIBKET_WITH_OPENMP} CACHE BOOL "Qulacs in multi-threaded mode")
set(USE_MPI    ${LIBKET_WITH_MPI}    CACHE BOOL "Qulacs in distributed mode")
set(USE_PYTHON OFF                   CACHE BOOL "Qulacs disable Python")
set(USE_TEST   OFF                   CACHE BOOL "Qulacs disable tests")

FetchContent_MakeAvailable(qulacs)
FetchContent_GetProperties(qulacs)
include_directories(${qulacs_SOURCE_DIR}/include)
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   cppsim_shared)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES cppsim_shared)

# Boost
set(Boost_USE_STATIC_LIBS    ON)
set(Boost_USE_MULTITHREADED  ON)
set(Boost_USE_STATIC_RUNTIME ON)
# The minimum version tested in CI is 1.71.0.
find_package(Boost 1.71.0 REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})
