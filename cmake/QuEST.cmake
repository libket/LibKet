########################################################################
# QuEST.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

include(FetchContent)

########################################################################
# QuEST
########################################################################

FetchContent_Declare(quest
  URL https://github.com/QuEST-Kit/QuEST/archive/refs/tags/v3.5.0.zip
  )

set(TESTING       0                     CACHE BOOL "QuEST disable testing")
set(DISTRIBUTED   ${LIBKET_WITH_MPI}    CACHE BOOL "QuEST in distributed mode")
set(MULTITHREADED ${LIBKET_WITH_OPENMP} CACHE BOOL "QuEST in multi-threaded mode")

if(LIBKET_COEFF_TYPE STREQUAL "float")
  set(PRECISION "1" CACHE STRING "QuEST in single precision mode")
elseif(LIBKET_COEFF_TYPE STREQUAL "double")
  set(PRECISION "2" CACHE STRING "QuEST in double precision mode")
else()
  set(PRECISION "4" CACHE STRING "QuEST in quadrupel precision mode")
endif()

FetchContent_MakeAvailable(quest)
FetchContent_GetProperties(quest)
include_directories(${quest_SOURCE_DIR}/include)
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   QuEST)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES QuEST)
