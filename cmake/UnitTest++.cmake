########################################################################
# UnitTest++.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2021 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# UnitTest++
########################################################################

include(FetchContent)
FetchContent_Declare(unittest-cpp
  URL https://github.com/unittest-cpp/unittest-cpp/archive/refs/heads/master.zip
  )

FetchContent_MakeAvailable(unittest-cpp)
FetchContent_GetProperties(unittest-cpp)
include_directories(${unittest-cpp_SOURCE_DIR}/include)
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   UnitTest++)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES UnitTest++)
